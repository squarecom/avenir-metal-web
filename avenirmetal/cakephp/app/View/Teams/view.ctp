<!-- File: /app/View/Teams/view.ctp -->

<?php 
//debug($team['Team']);
//debug($team['CoteTeam']);
?>

<div class="row">
    <div class="col-xs-12 col-sm-2">
        <div class="widget-box">
            <div class="widget-content text-center nopadding">
                <?php
				if(!isset($team['Team']['image'])) 
				    {
				            echo $this->Html->image('team/default', array('alt' => ' ','class'=>'img_team img-thumbnail',
				                    ));
				    } else {
				            echo $this->Html->image($team['Team']['image'], array('alt' => ' ','class'=>'img_team img-thumbnail',
				                    )); 
				    }
				    echo '<h3 class="text-center">';
				    echo $team['Team']['title'];
				    echo '</h3>';
				?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="widget-box">
            <div class="widget-content">
                <?php 
                //debug($team['Team']); 
                echo '<pre>';
                echo $team['Team']['content'];
                echo '</pre>';
                foreach ($members as $mb) {
                    echo $this->html->link($mb['User']['show_username'], array('controller' => 'users', 'action' => 'profile', $mb['User']['id']), array('class'=> 'btn btn-primary'));
                    echo ' ';
                } 
                if((isset($team['Team']['url'])) && ($team['Team']['url'] != '')){
                    echo '<span class="btn btn-danger">';
                    echo $team['Team']['url'];
                    echo '</span> ';
                }
                echo '<span class="btn btn-warning">';
                echo $team['Team']['acronym'];
                echo '</span> ';
                echo '<span class="btn btn-inverse">';
                    echo $jeu['Game']['acr'];
                echo '</span> ';
                echo '<span class="btn btn-info">';
                echo $team['Team']['created'];
                echo '</span>';
                ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <h5>Actu</h5>
            </div>
            <div class="widget-content text-center">
                Tableau des derniers Actualité_team
            </div>
        </div>
    </div>
</div>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-picture-o"></i>
                </span>
                <h5>Badges</h5>
            </div>
            <div class="widget-content">
                <div class="gallery-masonry" style="text-align:center;">
                    <div class="row">
                        <div class="col-xs-3 col-sm-2 col-lg-1 nopadding">
                            <a href="#" class="thumbnail" style="border-radius: 50%;">
                                <?php echo $this->Html->image('apple-touch-icon-114-precomposed.png', array('class' => 'img-circle')); ?>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-2 col-lg-1 nopadding">
                            <a href="#" class="thumbnail" style="border-radius: 50%;">
                                <?php echo $this->Html->image('apple-touch-icon-114-precomposed.png', array('class' => 'img-circle')); 
                                //echo sha1(1);
                                //echo mcrypt_encrypt(1);
                                //echo '<br>';
                                //mcrypt_encrypt(cipher, key, data, mode)
                                //echo mcrypt_decrypt(cipher, key, data, mode)
                                //echo sha1(1);
                                ?>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-2 col-lg-1 nopadding">
                            <a href="#" class="thumbnail" style="border-radius: 50%;">
                                <?php echo $this->Html->image('apple-touch-icon-114-precomposed.png', array('class' => 'img-circle')); ?>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-2 col-lg-1 nopadding">
                            <a href="#" class="thumbnail" style="border-radius: 50%;">
                                <?php echo $this->Html->image('apple-touch-icon-114-precomposed.png', array('class' => 'img-circle')); ?>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-2 col-lg-1 nopadding">
                            <a href="#" class="thumbnail" style="border-radius: 50%;">
                                <?php echo $this->Html->image('apple-touch-icon-114-precomposed.png', array('class' => 'img-circle')); ?>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-2 col-lg-1 nopadding">
                            <a href="#" class="thumbnail" style="border-radius: 50%;">
                                <?php echo $this->Html->image('apple-touch-icon-114-precomposed.png', array('class' => 'img-circle')); ?>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-2 col-lg-1 nopadding">
                            <a href="#" class="thumbnail" style="border-radius: 50%;">
                                <?php echo $this->Html->image('apple-touch-icon-114-precomposed.png', array('class' => 'img-circle')); ?>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-2 col-lg-1 nopadding">
                            <a href="#" class="thumbnail" style="border-radius: 50%;">
                                <?php echo $this->Html->image('apple-touch-icon-114-precomposed.png', array('class' => 'img-circle')); ?>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-2 col-lg-1 nopadding">
                            <a href="#" class="thumbnail" style="border-radius: 50%;">
                                <?php echo $this->Html->image('apple-touch-icon-114-precomposed.png', array('class' => 'img-circle')); ?>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-2 col-lg-1 nopadding">
                            <a href="#" class="thumbnail" style="border-radius: 50%;">
                                <?php echo $this->Html->image('apple-touch-icon-114-precomposed.png', array('class' => 'img-circle')); ?>
                            </a>
                        </div>
                        <div class="col-xs-3 col-sm-2 col-lg-1 nopadding">
                            <a href="#" class="thumbnail" style="border-radius: 50%;">
                                <?php echo $this->Html->image('apple-touch-icon-114-precomposed.png', array('class' => 'img-circle')); ?>
                            </a>
                        </div>
                        
                    </div>

                    <!--
                    <div class="item" style="display:inline-block; float:none;">
                        <a href="#" class="thumbnail"><?php echo $this->Html->image('apple-touch-icon-114-precomposed.png'); ?></a>
                    </div>
                    <div class="item" style="display:inline-block; float:none;">
                        <a href="#" class="thumbnail"><?php echo $this->Html->image('apple-touch-icon-114-precomposed.png'); ?></a>
                    </div>
                    <div class="item" style="display:inline-block; float:none;">
                        <a href="#" class="thumbnail"><?php echo $this->Html->image('apple-touch-icon-114-precomposed.png'); ?></a>
                    </div>
                    <div class="item" style="display:inline-block; float:none;">
                        <a href="#" class="thumbnail"><?php echo $this->Html->image('apple-touch-icon-114-precomposed.png'); ?></a>
                    </div>
                    <div class="item" style="display:inline-block; float:none;">
                        <a href="#" class="thumbnail"><?php echo $this->Html->image('apple-touch-icon-114-precomposed.png'); ?></a>
                    </div>
                    <div class="item" style="display:inline-block; float:none;">
                        <a href="#" class="thumbnail"><?php echo $this->Html->image('apple-touch-icon-114-precomposed.png'); ?></a>
                    </div>
                    <div class="item" style="display:inline-block; float:none;">
                        <a href="#" class="thumbnail"><?php echo $this->Html->image('apple-touch-icon-114-precomposed.png'); ?></a>
                    </div>
                    <div class="item" style="display:inline-block; float:none;">
                        <a href="#" class="thumbnail"><?php echo $this->Html->image('apple-touch-icon-114-precomposed.png'); ?></a>
                    </div>
                    <div class="item" style="display:inline-block; float:none;">
                        <a href="#" class="thumbnail"><?php echo $this->Html->image('apple-touch-icon-114-precomposed.png'); ?></a>
                    </div>
                -->








                </div>
            </div>
        </div>
    </div>
</div>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<?php 
//debug($score); 
$score = array_reverse($score);
echo '<span id="graphcreator" data-win="1225" data-loose="1225" data-b1="1225" data-b2="1225" data-b3="1225" data-bp="1225"';
for ($i=0; $i < count($score); $i++) { 
	$val = 20-$i;
	echo 'data-e'.$i.'="'.$score[$i]['CoteTeam']['score'].'"';
}
echo 'data-style="1"></span>';


?>

<div class="row">
    <div class="col-xs-12">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <h5>Stats</h5>
            </div>
            <div class="widget-content">

                <div class="chart chart-team"></div>

            </div>
        </div>
    </div>
</div>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<div class="row">
    <div class="col-xs-4">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <h5>Graph1</h5>
            </div>
            <div class="widget-content nopadding">

                <div class="pie pie-ratio"></div>

            </div>
        </div>
    </div>
    <div class="col-xs-4">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <h5>Graph2</h5>
            </div>
            <div class="widget-content nopadding">

                <div class="pie pie-coupe"></div>

            </div>
        </div>
    </div>
    <div class="col-xs-4">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-th"></i>
                </span>
                <h5>Match Récent</h5>
            </div>
            <div class="widget-content nopadding">

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="widget-box widget-chat">
            <div class="widget-title"><span class="icon"><i class="fa fa-list"></i></span><h5>Commentaires</h5></div>
            <div class="widget-content nopadding">
                <div class="chat-content nopadding" style="height:auto;">
                   <div class="chat-messages" id="chat-messages" style="height:auto;">
                        <div id="chat-messages-inner" class="chat-messages-inner">
                            <?php 
                            echo ' <div class="chat-message  nopadding well"><span style="line-height:20px;" class="input-box input-group">';
                            echo '<span>&emsp; Légende : </span>';
                            echo '<span>&emsp;<span style="display:inline-block; border:1px solid grey; width:20px; height:11px; background-color:rgb(255, 240, 240);"> </span> Administrateur </span>';
                            echo '<span>&emsp;<span style="display:inline-block; border:1px solid grey; width:20px; height:11px; background-color:rgb(240, 240, 255);"> </span> Chef d\'équipe </span>';
                            echo '<span>&emsp;<span style="display:inline-block; border:1px solid grey; width:20px; height:11px; background-color:rgb(240, 255, 255);"> </span> Membre de l\'équipe </span>';
                            echo '</span></div> ';   
                            $aff = $this->Paginator->numbers(array('first' => 2, 'last' => 2));
                            if($aff!='') {
                                echo ' <div class="chat-message  nopadding well"><span class="input-box input-group">';
                                echo '<span>&emsp; Page : </span>';
                                echo  $aff;
                                echo '</span></div> ';
                            }
                            if(isset($comment)){
                                foreach ($comment as $com) { 
                                    echo '<p class="show">';
                                    if(!is_null($com['Infos']['Profile']['avatar'])){
                                      echo $this->Html->image($com['Infos']['Profile']['avatar'], array('alt' => ''));
                                    } else {
                                        echo $this->Html->image('avatar/default', array('alt' => ''));
                                    }
                                    $membt =false;
                                    $membtadm=false;
                                    foreach ($members as $mb) {
                                        if($com['Infos']['User']['id']==$mb['User']['id']){
                                            $membt=true;
                                            if(1==$mb['UsersHasTeam']['adm']){
                                                $membtadm=true;
                                            }
                                        }
                                    } 
                                    if($com['Infos']['User']['role']>=7) { $colormsg= 'style="background-color:rgb(255, 240, 240)"';}

                                    else if($membtadm) { $colormsg= 'style="background-color:rgb(240, 240, 255)"';}

                                    else if($membt) { $colormsg= 'style="background-color:rgb(240, 255, 255)"';}
                                    else { $colormsg= '';}
                                    echo '<span '.$colormsg.' class="msg-block"><strong>';
                                    echo $this->Html->link($com['Infos']['User']['show_username'], array('action' => 'profile', $com['Infos']['User']['show_username']), array('style'=>'color:#333;'));
                                    echo '</strong><span class="time"> -';
                                    echo $com['CommentTeam']['created'];
                                    echo '</span>';
                                    $idcpt = $this->Session->read('Auth.User.id');
                                    if((isset($idcpt)) && ($idcpt==$com['Infos']['User']['id'])){
                                        echo $this->html->link('Supprimer',
                                             array('controller'=>'CommentTeams', 'action' => 'delete', $com['CommentTeam']['id'], $team['Team']['id']),
                                            array('confirm' => 'Etes-vous sure de vouloir supprimer ce message ?', 'style' => 'float:right; padding:0 5px; margin:0;', 'class' => 'btn btn-danger btn-xs')
                                        );
                                    }
                                    echo '<span class="msg">';   
                                    echo $com['CommentTeam']['content'];
                                    echo '</span>';
                                    echo '</span></p>';

                                    //echo $com['User']['id'];   
                                }
                            }
                            ?>
                        </div>
                   </div>  
                </div>
                   <?php
                   if($aff!='') {
                        echo ' <div class="chat-message  nopadding well"><span class="input-box input-group">';
                        echo '<span>&emsp; Page : </span>';
                        echo  $aff;
                        echo '</span></div> ';
                    }


                    echo $this->Form->create('CommentTeam', array('action'=>'add')); 
                    echo ' <div class="chat-message well"><span class="input-box input-group">';
                    

                    echo $this->Form->hidden('CommentTeam.team_id', array('value'=>$team['Team']['id']));

                    echo $this->Form->input('CommentTeam.content', 
                        array(  'label'         => false,
                                'placeholder'   => 'Taper votre commentaire...',
                                'required'      => false,
                                'class'         => 'form-control input-small maxwidthme',
                                'maxlength'     => 500,
                                'rows'          => 1,
                                'div'           => false,
                                'minlength'     => 1,
                                'error' => array('attributes' => array('class' => 'alert alert-danger'))
                        )
                    );
                    echo '<span class="input-group-btn">';
                    $options = array(
                        'label'         => 'Envoyer',
                        'div'           => false,
                        'class'         =>'btn btn-success btn-small'
                    );
                    echo $this->Form->end($options);
                    echo '</span></div>';
                    ?>
                </span>                                                   
           </div>                               
        </div>
    </div>
</div>

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!--
    <input placeholder="Enter message here..." type="text" class="form-control input-small" name="msg-box" id="msg-box" />
    <span class="input-group-btn">
        <button class="btn btn-success btn-small" type="button">Send</button>
    </span>
                -->