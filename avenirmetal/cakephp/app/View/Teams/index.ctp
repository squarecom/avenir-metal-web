<!-- File: /app/View/ActivityUser/index.ctp  (edit links added) -->

<table>
    <tr>
        <th></th>
        <th></th>
        <th></th>
    </tr>

</table>

<div class="row">
    <div class="col-xs-2">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-th"></i>
                </span>
                <h5>Sous menu</h5>
            </div>
            <div class="widget-content nopadding">
                <table class="table table-bordered table-striped table-hover">
                    <tbody>
                        <tr>
                            <td><?php echo $this->Html->link('Tous le monde', array('controller'=>'Teams', 'action'=>'index'), array('class' => 'btn btn-success btn-block'));  ?></td>
                        </tr>
                        <tr>
                            <td><?php echo $this->Html->link('Mes Amis', array('controller'=>'Teams', 'action'=>'index/friend'), array('class' => 'btn btn-primary btn-block'));  ?></td>               
                        </tr>
                        <tr>
                            <td><?php echo $this->Html->link('Mes Equipes', array('controller'=>'Teams', 'action'=>'index/all'), array('class' => 'btn btn-primary btn-block'));  ?></td>                
                        </tr>
                        <tr>
                            <td><?php echo $this->Html->link('Joueur de League of Legend', array('controller'=>'Teams', 'action'=>'index/all'), array('class' => 'btn btn-danger btn-block'));  ?></td>                
                        </tr>
                        <tr>
                            <td><?php echo $this->Html->link('Joueur de Starcraft 2', array('controller'=>'Teams', 'action'=>'index/all'), array('class' => 'btn btn-danger btn-block'));  ?></td>              
                        </tr>
                        <tr>
                            <td><?php echo $this->Html->link('Joueur de Shootmania ', array('controller'=>'Teams', 'action'=>'index/all'), array('class' => 'btn btn-danger btn-block'));  ?></td>
                        </tr>
                        <tr>
                            <td><?php echo $this->Html->link('Joueur de Dota 2', array('controller'=>'Teams', 'action'=>'index/all'), array('class' => 'btn btn-danger btn-block'));  ?></td>
                        </tr>
                    </tbody>
                    </table>  
            </div>
        </div>
    </div>

    <div class="col-xs-10">
        <div class="widget-box">
            <div class="widget-title">
                <span class="icon">
                    <i class="icon-th"></i>
                </span>
                <h5>Dynamic table</h5>
            </div>
            <div class="widget-content nopadding">
                <table class="table table-bordered table-striped table-hover data-table">
                    <thead>
                        <tr>
                            <th>Avatar</th>
                            <th>Nom de l'équipe</th>
                            <th>Description</th>
                            <th>Crée le</th>
                            <th>(Approved)</th>
                            <th>Nombre de membres</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($teams as $team): ?>
                        <tr class="gradeA">
                            <td class="text-center">
                            <?php
                            $filename = 'http://localhost/teamesprit'.$team['Team']['image'];
                            echo $this->Html->image($filename, array('alt' => '','class'=>'img_post img-thumbnail',
                                'style'=>'min-height:50px; height:50px; width:50px; min-width:50px;'
                            )); 
                            ?></td>

                            <td> <?php
                                $adr = '/teams/view/'.$team['Team']['id'];
                                echo $this->Html->link($team['Team']['title'], $adr, array('class' => 'btn'));  
                                 ?>
                            </td>
                            <td><?php echo $team['Team']['content']; ?></td>
                            <td class="text-center"><?php echo $team['Team']['created']; ?></td>   
                            <td class="text-center">Undef ;)</td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                    </table>  
            </div>
        </div>
    </div>
</div>

<script>
    $(".img_post").error(function() {
       $( this ).attr( "src", "http://localhost/teamesprit/img/avatar/default.jpg" );
      });
</script>