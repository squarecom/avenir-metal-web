<div class="row bloc-am">
<div class="col-sm-12 bloc-min">
    
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="widget-box">
            <div class="widget-title"><span class="icon"><i class="fa fa-plus"></i></span><h5>Création d'une équipe</h5></div>
            <div class="widget-content nopadding">
                <?php
                $this->Html->addCrumb('Equipe', '/teams');
                $this->Html->addCrumb('Créer une équipe', '/teams/add', 'class=\'current\'');

                echo $this->Form->create('Team', array('class'=>'form-horizontal','type'=>'file')); 

                echo $this->Form->input('User.id', array('type' => 'hidden', 'value' => $this->Session->read('Auth.User.id'))); 

                echo $this->Form->input('Team.title', 
                            array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Nom*'),
                                    'placeholder'   => 'Exemple : Esprit...',
                                    'before'        => '',
                                    'after'         => '</div>',
                                    'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                                    'div'           => array('class' => 'form-group'),
                                    'class'         => 'form-control input-sm',
                                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                            )
                );
                echo '<div style="clear:both;"></div>';

                echo $this->Form->input('Team.acronym', 
                            array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Acronyme*'),
                                    'placeholder'   => 'Exemple : EsP...',
                                    'before'        => '',
                                    'after'         => '</div>',
                                    'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                                    'div'           => array('class' => 'form-group'),
                                    'class'         => 'form-control input-sm',
                                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                            )
                );
                echo '<div style="clear:both;"></div>';

                echo $this->Form->input('Team.content', 
                            array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Description*'),
                                    'placeholder'   => 'Exemple : Nous sommes des amis jouant ensemble...',
                                    'before'        => '',
                                    'after'         => '</div>',
                                    'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                                    'div'           => array('class' => 'form-group'),
                                    'class'         => 'form-control input-sm',
                                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                            )
                );
                echo '<div style="clear:both;"></div>';

                echo $this->Form->input('Team.url', 
                            array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Site Internet'),
                                    'placeholder'   => 'Exemple : www.Team-Esprit.fr',
                                    'before'        => '',
                                    'after'         => '</div>',
                                    'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                                    'div'           => array('class' => 'form-group'),
                                    'class'         => 'form-control input-sm',
                                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                            )
                );
                echo '<div style="clear:both;"></div>';

                $tabgame = array();
                foreach ($styleavailable as $style) {
                    $v1 = $style['Game']['id'];
                    $v2 = ' '.$style['Game']['title'].'<br>'.$this->Html->image($style['Thumb']['icon'], array('alt' => ' ','class'=>'img_post img-thumbnail'  ));
                    $tabgame[$v1] = $v2;
                }

                echo '<div class="form-group"><label for="TeamGame_id" class="col-sm-3 col-md-3 col-lg-2 control-label">Icone :</label>';
                echo '<div class="col-sm-9 col-md-9 col-lg-10 "><label class="text-center">';
                echo $this->Form->radio('Team.game_id', $tabgame,
                    array(  'legend'        => false,
                            'fieldset'      => false,
                            'label'         => false,
                            'between'       => '<label>',
                            'separator'     => '</label><label class="text-center">',
                            ));
                echo '</div></div>';
                echo '<div style="clear:both;"></div>';

                echo $this->Form->input('Team.image_file', 
                            array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Image'),
                                    'type'          => 'file',
                                    'required'      => false,
                                    'before'        => '',
                                    'after'         => '</div>',
                                    'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                                    'div'           => array('class' => 'form-group'),
                                    'class'         => 'form-control input-sm',
                                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                            )
                );


                $options = array(
                    'label' => 'Créer l\'équipe',
                    'div' => array(
                        'class' => 'col-sm-9 col-md-9 col-lg-10',
                    ),
                    'before'        => '',
                    'after'         => '</div>',
                    'class'=>'btn btn-primary'
                );
                echo '<div class="form-group"><label class="col-sm-3 col-md-3 col-lg-2 control-label"></label>';
                echo $this->Form->end($options);
                ?>
            </div>
        </div>
    </div>
</div>
<br/><br/>
          
                              

