<div style="text-align:center">
  <h1>API AVENIR METAL</h1>
</div>  
<div class="functionContainer">
   <a target="_blank" href="http://api.team-esprit.fr/api/getLogin/">getLogin($login, $password)</a>
   <div class="entry-summary">
    <p>Permet de savoir si le login et le mot de passe donné correspond à un Admin</p>
  </div><!-- .entry-summary -->
</div>  
<div class="functionContainer">
   <a target="_blank" href="http://api.team-esprit.fr/api/getHash/password">getHash($password)</a>
   <div class="entry-summary">
    <p>Permet d'obtenir le hash du mot de passe donné</p>
  </div><!-- .entry-summary -->
</div> 
<div class="functionContainer">
   <a target="_blank" href="http://api.team-esprit.fr/api/getChef/p.durant/password">getChef($login, $pasword)</a>
   <div class="entry-summary">
    <p>Permet de savoir si le login et le mot de passe donné correspond à un Chef de chantier</p>
    <p>Fonction pour le login d'un Chef de chantier</p>
  </div><!-- .entry-summary -->
</div> 
<div class="functionContainer">
   <a target="_blank" href="http://api.team-esprit.fr/api/getChantier/1">getChantier($id_du_chantier)</a>
   <div class="entry-summary">
    <p>Permet d'obtenir le détails d'un chantier</p>
  </div><!-- .entry-summary -->
</div> 
<div class="functionContainer">
   <a target="_blank" href="http://api.team-esprit.fr/api/getChantiersByChef/1">getChantiersByChef($id_du_chef_de_chantier)</a>
   <div class="entry-summary">
    <p>Permet d'obtenir la liste des chantiers d'un chef de chantier</p>
  </div><!-- .entry-summary -->
</div> 
<div class="functionContainer">
   <a target="_blank" href="http://api.team-esprit.fr/api/getLastFichesByChantier/1">getLastFichesByChantier($id_du_chantier)</a>
   <div class="entry-summary">
    <p>Permet d'obtenir la liste des 5 dernières fiches du chantier correspondant à l'id</p>
  </div><!-- .entry-summary -->
</div> 
<div class="functionContainer">
   <a target="_blank" href="http://api.team-esprit.fr/api/getLastFichesByChantierAndChef/1/1">getLastFichesByChantierAndChef($id_du_chantier, $id_du_chef)</a>
   <div class="entry-summary">
    <p>Permet d'obtenir la liste des 5 dernières fiches par rapport à un chantier et un chef donné</p>
  </div><!-- .entry-summary -->
</div> 
<div class="functionContainer">
   <a target="_blank" href="http://api.team-esprit.fr/api/getFicheDetails/1/1/2014-04-30">getFicheDetails($id_du_chantier,$id_du_chef,$jour)</a>
   <div class="entry-summary">
    <p>Renvois le details des fiches à un chantier, un chef et un jour donnés</p>
  </div><!-- .entry-summary -->
</div> 
<div class="functionContainer">
   <a target="_blank" href="http://api.team-esprit.fr/api/getEmployeById/1">getEmployeById($id_employé)</a>
   <div class="entry-summary">
    <p>Permet d'obtenir le détails d'un employé correspondant à l'id donné</p>
  </div><!-- .entry-summary -->
</div> 
<div class="functionContainer">
   <a target="_blank" href="http://api.team-esprit.fr/api/getEmployees">getEmployees()</a>
   <div class="entry-summary">
    <p>Permet d'obtenir la liste de tous les employés (non intérimaire)</p>
  </div><!-- .entry-summary -->
</div> 
<div class="functionContainer">
   <a target="_blank" href="http://api.team-esprit.fr/api/getInterims">getInterims()</a>
   <div class="entry-summary">
    <p>Permet d'obtenir la liste de tous les intérimaires</p>
  </div><!-- .entry-summary -->
</div> 
<div class="functionContainer">
   <a target="_blank" href="http://api.team-esprit.fr/api/getFiches">getFiches()</a>
   <div class="entry-summary">
    <p>Permet d'obtenir la listes de toutes les fiches</p>
  </div><!-- .entry-summary -->
</div> 
<div class="functionContainer">
   <a target="_blank" href="http://api.team-esprit.fr/api/getContactAdmin">getContactAdmin()</a>
   <div class="entry-summary">
    <p> Recupère la liste de tous les contacts de l'administration </p>
  </div><!-- .entry-summary -->
</div> 
<div class="functionContainer">
   <a target="_blank" href="http://api.team-esprit.fr/api/setFiche">setFiche()</a>
   <div class="entry-summary">
    <p> Permet de créer une fiche </p>
    <p> Attent un JSON en POST de l'objet fiche </p>
  </div><!-- .entry-summary -->
</div> 
