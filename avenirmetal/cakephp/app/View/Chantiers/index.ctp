<div class="row bloc-am">
    <div class="col-sm-12 bloc-min">
    <h4>CHANTIERS</h4>
    <?php 
        echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> Ajouter un Chantier', array('action'=>'add'), array('class'=>'btn btn-success btn-blocked-su','escape'=>false)); 
        $this->Html->addCrumb('Chantiers', '/Chantiers');
        //$this->Html->addCrumb('Connexion', '/users/login');
    ?>
    <hr class="hrw">
        <div class="table-responsive">
          <table class="table table-bordered table-hover avenir-datatable ">
            <thead> 
              <tr>
                <th>Informations</th>
                <th>A propos</th>
                <th>Statut</th>
                <th class="text-center">Fiche</th>
                <th class="text-center">EDITER</th>
                <th class="text-center">Effacer</th>
              </tr>
            </thead>
            <tbody>
            <?php
            // echo $chantier['chantier']['id'];
            // echo '</td><td>';
            // echo '<br/>';

            foreach ($chantiers as $chantier) { 
                echo '<tr><td>';
                    echo '<span style="font-weight:800;">';
                    echo $chantier['Chantier']['num'];
                    echo ' ';
                    echo $chantier['Chantier']['nom'];
                    echo '</span>';
                    $tel = $chantier['Chantier']['tel'];

                    if($tel!=null){
                    echo ' (';
                    echo substr($tel, 0,2).'.'.substr($tel, 2,2).'.'.substr($tel, 4, 2).'.'.substr($tel, 6, 2).'.'.substr($tel, 8, 2);
                    echo ')';
                    } 

                    if(($chantier['Chantier']['adresse']!=null)&&($chantier['Chantier']['ville']!=null)){
                        echo '<br/>';
                        echo $chantier['Chantier']['adresse'];
                        if(isset($chantier['Chantier']['cp'])){
                            echo ' - ';
                            echo $chantier['Chantier']['cp'];
                        }
                        if(isset($chantier['Chantier']['ville'])){
                            echo ' - ';
                            echo $chantier['Chantier']['ville'];
                        }
                    }
                    echo '</td><td>';
                    echo $chantier['Chantier']['commentaire'];
               
                echo '</td><td>';
                switch ($chantier['Chantier']['clos']) {
                    case 1:
                        echo '<span style="font-weight:800;">Terminé</span><br/>';
                        break;
                    default:
                        echo '<span style="font-weight:800;">En cours</span><br/>';
                        break;
                }  
                echo '</td><td class="text-center">';            
                echo $this->Html->link('<i class="glyphicon glyphicon-eye-open"></i>', array('controller'=>'Fiches', 'action'=>'chantier', $chantier['Chantier']['id']), array('escape'=>false));
                echo '</td><td class="text-center">';            
                echo $this->Html->link('<i class="glyphicon glyphicon-pencil"></i>', array('action'=>'edit', $chantier['Chantier']['id']), array('escape'=>false));
                echo '</td><td class="text-center">';
                echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>', array('action' => 'delete', $chantier['Chantier']['id']), array('confirm' => 'Etes vous sure?','escape'=>false));
                echo '</td></tr>';
              }
              ?>
            </tbody>
          </table>
        </div></div></div>
        



        <div class="row bloc-am"><div class="col-sm-12 bloc-min">
        <h4>ASSIGNATIONS AUX CHANTIERS</h4>
            <?php 
                echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> Assigner un chef a un chantier', array('controller'=>'ChantiersHasChef', 'action'=>'add'), array('class'=>'btn btn-success btn-blocked-su','escape'=>false)); 
                //$this->Html->addCrumb('Connexion', '/users/login');
            ?>
        <hr class="hrw">

        <div class="table-responsive">
          <table class="table table-bordered table-hover avenir-datatable ">
            <thead> 
              <tr>
                <th>Chantier</th>
                <th>Chef de chantier</th>
                <th class="text-center">Effacer</th>
              </tr>
            </thead>
            <tbody>
            <?php
            // echo $chantier['chantier']['id'];
            // echo '</td><td>';
            // echo '<br/>';
            // debug($ChantiersHasChef);
            // die();
            //var_dump($ChantiersHasChefF);
            foreach ($ChantiersHasChefF as $chantiersHasChef) { 
                echo '<tr><td>';
                    echo '<span style="font-weight:800;">';
                    echo $chantiersHasChef['Chantier']['num'];
                    echo ' ';
                    echo $chantiersHasChef['Chantier']['nom'];
                    echo '</span>';
                echo '</td><td>';
                    echo '<span style="font-weight:800;">';
                    echo $chantiersHasChef['Chef']['Employe']['nom'];
                    echo ' ';
                    echo $chantiersHasChef['Chef']['Employe']['prenom'];
                    echo '</span>';
                echo '</td><td class="text-center">';
                echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>', array('controller'=>'ChantiersHasChef', 'action' => 'delete', 
                    $chantiersHasChef['ChantiersHasChef']['chantier_id'],$chantiersHasChef['ChantiersHasChef']['chef_id']), 
                    array('confirm' => 'Etes vous sure?','escape'=>false));
                echo '</td></tr>';
              }
              ?>
            </tbody>
          </table>
        </div>

    
</div></div>



























<div class="row bloc-am">
    <div class="col-sm-12 bloc-min">
    <h4>CHEFS</h4>
    <?php 
        echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> Ajouter un Chef', array('controller'=>'Chefs','action'=>'add'), array('class'=>'btn btn-success btn-blocked-su','escape'=>false)); 
        //$this->Html->addCrumb('Administrateurs', '/Users');
    ?>
  <hr class="hrw">
  <div class="table-responsive">
    <table class="table table-bordered table-hover avenir-datatable ">
      <thead>
        <tr>
          <th>Nom de Compte</th>
          <th>Fiche</th>
          <th class="text-center">Supprimer</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($Chefs as $user) {
          echo '<tr><td><span style="font-weight:800;">';
          echo $user['Chef']['username'];
          echo '</span></td><td class="text-center">';
          echo $this->Html->link('<i class="glyphicon glyphicon-eye-open"></i>', array('controller'=>'Fiches', 'action' => 'chef', $user['Chef']['id']), array('escape'=>false));
          echo '</span></td><td class="text-center">';
          echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>', array('controller'=> 'Chefs', 'action' => 'delete', $user['Chef']['id']), array('confirm' => 'Etes vous sure?','escape'=>false));
          echo '</td></tr>';
        }
        ?>
      </tbody>
    </table>
    
    </div>

  </div>
   <!-- <p style="padding:15px;">
    Ceci est la page des accès à l'Interface Web du site. Il est préférable d'en avoir un par personne.<br/>
    Vous pouvez en créer autant que vous le souhaitez, vous devriez cependant supprimer ceux inutilisé.<br/>
    Les accès comporte simplement un nom de compte et un mot de passe.
    </p> -->
</div>




<style type="text/css">
    h4.white {
        color:white;
    }
    hr.hrtr{
        border-top:rgb(87, 153, 250);
    }
    .tickLabel {
        color:white;
        font-size: 15px;
        font-weight: 100;
    }
    div#placeholder3 div.legend table {
        height: 150px;
    }
    div#placeholder3 div.legend table tbody tr td.legendLabel {
        font-size: 20px;
        width:250px;
        text-align: left;
        padding-left:20px;
    }
    div#placeholder3 div.legend table tbody tr td.legendColorBox div {
        border:none !important;
    }

    div#placeholder3 div.legend table tbody tr:first-child td.legendColorBox div:first-child div {
        background: rgb(200, 200, 255);
        border-radius:5px;
        height:35px !important;
        width: 35px !important;
    }
    div#placeholder3 div.legend table tbody tr:last-child td.legendColorBox div:first-child div {
        background: rgb(80, 80, 255);
        border-radius:5px;
        height:35px !important;
        width: 35px !important;
    }  
    .table-bordered>tbody>tr>td {
        vertical-align: middle;
    }
</style>