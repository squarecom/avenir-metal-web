<h2 class="sub-header">Employes</h2>
<?php echo $this->Html->link('Ajouter un Administrateur', array('action'=>'add'), array('class'=>'btn btn-success')); ?>
<hr>
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover avenir-datatable">
    <thead>
      <tr>
        <th>Identifiant</th>
        <th>Nom de l'employé</th>
        <th>Prenom</th>
        <th>Téléphone</th>
        <th>Interim</th>
        <th>Adresse</th>
        <th>Commentaire</th>
        <th>Editer</th>
        <th>Supprimer</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach ($employes as $employe) { 
        echo '<tr><td>';
        echo $employe['Employe']['id'];
        echo '</td><td>';
        echo $employe['Employe']['nom'];
        echo '</td><td>';
        echo $employe['Employe']['prenom'];
        echo '</td><td>';
        echo $employe['Employe']['tel'];
        echo '</td><td>';
        $interim = $employe['Employe']['interim'];
        switch ($interim) {
            case 0:
                echo 'Non';
                break;
            default:
            echo 'Oui';
                break;
        }
        echo '</td><td>';
        $s = $employe['Employe']['adresse'];
        $s=iconv("ISO-8859-1","UTF-8",$s);
        echo $s;
        echo '</td><td>';
        echo $employe['Employe']['commentaire'];
        echo '</td><td>';
        echo $this->Html->link('<i class="glyphicon glyphicon-pencil"></i>', array('action'=>'edit', $employe['Employe']['id']), array('escape'=>false));
        echo '</td><td>';
        echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>', array('action' => 'delete', $employe['Employe']['id']), array('confirm' => 'Etes vous sure?','escape'=>false));
        echo '</td></tr>';
      }
      ?>
    </tbody>
  </table>
</div>