<div class="row bloc-am">
	<div class="col-sm-12 bloc-min" style="color:#7C7C7C;">
		<h4>RECAPITULATIF</h4> 
		<hr class="hrw">
		<div class="row text-center">

			<?php if(isset($fiches)) { ?>
			<div class="col-sm-2" style="border-radius:4px; color:white; background-color:rgb(102, 209, 201);">
				<hr><span class="titlerecap"><?php echo $fiches; ?></span><br/>Fiche(s)<hr>
			</div>
			<div class="col-sm-1"></div>
			<?php } ?>

			<?php if(isset($fiches) && isset($employes)) { 
				$nb = ($employes-$fiches);
				if($nb==0){
					?>
				<div class="col-sm-6" style="border-radius:4px; color:white; background-color:rgb(120, 250, 120);">
				<hr><span style="font-size:38px;"> OK </span><br/><hr>
			</div>
			<div class="col-sm-1"></div>
			<?php
				} else {
			?>
			<div class="col-sm-6" style="border-radius:4px; color:white; background-color:rgb(250, 80, 87);">
				<hr><span class="titlerecap">
				<?php 
				echo $nb;
				?>
				</span><br/>Manquante(s)<hr>
			</div>
			<div class="col-sm-1"></div>
			<?php }} ?>
			<?php if(isset($employes)) { ?>
			<div class="col-sm-2" style="border-radius:4px; color:white; background-color:rgb(87, 153, 250);">
				<hr><span class="titlerecap"><?php echo $employes; ?></span><br/>Employe(s)<hr>
			</div>
			<?php } ?>


			



		</div><div class="row text-center"><hr class="hrw">
		<div class="col-sm-1"></div>
		<?php if(isset($agences)) { ?>
			<div class="col-sm-2" style="border-radius:4px; ">
				<!-- color:white; background-color:rgb(87, 153, 250);"> -->
				<hr><span style="font-size:25px;"><?php echo $agences; ?></span><br/>Agence(s)<hr>
			</div>
			<?php } ?>
			<?php if(isset($chefs)) { ?>
			<div class="col-sm-2" style="border-radius:4px; ">
			<!-- color:white; background-color:rgb(87, 153, 250);"> -->
				<hr><span class="titlerecap"><?php echo $chefs; ?></span><br/>Chef(s)<hr>
			</div>
			<?php } ?>
			<?php if(isset($chantiers)) { ?>
			<div class="col-sm-2" style="border-radius:4px; ">
			<!-- color:white; background-color:rgb(87, 153, 250);"> -->
				<hr><span class="titlerecap"><?php echo $chantiers; ?></span><br/>Chantier(s)<hr>
			</div>
			<?php } ?>
			<?php if(isset($users)) { ?>
			<div class="col-sm-2" style="border-radius:4px; ">
			<!-- color:white; background-color:rgb(87, 153, 250);"> -->
				<hr><span class="titlerecap"><?php echo $users; ?></span><br/>Administrateur(s)<hr>
			</div>
			<?php } ?>
			<?php if(isset($contacts)) { ?>
			<div class="col-sm-2" style="border-radius:4px; ">
			<!-- color:white; background-color:rgb(87, 153, 250);"> -->
				<hr><span class="titlerecap"><?php echo $contacts; ?></span><br/>Contact(s)<hr>
			</div>
			<?php } ?>
			<div class="col-sm-1"></div>
			
		</div>
	</div> 
</div>


<style type="text/css">
	.titlerecap {
		font-size:25px;
		font-weight: 800;
	}
	h4.white {
		color:white;
	}
	hr.hrtr{
		border-top:rgb(87, 153, 250);
	}
	.tickLabel {
		color:white;
		font-size: 15px;
		font-weight: 100;
	}
</style>