<div class="row bloc-am">
    <div class="col-sm-12 bloc-min">
    <h4>ADMINS</h4>
    <?php 
        echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> Ajouter un Administrateur', array('action'=>'add'), array('class'=>'btn btn-success btn-blocked-su','escape'=>false)); 
        $this->Html->addCrumb('Administrateurs', '/Users');
    ?>
  <hr class="hrw">
  <div class="table-responsive">
    <table class="table table-bordered table-hover avenir-datatable ">
      <thead>
        <tr>
          <th>Identifiant</th>
          <th>Nom de Compte</th>
          <th class="text-center">Editer</th>
          <th class="text-center">Supprimer</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($users as $user) {
          echo '<tr><td>';
          echo $user['User']['id'];
          echo '</td><td><span style="font-weight:800;">';
          echo $user['User']['username'];
          echo '</span></td><td class="text-center">';
          echo $this->Html->link('<i class="glyphicon glyphicon-pencil"></i>', array('action'=>'edit', $user['User']['id']), array('escape'=>false));
          echo '</td><td class="text-center">';
          echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>', array('action' => 'delete', $user['User']['id']), array('confirm' => 'Etes vous sure?','escape'=>false));
          echo '</td></tr>';
        }
        ?>
      </tbody>
    </table>
    
    </div>

  </div>
   <!-- <p style="padding:15px;">
    Ceci est la page des accès à l'Interface Web du site. Il est préférable d'en avoir un par personne.<br/>
    Vous pouvez en créer autant que vous le souhaitez, vous devriez cependant supprimer ceux inutilisé.<br/>
    Les accès comporte simplement un nom de compte et un mot de passe.
    </p> -->
</div>