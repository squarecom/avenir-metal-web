<h2 class="sub-header">Chantier</h2>
<button class="btn btn-success">Ajouter un Chantier</button>
<hr>
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover avenir-datatable">
    <thead>
      <tr>
        <th>#</th>
        <th>Ville</th>
        <th>Numéro</th>
        <th>Adresse</th>
        <th>Commentaire</th>
        <th>Editer</th>
        <th>Supprimer</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Paris</td>
        <td>01</td>
        <td>98 Allée des Baies, Paris 75000</td>
        <td></td>
        <td><i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>2</td>
        <td>Paris</td>
        <td>02</td>
        <td>78 Rue des Baieyass, Paris 75000</td>
        <td>Chantier terminé le 5 avril</td>
        <td> <i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>3</td>
        <td>Lyon</td>
        <td>01</td>
        <td>64 Allée des Monti, Lyon 69500</td>
        <td></td>
        <td> <i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>4</td>
        <td>Lyon</td>
        <td>02</td>
        <td>12 Allée des Baies, Lyon 69500</td>
        <td></td>
        <td> <i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>5</td>
        <td>Lyon</td>
        <td>03</td>
        <td>111 Chemin des malapier, Lyon 69500</td>
        <td></td>
        <td> <i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>6</td>
        <td>Grenoble</td>
        <td>01</td>
        <td>4 Rue des Saint-Jarmin, Grenoble 38000</td>
        <td>Commencé le 3 Mars - 6 mois minimum</td>
        <td> <i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>7</td>
        <td>Grenoble</td>
        <td>01</td>
        <td>45 Chemin des Baies, Grenoble 38000</td>
        <td></td>
        <td> <i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
    </tbody>
  </table>
</div>
<br><br>
<h2 class="sub-header">Chef de Chantier</h2>
<button class="btn btn-success">Ajouter un chef de chantier</button>
<hr>
<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>#</th>
        <th>Chef</th>
        <th>Chantier</th>
        <th>Numéro</th>
        <th>Supprimer</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Paul Dupont</td>
        <td>Paris</td>
        <td>01</td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>2</td>
        <td>Antoine Chat</td>
        <td>Paris</td>
        <td>02</td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>3</td>
        <td>Francois Dupier</td>
        <td>Lyon</td>
        <td>01</td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>4</td>
        <td>Jean Poulet</td>
        <td>Lyon</td>
        <td>02</td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
    </tbody>
  </table>
</div>