<div class="row bloc-am">
    <div class="col-sm-12 bloc-min">
    <h3>RESULTATS</h3>
    <?php
    $this->Html->addCrumb('Recherche', '/users/search');

    echo '</div></div>'; 

    $jour = array('Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche');
    $mois = array('Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Décembre');
    ?>








<div class="row bloc-am">
        <div class="col-sm-12 bloc-min">
    <h4>EMPLOYES</h4>
    <?php 
        echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> Ajouter un Employé', array('controller'=>'employes','action'=>'add'), array('class'=>'btn btn-success btn-blocked-su','escape'=>false)); 
        //$this->Html->addCrumb('Connexion', '/users/login');
    ?>
    <hr class="hrw">
        <div class="table-responsive"> 
          <table class="table table-bordered table-hover avenir-datatable ">
            <thead> 
              <tr>
                <th>Informations</th>
                <th>A propos</th>
                <th class="text-center">fiche</th>
                <th class="text-center">EDITER</th>
                <th class="text-center">Effacer</th>
              </tr>
            </thead>
            <tbody>
            <?php
            // echo $employe['Employe']['id'];
            // echo '</td><td>';
            // echo '<br/>';

            foreach ($employes as $employe) { 
                echo '<tr><td>';
                    echo '<span style="font-weight:800;">';
                    echo $employe['Employe']['nom'];
                    echo ' ';
                    echo $employe['Employe']['prenom'];
                    echo '</span> (';
                    $tel = $employe['Employe']['tel'];
                    echo substr($tel, 0,2).'.'.substr($tel, 2,2).'.'.substr($tel, 4, 2).'.'.substr($tel, 6, 2).'.'.substr($tel, 8, 2);
                    echo ')';
                    echo '<br/>';
                    echo $employe['Employe']['adresse'];
                    echo ' - ';
                    echo $employe['Employe']['ville'];
                echo '</td><td>';
                $interim = $employe['Employe']['interim'];
                switch ($interim) {
                    case 1:
                        echo '<span style="font-weight:800;">Interimaire</span>';
                        break;
                }
                if(isset($employe['Agence']['nom'])){
                    echo ' de '.$employe['Agence']['nom'].'.';
                } 
                if((isset($employe['Agence']['nom']))||isset($employe['Agence']['interim'])){
                    echo '<br/>';
                }                  
                echo $employe['Employe']['commentaire'];
                echo '</td><td class="text-center">';
                echo $this->Html->link('<i class="glyphicon glyphicon-eye-open"></i>', array('controller'=>'Fiches', 'action'=>'employe', $employe['Employe']['id']), array('escape'=>false));
                echo '</td><td class="text-center">';
                echo $this->Html->link('<i class="glyphicon glyphicon-pencil"></i>', array('action'=>'edit', $employe['Employe']['id']), array('escape'=>false));
                echo '</td><td class="text-center">';
                echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>', array('action' => 'delete', $employe['Employe']['id']), array('confirm' => 'Etes vous sure?','escape'=>false));
                echo '</td></tr>';
              }
              ?>
            </tbody>
          </table>
        </div>

    </div>
    </div>


































    <div class="row bloc-am">
    <div class="col-sm-12 bloc-min">
    <h4>AGENCES</h4>
    <?php 
        echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> Ajouter une Agence', array('controller'=>'agences','action'=>'add'), array('class'=>'btn btn-success btn-blocked-su','escape'=>false)); 
        //$this->Html->addCrumb('Connexion', '/users/login');
    ?>
    <hr class="hrw">
        <div class="table-responsive">
          <table class="table table-bordered table-hover avenir-datatable ">
            <thead> 
              <tr>
                <th>Informations</th>
                <th>A propos</th>
                <th>EDITER</th>
                <th>Effacer</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($agences as $agence) { 
                echo '<tr><td>';
                    echo '<span style="font-weight:800;">';
                    echo $agence['Agence']['nom'];
                    echo '</span> (';
                    $tel = $agence['Agence']['tel'];
                    echo substr($tel, 0,2).'.'.substr($tel, 2,2).'.'.substr($tel, 4, 2).'.'.substr($tel, 6, 2).'.'.substr($tel, 8, 2);
                    echo ' - ';
                    echo $agence['Agence']['email'];
                    echo ')';
                    echo '<br/>';
                    echo $agence['Agence']['adresse'];
                    echo ' - ';
                    echo $agence['Agence']['ville'];
                echo '</td><td>';
                echo $agence['Agence']['commentaire'];
                echo '</td><td>';
                echo $this->Html->link('<i class="glyphicon glyphicon-pencil"></i>', array('action'=>'edit', $agence['Agence']['id']), array('escape'=>false));
                echo '</td><td>';
                echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>', array('action' => 'delete', $agence['Agence']['id']), array('confirm' => 'Etes vous sure?','escape'=>false));
                echo '</td></tr>';
              }
              ?>
            </tbody>
          </table>
        </div>

    </div>
</div>





















<div class="row bloc-am">
    <div class="col-sm-12 bloc-min">
    <h4>CHANTIERS</h4>
    <?php 
        echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> Ajouter un Chantier', array('controller'=>'chantiers','action'=>'add'), array('class'=>'btn btn-success btn-blocked-su','escape'=>false)); 
        //$this->Html->addCrumb('Connexion', '/users/login');
    ?>
    <hr class="hrw">
        <div class="table-responsive">
          <table class="table table-bordered table-hover avenir-datatable ">
            <thead> 
              <tr>
                <th>Informations</th>
                <th>A propos</th>
                <th class="text-center">fiche</th>
                <th class="text-center">EDITER</th>
                <th class="text-center">Effacer</th>
              </tr>
            </thead>
            <tbody>
            <?php
            // echo $chantier['chantier']['id'];
            // echo '</td><td>';
            // echo '<br/>';

            foreach ($chantiers as $chantier) { 
                echo '<tr><td>';
                    echo '<span style="font-weight:800;">';
                    echo $chantier['Chantier']['num'];
                    echo ' ';
                    echo $chantier['Chantier']['nom'];
                    echo '</span> (';
                    $tel = $chantier['Chantier']['tel'];
                    echo substr($tel, 0,2).'.'.substr($tel, 2,2).'.'.substr($tel, 4, 2).'.'.substr($tel, 6, 2).'.'.substr($tel, 8, 2);
                    echo ')';
                    echo '<br/>';
                    echo $chantier['Chantier']['adresse'];
                    echo ' - ';
                    echo $chantier['Chantier']['ville'];
                echo '</td><td>';
                switch ($chantier['Chantier']['clos']) {
                    case 1:
                        echo '<span style="font-weight:800;">Terminé</span>';
                        break;
                }              
                echo $chantier['Chantier']['commentaire'];
                echo '</td><td class="text-center">';
                echo $this->Html->link('<i class="glyphicon glyphicon-eye-open"></i>', array('controller'=>'fiches', 'action'=>'employe', $chantier['Chantier']['id']), array('escape'=>false));
                echo '</td><td class="text-center">';
                echo $this->Html->link('<i class="glyphicon glyphicon-pencil"></i>', array('action'=>'edit', $chantier['Chantier']['id']), array('escape'=>false));
                echo '</td><td class="text-center">';
                echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>', array('action' => 'delete', $chantier['Chantier']['id']), array('confirm' => 'Etes vous sure?','escape'=>false));
                echo '</td></tr>';
              }
              ?>
            </tbody>
          </table>
        </div></div></div>














<div class="row bloc-am">
    <div class="col-sm-12 bloc-min">
    <h4>ADMINS</h4>
    <?php 
        echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> Ajouter un Administrateur', array('controller'=>'users','action'=>'add'), array('class'=>'btn btn-success btn-blocked-su','escape'=>false))
    ?>
  <hr class="hrw">
  <div class="table-responsive">
    <table class="table table-bordered table-hover avenir-datatable ">
      <thead>
        <tr>
          <th>Identifiant</th>
          <th>Nom de Compte</th>
          <th>Editer</th>
          <th>Supprimer</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($users as $user) {
          echo '<tr><td>';
          echo $user['User']['id'];
          echo '</td><td>';
          echo $user['User']['username'];
          echo '</td><td>';
          echo $this->Html->link('<i class="glyphicon glyphicon-pencil"></i>', array('action'=>'edit', $user['User']['id']), array('escape'=>false));
          echo '</td><td>';
          echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>', array('action' => 'delete', $user['User']['id']), array('confirm' => 'Etes vous sure?','escape'=>false));
          echo '</td></tr>';
        }
        ?>
      </tbody>
    </table>
    
    </div>

  </div>

</div>













<div class="row bloc-am">
    <div class="col-sm-12 bloc-min">
    <h4>CONTACTS</h4>
    <?php 
        echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> Ajouter une contact', array('controller'=>'contacts','action'=>'add'), array('class'=>'btn btn-success btn-blocked-su','escape'=>false)); 
    ?>
    <hr class="hrw">
        <div class="table-responsive">
          <table class="table table-bordered table-hover avenir-datatable ">
            <thead> 
              <tr>
                <th>Informations</th>
                <th>A propos</th>
                <th>EDITER</th>
                <th>Effacer</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($contacts as $contact) { 
                echo '<tr><td>';
                    echo '<span style="font-weight:800;">';
                    echo $contact['Contact']['prenom'];
                    echo ' ';
                    echo $contact['Contact']['nom'];
                    echo '</span> (';
                    $tel = $contact['Contact']['tel'];
                    echo substr($tel, 0,2).'.'.substr($tel, 2,2).'.'.substr($tel, 4, 2).'.'.substr($tel, 6, 2).'.'.substr($tel, 8, 2);
                    echo ' - ';
                    echo $contact['Contact']['email'];
                    echo ')';
                    echo '<br/>';
                    echo $contact['Contact']['adresse'];
                    echo ' - ';
                    echo $contact['Contact']['ville'];
                echo '</td><td>';
                echo $contact['Contact']['commentaire'];
                echo '</td><td>';
                echo $this->Html->link('<i class="glyphicon glyphicon-pencil"></i>', array('action'=>'edit', $contact['Contact']['id']), array('escape'=>false));
                echo '</td><td>';
                echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>', array('action' => 'delete', $contact['Contact']['id']), array('confirm' => 'Etes vous sure?','escape'=>false));
                echo '</td></tr>';
              }
              ?>
            </tbody>
          </table>
        </div>

    </div>
</div>
<div style="clear:both;"></div>


















<style type="text/css">
    h4.white {
        color:white;
    }
    hr.hrtr{
        border-top:rgb(87, 153, 250);
    }
    .tickLabel {
        color:white;
        font-size: 15px;
        font-weight: 100;
    }
    div#placeholder3 div.legend table {
        height: 150px;
    }
    div#placeholder3 div.legend table tbody tr td.legendLabel {
        font-size: 20px;
        width:250px;
        text-align: left;
        padding-left:20px;
    }
    div#placeholder3 div.legend table tbody tr td.legendColorBox div {
        border:none !important;
    }

    div#placeholder3 div.legend table tbody tr:first-child td.legendColorBox div:first-child div {
        background: rgb(200, 200, 255);
        border-radius:5px;
        height:35px !important;
        width: 35px !important;
    }
    div#placeholder3 div.legend table tbody tr:last-child td.legendColorBox div:first-child div {
        background: rgb(80, 80, 255);
        border-radius:5px;
        height:35px !important;
        width: 35px !important;
    }  
    .table-bordered>tbody>tr>td {
        vertical-align: middle;
    }
</style>