<div class="row bloc-am">
<div class="col-sm-12 bloc-min">
<h4>MODIFIER UN ADMINISTRATEUR</h4>
<?php 
    echo $this->Html->link('<i class="glyphicon glyphicon-arrow-left"></i> Retourner à l\'écran précédént',
     array('action'=>'index'), array('class'=>'btn btn-danger btn-blocked-dg', 'escape'=>false));
    $this->Html->addCrumb('Administrateurs', '/Users');
    $this->Html->addCrumb('Ajouter', '/Users/add');
?>
<hr class="hrw">

    <?php
    echo $this->Form->create('User', array('class'=>'form-horizontal','type'=>'file', 'action' => 'edit')); ?>

        <?php 

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('User.username', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Pseudo*'),
                'placeholder'   => '(Exemple) j.dupont',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('User.password', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Mot de passe*'),
                'type'          => 'password',
                'placeholder'   => '(Exemple) avenir38mdp',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    $options = array(
        'label' => 'Enregister',
        'div' => array(
            'class' => 'col-sm-9 col-md-9 col-lg-10',
        ),
        'before'        => '',
        'after'         => '</div>',
        'class'         =>'btn btn-success btn-success-my btn-block',
    );
    echo '<div class="form-group"><label class="col-sm-3 col-md-3 col-lg-2 control-label"></label>';
    echo $this->Form->end($options);
    echo '</div></div>';