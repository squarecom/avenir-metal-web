<h2 class="sub-header">Fiches</h2>
<a href="./fiches" class="btn btn-info">Trier par jour</a>
<a href="./fichescha" class="btn btn-info">Trier par chantier</a>
<a href="./fichesche" class="btn btn-info">Trier par chef</a>
<a href="./fichesadd" class="btn btn-success">Ajouter une Fiche</a>
<br><br>
<div class="">
<select class="form-control" name="" id="">
  <option value="0">Arnaud Lameche</option> 
  <option value="1">Bruno Mens</option>
  <option value="2">David Ken</option>
</select>
</div>

<h3 class="sub-header">Francois Montin</h3>
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover avenir-datatable">
    <thead>
      <tr>
        <th>#</th>
        <th>Chantier</th>
        <th>Chef</th>

        <th>Date</th>
        <th>Début</th>
        <th>Fin</th>
        <th>Grand Deplacement</th>
        <th>Repas</th>
        <th>Commentaire</th>

        <th>Editer</th>
        <th>Supprimer</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Paris 01</td>
        <td>Paul Dumont</td>

        <td>Mercredi 2 Avril</td>
        <td>9h00</td>
        <td>18h00</td>
        <td>Oui</td>
        <td>Oui</td>
        <td></td>

        <td><i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>1</td>
        <td>Paris 01</td>
        <td>Paul Dumont</td>

        <td>Mardi 1 Avril</td>
        <td>9h00</td>
        <td>18h00</td>
        <td>Oui</td>
        <td>Oui</td>
        <td></td>

        <td><i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>1</td>
        <td>Paris 01</td>
        <td>Paul Dumont</td>

        <td>Lundi 31 Mars</td>
        <td>9h00</td>
        <td>18h00</td>
        <td>Oui</td>
        <td>Oui</td>
        <td></td>

        <td><i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>1</td>
        <td>Lyon 03</td>
        <td>Henry Latuile</td>

        <td>Vendredi 28 Mars</td>
        <td>8h00</td>
        <td>17h00</td>
        <td>Non</td>
        <td>Oui</td>
        <td></td>

        <td><i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>1</td>
        <td>Lyon 03</td>
        <td>Henry Latuile</td>

        <td>Jeudi 27 Mars</td>
        <td>8h00</td>
        <td>17h00</td>
        <td>Non</td>
        <td>Oui</td>
        <td></td>

        <td><i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>1</td>
        <td>Lyon 03</td>
        <td>Henry Latuile</td>

        <td>Mercredi 26 Mars</td>
        <td>8h00</td>
        <td>17h00</td>
        <td>Non</td>
        <td>Oui</td>
        <td></td>

        <td><i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>1</td>
        <td>Lyon 03</td>
        <td>Henry Latuile</td>

        <td>Mardi 25 Mars</td>
        <td>8h00</td>
        <td>17h00</td>
        <td>Non</td>
        <td>Oui</td>
        <td></td>

        <td><i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>1</td>
        <td>Lyon 03</td>
        <td>Henry Latuile</td>

        <td>Lundi 24 Mars</td>
        <td>8h00</td>
        <td>17h00</td>
        <td>Non</td>
        <td>Oui</td>
        <td></td>

        <td><i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
    </tbody>
  </table>
</div>
