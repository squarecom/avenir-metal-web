<h2 class="sub-header">Employees</h2>
<button class="btn btn-primary">Ajouter un Employés</button>
<hr>
<div class="table-responsive">
  <table class="table table-bordered table-striped table-hover avenir-datatable">
    <thead>
      <tr>
        <th>#</th>
        <th>Nom</th>
        <th>Prenom</th>
        <th>Telephone</th>
        <th>Interim</th>
        <th>Adresse</th>
        <th>Commentaire</th>
        <th>Editer</th>
        <th>Supprimer</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Dupont</td>
        <td>Jean</td>
        <td>06-99-99-99-99</td>
        <td>Non</td>
        <td>64 Rue de la raie, Grenoble 38000</td>
        <td></td>
        <td><i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
      <tr>
        <td>2</td>
        <td>Manique</td>
        <td>Paul</td>
        <td>06-44-44-44-44</td>
        <td>Oui</td>
        <td>75 Allée du manoire, Voiron 38000</td>
        <td>stagiaire quelques mois</td>
        <td><i class="glyphicon glyphicon-pencil"></i></td>
        <td><i class="glyphicon glyphicon-trash"></i></td>
      </tr>
    </tbody>
  </table>
</div>