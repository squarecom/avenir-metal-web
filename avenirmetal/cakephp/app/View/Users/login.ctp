<div class="container" style="max-width:330px; padding:15px; margin:0 auto; box-shadow:4px 4px 4px #272727; background-color:white; border-top-left-radius:4px; border-top-right-radius:4px;">

<?php
    echo $this->Form->create('User', array('class'=>'form-signin')); 

    echo '<div style="text-align:center;">';
    echo $this->Html->image('am.png', array('style'=>'width:100%;'));
    //echo '<h2 class="form-signin-heading" style="margin-top:0;">Avenir Metal</h2>';
    echo '</div>';

    echo '<hr>';

    echo $this->Form->input('username', 
        array(  'label'         => false,
                'placeholder'   => 'Pseudonyme...',
                'div'           => false,
                'required'      => true,
                'autofocus'     => true,
                'class'         => 'form-control',
                'style'         => 'border-bottom-right-radius:0; border-bottom-left-radius:0; margin-bottom:-1px; '
        )
    );
    echo $this->Form->input('password', 
        array(  'label'         => false,
                'placeholder'   => 'Mot de passe..',
                'div'           => false,
                'type'          => 'password',
                'required'      => true,
                'class'         => 'form-control'
        )
    );

    echo '<hr>';

    //echo '<label class="checkbox"><input type="checkbox" value="remember-me"> Se rappeler de moi</label>';

    $options = array(
        'label' => 'Se connecter',
        'div'   => false,
        'type'  => 'submit',
        'value' => 'Se connecter',
        'class' => 'btn btn-lg btn-primary btn-block'
    );
    echo $this->Form->end($options);

    echo '</div><div style="box-shadow:4px 4px 4px #272727; background-color:rgb(85, 85, 105); color:rgb(182, 182, 182); max-width:330px; padding:15px; margin:0 auto;  border-bottom-left-radius:4px; border-bottom-right-radius:4px;">';
    echo 'Espace privé de l\'entreprise <u>Avenir Metal</u>, veuillez ne pas essayer de vous y connecter si vous n\'y êtes pas invité.';
    echo '</div><div style="color:white; max-width:330px; padding:0; margin:0 auto;">';
    echo '<div id="bsull"></div>';
    echo '</div>';
?>
<style type="text/css">
    input[type=submit] {
        background-image:none;
        border:none;
        background-color: rgb(14,117,188)
    }
    #bull {
        margin-left: 35px;
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 50px 50px 0 0;
        border-color: #555569 transparent transparent transparent;
        line-height: 0px;
        _border-color: #555569 #000000 #000000 #000000;
        _filter: progid:DXImageTransform.Microsoft.Chroma(color='#000000');
    }
</style>


