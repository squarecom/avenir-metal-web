    <?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Avenir Metal : Entreprise de TPE');
?>

<!DOCTYPE html>
<html>
<!-- HEADER -->
    <head>
        <title><?php echo $cakeDescription ?> (<?php echo $title_for_layout; ?>)</title>
        <?php echo $this->Html->charset(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <?php
           
        /////////////////////////////////// ICON //////////////////////////////////////////////

        echo $this->Html->meta('icon');
        echo $this->Html->meta('description','Avenir Metal Interface Web - Private Service');
        echo $this->Html->meta(array('name' => 'robots', 'content' => 'noindex, nofollow'));
        echo $this->Html->meta(array('name' => 'author', 'content' => 'SquareCom'));
        echo $this->Html->meta(array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8'));
        echo $this->Html->meta(array('name' => 'Content-Language', 'content' => 'fr'));
        echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
        echo $this->fetch('meta');

        /////////////////////////////////// CSS //////////////////////////////////////////////

        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('bootstrap-theme.min');
        echo $this->Html->css('font-awesome.min');
        //echo $this->Html->css('jquery-ui.css');
        echo $this->Html->css('dashboard.css');
        echo $this->Html->css('signin.css');
        echo $this->Html->css('jquery.datatables.css');
        echo $this->fetch('css');     
        ?>         

    </head>
    <body>
        <div class="navbar navbar-white navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3 col-md-2 " style="text-align:center; display:block; position:fixed; background-color:rgb(87, 153, 250); border-bottom:1px solid rgb(87, 153, 250);">
                        <a class="navbar-brand title_brand" href="#">Avenir Metal Web</a>
                    </div>
                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 bsed">
                        <div class="navbar-collapse collapse">
                            <div class="navbar-form navbar-right">
                            <?php
                            echo $this->Form->create('User', array('action'=>'search','type' => 'get', 'style'=> 'display:inline-block;'));

                            /* --------------------------------------------------------------------------------------------------------------------------------------------- */
                            echo $this->Form->input('User.valeur',
                                array(  'label'         => false,
                                        'div'   => false,
                                        'id'    => 'search',
                                        'placeholder'   => 'Rechercher...',
                                        'class'         => 'form-control ',
                                )
                            );
                            echo ' '; 
                            //echo '<div style="clear:both;"></div>';
                            /* --------------------------------------------------------------------------------------------------------------------------------------------- */
                            $options = array(
                                'label' => 'Rechercher',
                                'div'   => false,
                                'class'=>'btn btn-primary btn-primary-my'
                            );
                            echo $this->Form->end($options);
                            ?>



                        <!-- <input type="text" class="form-control" placeholder="Rechercher...">
                        <button class="btn btn-primary btn-primary-my"><i class="glyphicon glyphicon-search"></i> Rechercher</button>  -->




                        <?php echo $this->Html->link('<i class="glyphicon glyphicon-remove"></i> Déconnexion', array('controller'=>'Users', 'action'=>'logout'), array('class'=>'btn btn-danger btn-danger-my', 'escape'=>false)); ?>
                    </div>
                </div>

                </div></div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>



            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar"> 
                                <?php
                                $curr_act = $this->params['action'];
                                $curr_ctrl = $this->params['controller'];


                                echo '<li>';
                                if (($curr_ctrl=='users')&&($curr_act=='home')){
                                    echo $this->Html->link('<i class="glyphicon glyphicon-home"></i><span class="title_menu">Accueil</span>', array('controller'=>'users', 'action'=>'home'),
                                array('escape'=>false, 'class'=>'curr'));
                                } else {
                                    echo $this->Html->link('<i class="glyphicon glyphicon-home"></i><span class="title_menu">Accueil</span>', array('controller'=>'users', 'action'=>'home'),
                                array('escape'=>false));
                                }
                                echo '</li><li>';
                                if ($curr_ctrl=='chantiers'){
                                    echo $this->Html->link('<i class="glyphicon glyphicon-briefcase"></i><span class="title_menu">Chantiers</span>', array('controller'=>'chantiers', 'action'=>'index'),array('escape'=>false, 'class'=>'curr')); 
                                } else {
                                    echo $this->Html->link('<i class="glyphicon glyphicon-briefcase"></i><span class="title_menu">Chantiers</span>', array('controller'=>'chantiers', 'action'=>'index'),array('escape'=>false)); 
                                }
                                echo '</li><li>';
                                if (($curr_ctrl=='users')&&($curr_act!='home')){
                                    echo $this->Html->link('<i class="glyphicon glyphicon-dashboard"></i><span class="title_menu">Administrateurs</span>', array('controller'=>'users', 'action'=>'index'),array('escape'=>false, 'class'=>'curr'));
                                } else {
                                    echo $this->Html->link('<i class="glyphicon glyphicon-dashboard"></i><span class="title_menu">Administrateurs</span>', array('controller'=>'users', 'action'=>'index'),array('escape'=>false));
                                }
                                echo '</li><li>';
                                if ($curr_ctrl=='agences'){
                                    echo $this->Html->link('<i class="glyphicon glyphicon-credit-card"></i><span class="title_menu">Agences</span>', array('controller'=>'agences', 'action'=>'index'),
                                        array('class'=>'curr','escape'=>false)); 
                                } else {
                                    echo $this->Html->link('<i class="glyphicon glyphicon-credit-card"></i><span class="title_menu">Agences</span>', array('controller'=>'agences', 'action'=>'index'),
                                        array('escape'=>false)); 
                                }
                                echo '</li><li>';
                                if ($curr_ctrl=='contacts'){
                                    echo $this->Html->link('<i class="glyphicon glyphicon-star"></i><span class="title_menu">Contacts</span>', array('controller'=>'contacts', 'action'=>'index'),
                                        array('class'=>'curr','escape'=>false)); 
                                } else {
                                    echo $this->Html->link('<i class="glyphicon glyphicon-star"></i><span class="title_menu">Contacts</span>', array('controller'=>'contacts', 'action'=>'index'),
                                        array('escape'=>false)); 
                                }
                                echo '</li><li>';
                                if ($curr_ctrl=='employes'){
                                    echo $this->Html->link('<i class="glyphicon glyphicon-user"></i><span class="title_menu">Employés</span>', array('controller'=>'employes', 'action'=>'index'),
                                        array('class'=>'curr','escape'=>false)); 
                                } else {
                                    echo $this->Html->link('<i class="glyphicon glyphicon-user"></i><span class="title_menu">Employés</span>', array('controller'=>'employes', 'action'=>'index'),
                                        array('escape'=>false)); 
                                }
                                echo '</li><li>';
                                if ($curr_ctrl=='fiches'){
                                    echo $this->Html->link('<i class="glyphicon glyphicon-calendar"></i><span class="title_menu">Fiches</span>', array('controller'=>'fiches', 'action'=>'index'),
                                        array('escape'=>false, 'class'=>'curr')); 
                                } else {
                                    echo $this->Html->link('<i class="glyphicon glyphicon-calendar"></i><span class="title_menu">Fiches</span>', array('controller'=>'fiches', 'action'=>'index'),
                                        array('escape'=>false)); 
                                }
                                echo '</li>';
                                // echo '</li><li>';
                                // if ($curr_ctrl=='Stats'){
                                //     echo $this->Html->link('<i class="glyphicon glyphicon-stats"></i><span class="title_menu">Statistiques</span>', array('controller'=>'Fiches', 'action'=>'index'),
                                //         array('escape'=>false, 'class'=>'curr')); 
                                // } else {
                                //    echo $this->Html->link('<i class="glyphicon glyphicon-stats"></i><span class="title_menu">Statistiques</span>', array('controller'=>'Fiches', 'action'=>'index'),
                                //     array('escape'=>false)); 
                                // }
                                // echo '</li>';
                                ?>
                    </ul>
                </div>
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="background-color:#D6D6D6;">

                    <?php echo $this->Session->flash(); ?>

                    <div class="row bloc-am" style="margin-top:15px;">
                        <div class="col-sm-12 bloc-min nopadding">
                            <h5><i class="glyphicon glyphicon-home"></i>
                                <?php 
                                  echo $this->Html->getCrumbs(' > ', array(
                                      'text' => '<span class="title_menu_crumbs">Accueil</span>',
                                      'class' => 'tip-bottom ',
                                      'title' => 'Aller a l\'accueil',
                                      'escape' => false
                                  ));
                                ?>
                            </h5>
                        </div>
                    </div>
                    <?php 
                    echo $this->fetch('content'); 
                    //echo '<h1>Session User</h1>';
                    //debug($this->Session->read()); 
                    //echo $this->Session->read('Auth.User.username');
                    //echo $this->Session->read('Auth.User.role');
                    //echo $this->Html->script('excanvas.min.js'); // Graphique, Canvas,..
                    //echo $this->Html->script('jquery.min.js');
                    //echo $this->Html->script('jquery-ui.custom');
                    //echo $this->Html->script('jquery.nicescroll.min'); // Scrollbar personnalisé
                    ?>
                    <div style="clear:both;"></div>

                   <!-- <div class="row bloc-am">
                        <div class="col-sm-12 bloc-min">
                        </div> 
                    </div>-->
                    <div class="row bloc-am">
                        <div class="col-sm-12 bloc-min" style="color:white;">
                            <div class="row text-center">
                                <div id="footer" style="color:black;">
                                    &copy; Avenir Metal 2014 – 
                                    Developped by SquareCom
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                </div>
            </div>
        </div>
        <style type="text/css">
        .title_menu {
            margin-left:10px;
        }
        .title_menu_crumbs {
            margin-left:20px;
        }
        
        .nav-sidebar > li > a {
            padding: 20px;
        }
        a.title_brand {
            color:white;
            font-size: 20px;
        }
        .sidebar {
            /*top:50px;*/
            border:none;
            padding-top:0;
            background-color: #353242;
        }
        .sidebar li a {
            color:rgb(156, 156, 156);
        }
        .sidebar li a:hover {
            color:rgb(210, 210, 210);
            background-color: #423F4B;
        }
        .sidebar li a.curr {
            color:white;
            background-color: #5E5C66;
        }
        .navbar-white {
            background-color: white;
        }
        .bloc-am {
            background-color: white;
            margin-bottom:25px;
        }
        .bloc-min {
            padding:35px;
        }
        .nopadding {
            padding-bottom:0 !important;
            padding-top:0 !important;
        }
        .bsed {
            -webkit-box-shadow: 0px 3px 5px 0px rgba(50, 50, 50, 0.75);
            -moz-box-shadow:    0px 3px 5px 0px rgba(50, 50, 50, 0.75);
            box-shadow:         0px 3px 5px 0px rgba(50, 50, 50, 0.75);
        }
        h1,h2,h3,h4,h5 {
            color:#696969;
        }
        h5 a {
            color:#696969;
            font-weight: 800;
        }
        h5 a:hover {
            color:#8A8A8A;
            text-decoration: none;
        }

        /* ----------------------------------------------------------- */

        .btn-danger-my {
            border:none;
            background-image:none;
            background-color: #E7706C;

        }
        .btn-primary-my {
            border:none;
            background-image:none;
            background-color: #6094C2;

        }
        .btn-success-my {
            border:none;
            background-image:none;
            background-color: #5FAF5F;

        }

        /* ----------------------------------------------------------- */
        .btn-blocked-su {
            display:block;
            position: absolute;
            top: 35px;
            padding: 10px;
            right:35px;
            border:none;
            background-image:none;
            background-color: #5FAF5F;
        }
        .btn-blocked-dg {
            display:block;
            position: absolute;
            top: 35px;
            padding: 10px;
            right:35px;
            border:none;
            background-image:none;
            background-color: #E7706C;
        }
        hr.hrw {
            border-top-color: white;
        }

        /* ----------------------------------------------------------- */

        pre {
            border:none;
            background-color: #666;
            color:white;
        }
        pre select {
            color: #666;
        }
        .dataTables_filter input {
            border:1px solid #666;
        }
        .fa-b {
            font-size: 5em;
        }
        pre select{
            border-radius: 4px;
            /*background-color: #D9ECF8;*/
            height:100px;
        }
        table.avenir-datatable {
            /*border: 2px solid #353242;*/
        }
        table.avenir-datatable a{
            color:#333;
        }
        table.avenir-datatable thead tr th {
            text-transform: uppercase;
            /*background-color: rgb(0,0,0);*/
            /*background-color:#353242;*/    
            background-color:rgb(87, 153, 250);
            font-weight: 100;
            /*color:#8A8A8A;*/
            color:rgb(255,255,255);
        }
        table.avenir-datatable tfoot tr th {
            text-transform: uppercase;
            background-color:rgb(87, 153, 250);
            font-weight: 100;
            color:rgb(255,255,255);
            border-top:1px solid white;
        }


        /* -------------------------- */
        table.avenir-datatable thead tr th:first-child {
            border-top-left-radius: 4px;
        }
        table.avenir-datatable thead tr th:last-child {
            border-top-right-radius: 4px;
        }
        table.avenir-datatable tfoot tr th:first-child {
            border-bottom-left-radius: 4px;
        }
        table.avenir-datatable tfoot tr th:last-child {
            border-bottom-right-radius: 4px;
        }
        /* --------------------------- */


        table.avenir-datatable tbody tr:hover td {
            background-color:rgb(220,220,220);            
        }
        table.avenir-datatable thead tr th, table.avenir-datatable tbody tr td{
            border-bottom:none;
            border-right:1px solid #ddd;
            /*border-right:none;*/
            border-left:none;
        }
        table.avenir-datatable thead tr th:last-child, table.avenir-datatable tbody tr td:last-child {
            border-bottom:none;
            /*border:none;*/
        }

        </style>

        <?php  

        echo $this->Html->script('jquery-1.11.0.min');
        echo $this->Html->script('jquery-ui-1.10.4.custom.min');
        echo $this->Html->script('jquery.dataTables.min.js');

        // Graph
        //echo $this->Html->script('jquery.flot.js');
        //echo $this->Html->script('jquery.flot.categories.js');
        //echo $this->Html->script('jquery.flot.pie.js');

        echo $this->Html->script('bootstrap.min');    
        echo $this->Html->script('avenirmetal.js');
        echo $this->fetch('script');

        ?>

        <script type="text/javascript"> 

        // $('#search').autocomplete('<?php echo $this->Html->url('/', true)?>Users/results');  

        // $(function() {

        //     var data = [
        //         ["", 0],
        //         ["AOU", 4],
        //         ["SEP", 4], 
        //         ["OCT", 3],
        //         ["NOV", 2],     
        //         ["DEC", 1], 
        //         ["JAN", 4],
        //         ["FEV", 4],
        //         ["MAR", 4],
        //         ["AVR", 3],
        //         ["MAI", 3],
        //         [" ", 0], 
        //     ];


        //     $.plot("#placeholder", [ data ], {
        //         legend : {
        //             labelBoxBorderColor:"rgb(87, 153, 250)",
        //         },
        //         series: {
        //             bars: {
        //                 show: true,
        //                 fillColor: "white",
        //                 barWidth: 0.3,
        //                 align: "center",
        //                 borderWidth:0,
        //                 borderColor: "rgb(0, 0, 250)", 
        //             }
        //         },
        //         colors : ["rgb(87, 153, 250)"],
        //         xaxis: {
        //             mode: "categories",
        //             //tickColor "rgb(0, 0, 0)",
        //             tickColor: null,
        //             tickLength: 0                
        //         },
        //         grid: {
        //             //color: "rgb(87, 153, 250)",  
        //             //borderWidth:0, 
        //             borderWidth: {top: 0.5, right: 0, bottom: 0, left: 0},
        //             borderColor: {top: "rgba(120,120,120,.5)",left: "rgb(87, 153, 250)",bottom: "rgb(87, 153, 250)",right: "rgb(87, 153, 250)"} 
        //         }
        //     });

        //     var data2 = [
        //         {label: "FICHE(S)",   data: 6},
        //         {label: "EMPLOYE(S)",  data: 9},
        //     ];

        //     $.plot('#placeholder2', data2, {
        //         legend: {
        //             show: true,
        //             position: "ne",
        //             margin: 20,
        //         },
        //         series: {
        //             pie: {
        //                 innerRadius: 0.8,
        //                 show: true
        //             }
        //         },
        //         grid: {
        //             hoverable: true,
        //             clickable: true,
        //         },
        //         colors : ["rgb(255, 200, 200)","rgb(255, 80, 80)"],
        //     });

        //     // Add the Flot version string to the footer

        //     $("#footer").prepend("Flot " + $.plot.version + " &ndash; ");
        // });
    </script>
    <script type="text/javascript"> 

    //     $(function() {

    //         var data3 = [
    //             {label: "INTERIMAIRE(S)",   data: 14},
    //             {label: "NON INTERIM(S)",  data: 3},
    //         ];

    //         $.plot('#placeholder3', data3, {
    //             legend: {
    //                 show: true,
    //                 position: "ne",
    //                 margin: 20,
    //             },
    //             series: {
    //                 pie: {
    //                     innerRadius: 0.8,
    //                     show: true
    //                 }
    //             },
    //             grid: {
    //                 hoverable: true,
    //                 clickable: true,
    //             },
    //             colors : ["rgb(80, 80, 255)","rgb(200, 200, 255)"],
    //         });

    //         // Add the Flot version string to the footer

    //         $("#footer").prepend("Flot " + $.plot.version + " &ndash; ");
    //     });
    </script>

    <?php
    if (($curr_ctrl=='employes')&&(($curr_act=='add')||($curr_act=='edit'))){
    ?>
    <script type="text/javascript">
        $(function() {

            // Au chargement de la page
            if($('#EmployeInterim').prop('checked')){
                $("#EmployeAgenceId").prop('disabled', false);
            } else {
                $("#EmployeAgenceId").prop('disabled', true);
            }

            // A chaque changement de statut de la checkbox interim
            $( "#EmployeInterim" ).click(function() {
                if($('#EmployeInterim').prop('checked')){
                    $("#EmployeAgenceId").prop('disabled', false);
                } else {
                    $("#EmployeAgenceId").prop('disabled', true);
                    $("#EmployeAgenceId").prop('value', null);
                }
            });  

        });
    </script>
    <?php
    }
    ?>

    <?php
    if (($curr_ctrl=='fiches')&&(($curr_act=='add')||($curr_act=='edit'))){
    ?>
    <script type="text/javascript">
        $(function() {

            // Au chargement de la page
            if($('#FicheGranddeplacement').prop('checked')){
                $("#FicheRepas").prop('disabled', true);
                $("#FicheRepas").prop('checked', false);
            } else {
                $("#FicheRepas").prop('disabled', false);
            }

            // A chaque changement de statut de la checkbox interim
            $( "#FicheGranddeplacement" ).click(function() {
                if($('#FicheGranddeplacement').prop('checked')){
                    $("#FicheRepas").prop('disabled', true);
                    $("#FicheRepas").prop('checked', false);
                } else {
                    $("#FicheRepas").prop('disabled', false);
                }
            });  
            
        });
    </script>
    <?php
    }
    ?>

    </body>
</html>