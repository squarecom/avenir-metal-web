    <?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Avenir Metal : Entreprise de TPE');
?>

<!DOCTYPE html>
<html>
<!-- HEADER -->
    <head>
        <title><?php echo $cakeDescription ?> (<?php echo $title_for_layout; ?>)</title>
        <?php echo $this->Html->charset(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <?php
           
        /////////////////////////////////// ICON //////////////////////////////////////////////

        echo $this->Html->meta('icon');
        echo $this->Html->meta('description','Avenir Metal Interface Web - Private Service');
        echo $this->Html->meta(array('name' => 'robots', 'content' => 'noindex, nofollow'));
        echo $this->Html->meta(array('name' => 'author', 'content' => 'SquareCom'));
        echo $this->Html->meta(array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8'));
        echo $this->Html->meta(array('name' => 'Content-Language', 'content' => 'fr'));
        echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
        echo $this->fetch('meta');

        /////////////////////////////////// CSS //////////////////////////////////////////////

        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('bootstrap-theme.min');
        echo $this->Html->css('jquery-ui.css');
        echo $this->Html->css('dashboard.css');
        echo $this->Html->css('signin.css');
        echo $this->Html->css('jquery.datatables.css');
        echo $this->fetch('css');     
        ?>         

    </head>
    <body style="background:url(img/fondam.jpg) no-repeat center center fixed; background-size:cover;">
    <!-- background-color:rgb(50, 50, 64); -->
        <?php 
        echo $this->Session->flash();
        echo $this->fetch('content');  
        // echo '<h1>Session User</h1>';
        // debug($this->Session->read()); 
        // echo $this->Session->read('Auth.User.username');
        // echo $this->Session->read('Auth.User.role');

        //echo $this->Html->script('excanvas.min.js'); // Graphique, Canvas,..
        //echo $this->Html->script('jquery.min.js');
        //echo $this->Html->script('jquery-ui.custom');
        //echo $this->Html->script('bootstrap.min');
        //echo $this->Html->script('jquery.nicescroll.min'); // Scrollbar personnalisé
        echo $this->Html->script('jquery-1.11.0.min');
        echo $this->Html->script('jquery-ui-1.10.4.custom.min');
        echo $this->Html->script('jquery.dataTables.min.js');
        echo $this->Html->script('avenirmetal.js');

        ////////////////////////////////// JS ///////////////////////////////////////////////

        echo $this->fetch('script');
        ?>

    </body>
</html>