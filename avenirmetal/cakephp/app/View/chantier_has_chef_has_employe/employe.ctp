    <?php
    $this->Html->addCrumb('Fiches', '/fiches');
    $this->Html->addCrumb('Fiches par employé', '/fiches/employe');

    echo '<div class="row" style="margin-bottom:20px;"><div class="col-sm-3">';
    echo $this->Html->link('<i class="fa fa-b fa-calendar"></i><br/>Trier par jour', 
        array('action'=>'index'), array('class'=>'btn btn-primary btn-primary-my btn-block','escape'=>false)); 
    echo '</div><div class="col-sm-3">';
    echo $this->Html->link('<i class="fa fa-b fa-briefcase"></i><br/>  Trier par chantier', 
        array('action'=>'chantier'), array('class'=>'btn btn-primary btn-primary-my btn-block','escape'=>false)); 
    echo '</div><div class="col-sm-3">';
    echo $this->Html->link('<i class="fa fa-b fa-bullhorn"></i><br/>Trier par chef', 
        array('action'=>'chef'), array('class'=>'btn btn-primary btn-primary-my btn-block','escape'=>false)); 
    echo '</div><div class="col-sm-3">';
    echo $this->Html->link('<i class="fa fa-b fa-plus"></i><br/>Ajouter une fiche', 
        array('action'=>'add'), array('class'=>'btn btn-success btn-success-my btn-block','escape'=>false)); 
    echo '</div></div>';

    $jour = array(' ','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche');
    $mois = array(1=>'Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Décembre');
    echo '<div class="row bloc-am"><div class="col-sm-12 bloc-min">';

    /// Trie par date
    $d = date('Y');
    if(isset($_GET['jour1'])){
        $this->request->data['Fiche']['jour1'] = $_GET['jour1'];
    }
    if(isset($_GET['jour1'])){
        $this->request->data['Fiche']['jour2'] = $_GET['jour2'];
    }
    if(isset($_GET['id']))
        $this->request->data['Fiche']['id'] = $_GET['id'];

    echo $this->Form->create('Fiche', array('class'=>'form-horizontal','type'=>'get','action'=>'employe','controller'=>'Fiches')); 
    echo '<div class="form-group"><label for="FicheInterim" class="col-sm-3 col-md-3 col-lg-2 control-label">Période</label><div class=\'col-sm-4 col-md-4 col-lg-5 \'>';
    echo $this->Form->input('Fiche.jour1', 
        array(  'label'         => false,
                'value'         => '{$d}',
                'type'          => 'date',
                'dateFormat'    => 'D M Y',
                'monthNames'    => $mois,
                'before'        => false,
                'div'           => false,
                'after'         => false,
                'class'         => 'input-sm',
                'error'         => array('attributes' => array('wrap' => false, 'class' => 'alert alert-danger'))
        )
    );
    echo '</div><div class=\'col-sm-4 col-md-4 col-lg-5 \'>';
    echo $this->Form->input('Fiche.jour2', 
        array(  'label'         => false,
                'value'         => '{$d}',
                'type'          => 'date',
                'dateFormat'    => 'D M Y',
                'monthNames'    => $mois,
                'before'        => false,
                'div'           => false,
                'after'         => false,
                'class'         => 'input-sm',
                'error' => array('attributes' => array('wrap' => false, 'class' => 'alert alert-danger'))
        )
    );

    echo '</div></div><div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    if(isset($employes))
    echo $this->Form->input('Fiche.id', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Employé'),
                'empty'         => 'Choix Employé',
                'options'       => $employes,
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    $options = array(
        'label' => 'Rechercher',
        'div' => array(
            'class' => 'col-sm-9 col-md-9 col-lg-10',
        ),
        'before'        => '',
        'after'         => '</div>',
        'class'=>'btn btn-success btn-success-my btn-block'
    );
    echo '<div class="form-group"><label class="col-sm-3 col-md-3 col-lg-2 control-label"></label>';
    echo $this->Form->end($options);
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo '</div></div>';

    if(isset($_GET['id'])&&($_GET['id']!='')){ // Que si on a un employé renseigné.

        /*if($_GET['jour1']['month']<10)
                $_GET['jour1']['month'] = '0'.$_GET['jour1']['month'];*/
        $jmin = $_GET['jour1']['year'].'-'.$_GET['jour1']['month'].'-'.$_GET['jour1']['day'];
        /*if($_GET['jour2']['month']<10)
            $_GET['jour2']['month'] = '0'.$_GET['jour2']['month'];*/
        $jmax = $_GET['jour2']['year'].'-'.$_GET['jour2']['month'].'-'.$_GET['jour2']['day'];

        echo '<div class="row bloc-am">';
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            echo $this->Html->link('Exporter les résultats',  array('action'=>'exportemp', $jmin,$jmax,$id), array('class'=>'btn btn-primary btn-primary-my btn-block','escape'=>false)); 
        }
        else{
            echo $this->Html->link('Exporter les résultats',  array('action'=>'exportemp', $jmin,$jmax), array('class'=>'btn btn-primary btn-primary-my btn-block','escape'=>false)); 
        }
        echo '</div>';  
    }


    echo '<div class="row bloc-am"><div class="col-sm-12 bloc-min">';
    if(isset($fiches[0])){
    	$nom =$fiches[0]['Employe']['prenom'].$fiches[0]['Employe']['nom'];
        echo '<h3 class="text-center">'.$fiches[0]['Employe']['prenom'].' '.$fiches[0]['Employe']['nom'].'</h3>';
    }
    ?> 
        <div class="table-responsive">
          <table class="table table-bordered table-hover avenir-datatable ">
            <thead> 
              <tr>
                <th></th>
                <th>Informations</th>
                <th>Details</th>
                <th class="text-center"><i class="fa fa-plane"></i></th>
                <th class="text-center"><i class="fa fa-cutlery"></i></th>
                <th class="text-center"><i class="fa fa-car"></i></th>
                <th class="text-center">horaires</th>
                <th class="text-center">Total</th>
                <th class="text-center">options</th>
              </tr>
            </thead>
            <tbody> 
            <?php

            $told = 1;
            $totalh =0;
            $totalgd = 0;
            $totalr = 0;
            $totalt = 0;
            $totaln = 0;

            if(isset($fiches))
            foreach ($fiches as $employe) {


                if(!isset($nom)){
                    $nom = $employe['Employe']['prenom'].$employe['Employe']['nom'];
                }
                if(($employe['Employe']['prenom'].$employe['Employe']['nom'])!=$nom){
                    $nom =$employe['Employe']['prenom'].$employe['Employe']['nom'];
                    $told =0;
                }
                if($told==0){
                    echo '</tbody><tfoot><tr>';
                    echo '
                    <th class="text-center" colspan="3">'.$totaln.' Jour(s)</th>
                    <th class="text-center">'.$totalgd.'</th>
                    <th class="text-center">'.$totalr.'</th>
                    <th class="text-center">'.$totalt.'</th>
                    <th colspan="3" class="text-center">'.$totalh.
                    ' Heure(s)</th></tr></tfoot>';

                    echo '</tbody></table></div>
                    <div style="clear:both;"></div>';
                    //echo '<div><button class="btn btn-success btn-success-my "><i class="glyphicon glyphicon-save"></i> Exporter les fiches de ce jour</button></div>'
                    echo '
        </div></div>
                    <div style="clear:both;"></div>
                    <div class="row bloc-am">

                    <div class="col-sm-12 bloc-min"><h3 class="text-center">'.$employe['Employe']['prenom'].' '.$employe['Employe']['nom'].'</h3><div class="table-responsive"><table class="table table-bordered table-hover avenir-datatable ">';
                    echo '<thead><tr>                <th></th>
                <th>Informations</th>
                <th>Details</th>
                <th class="text-center"><i class="fa fa-plane"></i></th>
                <th class="text-center"><i class="fa fa-cutlery"></i></th>
                <th class="text-center"><i class="fa fa-car"></i></th>
                <th class="text-center">horaires</th>
                <th class="text-center">Total</th>
                <th class="text-center">options</th></tr></thead><tbody>';
                    $told=1;
                    $totalh =0;
                    $totalgd = 0;
                    $totalr = 0;
                    $totalt = 0;
                    $totaln = 0;
                }

                echo '<tr><td>';
                    $heur1 = intval(date("G", strtotime($employe['Fiche']['matin_debut'])));
                    $heur2 = intval(date("G", strtotime($employe['Fiche']['matin_fin']))); 
                    $heur3 = intval(date("G", strtotime($employe['Fiche']['aprem_debut'])));
                    $heur4 = intval(date("G", strtotime($employe['Fiche']['aprem_fin']))); 
                    $nuit=false;

                    if((22<$heur1)||($heur1<8)){$nuit=true;} else {
                        if((22<$heur2)||($heur2<8)){$nuit=true;} else {
                            if((22<$heur3)||($heur3<8)){$nuit=true;} else {
                                if((22<$heur4)||($heur4<8)){$nuit=true;}
                            }
                        }
                    }
                    if($nuit){echo '<i class="fa fa-moon-o"></i>'; } else { echo '<i class="fa fa-sun-o"></i>';}
                    
                echo '</td><td>';
                    echo '<span style="font-weight:800;">';
	                    $jdt = date("N", strtotime($employe['Fiche']['jour']));
	                    $ndt = date("d", strtotime($employe['Fiche']['jour']));
	                    $adt = date("n", strtotime($employe['Fiche']['jour']));
	                    echo $jour[$jdt].' '.$ndt.' '.$mois[$adt];
                    echo '</span></br>';
                    echo $employe['Fiche']['commentaire'];
                echo '</td><td>';  
                    echo 'Chef : <span style="font-weight:800;">';
                    echo $employe['Chef']['Employe']['prenom'];
                    echo ' ';
                    echo $employe['Chef']['Employe']['nom'];
                    echo '</span><br>Chantier : <span style="font-weight:800;">';
                    echo $employe['Chantier']['Chantier']['num'];
                    echo ' ';
                    echo $employe['Chantier']['Chantier']['nom'];
                echo '</span></td><td class="text-center">';                 
                    switch ($employe['Fiche']['granddeplacement']) {
                        case 1: echo '<i class="glyphicon glyphicon-ok"></i>'; $totalgd++;break;
                        default: echo '<i class="glyphicon glyphicon-remove"></i>'; break;
                    }
                echo '</td><td class="text-center">';
                    switch ($employe['Fiche']['repas']) {
                        case 1: echo '<i class="glyphicon glyphicon-ok"></i>'; $totalr++; break;
                        default: echo '<i class="glyphicon glyphicon-remove"></i>'; break;
                    }
                echo '</td><td class="text-center">';
                    switch ($employe['Fiche']['trajet']) {
                        case 1: echo '<i class="glyphicon glyphicon-ok"></i>'; $totalt++; break;
                        default: echo '<i class="glyphicon glyphicon-remove"></i>'; break;
                    }
                /* -------------------------------------------------------------------------------------------------------------- */
                echo '</td><td class="text-center">';
                    echo '<span style="font-weight:800;">';
                    echo date("H:i", strtotime($employe['Fiche']['matin_debut']));
                    echo '</span> a <span style="font-weight:800;">';
                    echo date("H:i", strtotime($employe['Fiche']['matin_fin']));
                    echo '</span><br><span style="font-weight:800;">';
                    echo date("H:i", strtotime($employe['Fiche']['aprem_debut']));
                    echo '</span> a <span style="font-weight:800;">';
                    echo date("H:i", strtotime($employe['Fiche']['aprem_fin']));
                    echo '</span>';
                    /* -------------------------------------------------------------------------------------------------------------- */
                echo '</td><td class="text-center">';
                    echo '<span style="font-weight:800;">';
                    
                        $min1 = intval(date("i", strtotime($employe['Fiche']['matin_debut']))); // 0
                        $min1 = round(($min1*100)/60);
                        $total1 = $heur1.'.'.$min1;
                        $min2 = intval(date("i", strtotime($employe['Fiche']['matin_fin']))); // 0
                        $min2 = round(($min2*100)/60);
                        $total2 = $heur2.'.'.$min2;
                        $bloc1 = $total2-$total1;
                    
                        $min3 = intval(date("i", strtotime($employe['Fiche']['aprem_debut']))); // 0
                        $min3 = round(($min3*100)/60);
                        $total3 = $heur3.'.'.$min3;
                        $min4 = intval(date("i", strtotime($employe['Fiche']['aprem_fin']))); // 0
                        $min4 = round(($min4*100)/60);
                        $total4 = $heur4.'.'.$min4;
                        $bloc2 = $total4-$total3;

                        $total = $bloc1+$bloc2;
                        $totalh = $totalh+$total;
                        $totaln++;
                        echo $total;
                    echo '</span>';
                    /* -------------------------------------------------------------------------------------------------------------- */
                echo '</td><td class="text-center">';
                    echo $this->Html->link('<i class="glyphicon glyphicon-pencil"></i>', array('action'=>'edit', $employe['Fiche']['id']), array('escape'=>false));
                    echo ' ';
                    echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>', array('action' => 'delete', $employe['Fiche']['id']), array('confirm' => 'Etes vous sure?','escape'=>false));
                echo '</td></tr>';
              }
              echo '</tbody><tfoot><tr>';
                    echo '
                    <th class="text-center" colspan="3">'.$totaln.' Fiche(s)</th>
                    <th class="text-center">'.$totalgd.'</th>
                    <th class="text-center">'.$totalr.'</th>
                    <th class="text-center">'.$totalt.'</th>
                    <th colspan="3" class="text-center">'.$totalh.
                    ' Heure(s)</th></tr></tfoot>';
              ?>
            </tbody>
          </table>
        </div>
        <div style="clear:both;"></div>
        <!-- <div><button class="btn btn-success btn-success-my "><i class="glyphicon glyphicon-save"></i> Exporter les fiches de l'employé en CSV</button></div> -->
        <div style="clear:both;"></div>

    </div>
</div>
<div style="clear:both;"></div>
<?php 
    echo '<div class="row" style="margin-bottom:20px;"><div class="col-sm-6">';
    echo $this->Paginator->prev(
        '<i class="fa fa-arrow-left"></i>',
        array('data-original-title'=>'Précédent','style'=>"border: none !important;", 'tag' => false, 'class'=>'btn btn-info btn-block', 'escape'=>false),
        null,
        array('data-original-title'=>'Précédent','disabled'=>'disabled','style'=>"border: none !important;", 'class'=>' btn btn-info btn-block', 'escape'=>false)
    );

    echo '</div><div class="col-sm-6">';

     echo $this->Paginator->next(
        '<i class="fa fa-arrow-right"></i>',
        array('data-original-title'=>'Suivant','style'=>"border: none !important;",  'tag' => false,'class'=>' btn btn-info btn-block', 'escape'=>false),
        null,
        array('data-original-title'=>'Suivant','disabled'=>'disabled','style'=>"border: none !important;", 'class'=>' btn btn-info btn-block', 'escape'=>false)
    );

    echo '</div></div>';
    ?>
<style type="text/css">
    h4.white {
        color:white;
    }
    hr.hrtr{
        border-top:rgb(87, 153, 250);
    }
    .tickLabel {
        color:white;
        font-size: 15px;
        font-weight: 100;
    }
    div#placeholder3 div.legend table {
        height: 150px;
    }
    div#placeholder3 div.legend table tbody tr td.legendLabel {
        font-size: 20px;
        width:250px;
        text-align: left;
        padding-left:20px;
    }
    div#placeholder3 div.legend table tbody tr td.legendColorBox div {
        border:none !important;
    }

    div#placeholder3 div.legend table tbody tr:first-child td.legendColorBox div:first-child div {
        background: rgb(200, 200, 255);
        border-radius:5px;
        height:35px !important;
        width: 35px !important;
    }
    div#placeholder3 div.legend table tbody tr:last-child td.legendColorBox div:first-child div {
        background: rgb(80, 80, 255);
        border-radius:5px;
        height:35px !important;
        width: 35px !important;
    }  
    .table-bordered>tbody>tr>td {
        vertical-align: middle;
    }
</style>