<div class="row bloc-am">
<div class="col-sm-12 bloc-min">
<h4>MODIFIER UNE FICHE</h4>
<?php 
    echo $this->Html->link('<i class="glyphicon glyphicon-arrow-left"></i> Retourner à l\'écran précédént',
     array('action'=>'index'), array('class'=>'btn btn-danger btn-blocked-dg', 'escape'=>false));
    $this->Html->addCrumb('Fiches', '/Fiches');
    $this->Html->addCrumb('Modifier', '/Fiches/edit');
?>
<hr class="hrw">

    <?php
    echo $this->Form->create('Fiche', array('action'=>'edit', 'class'=>'form-horizontal','type'=>'file')); 
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Fiche.chantiers_has_chefs_chantiers_id', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Chantier*'),
                'empty'   => 'Choix du Chantier',
                'before'  => '',
                'options' => $chantiers,
                'after'   => '</div>',
                'between' => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'     => array('class' => 'form-group'),
                'class'   => 'form-control input-sm',
                'error'   => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Fiche.chantiers_has_chefs_chefs_id', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Chef*'),
                'empty'   => 'Choix du Chef',
                'before'  => '',
                'options' => $chefs,
                'after'   => '</div>',
                'between' => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'     => array('class' => 'form-group'),
                'class'   => 'form-control input-sm',
                'error'   => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Fiche.employe_id', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Employé*'),
                'empty'   => 'Choix de l\'employé',
                'before'  => '',
                'options' => $employes,
                'after'   => '</div>',
                'between' => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'     => array('class' => 'form-group'),
                'class'   => 'form-control input-sm',
                'error'   => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Fiche.granddeplacement', 
        array(  'label'         => false,
                'type'          => 'checkbox',
                'before'        => '<label for="FicheInterim" class="col-sm-3 col-md-3 col-lg-2 control-label">GD</label><div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'between'       => '</div>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Fiche.repas', 
        array(  'label'         => false,
                'type'          => 'checkbox',
                'before'        => '<label for="FicheInterim" class="col-sm-3 col-md-3 col-lg-2 control-label">Panier</label><div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'between'       => '</div>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Fiche.trajet', 
        array(  'label'         => false,
                'type'          => 'checkbox',
                'before'        => '<label for="FicheInterim" class="col-sm-3 col-md-3 col-lg-2 control-label">Trajet</label><div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'between'       => '</div>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';

    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    $d = date('Y');
    echo $this->Form->input('Fiche.jour', 
        array(  'label'         => false,
                'value'         => '{$d}',
                'type'          => 'date',
                'monthNames'    => $cmois,
                'dateFormat'    => 'D M Y',
                'before'        => '<label for="FicheInterim" class="col-sm-3 col-md-3 col-lg-2 control-label">Jour</label><div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'after'         => '</div>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'input-sm',
                //'class'=> 'input-sm col-xs-3 col-md-3 col-lg-3 ',

                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Fiche.matin_debut', 
        array(  'label'    => false,
                'type'     => 'time',
                'interval' => 15,
                'selected'  => array('meridian'=> $this->request->data['Fiche']['matin_debut']['meridian']),
                'before'   => '<label for="FicheInterim" class="col-sm-3 col-md-3 col-lg-2 control-label">Debut matinée</label><div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'after'    => '</div>',
                'div'      => array('class' => 'form-group'),
                'class'    => 'input-sm',
                'error'    => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Fiche.matin_fin', 
        array(  'label'         => false,
                'type'     => 'time',
                'interval' => 15,
                'selected'  => array('meridian'=> $this->request->data['Fiche']['matin_fin']['meridian']),
                'before'        => '<label for="FicheInterim" class="col-sm-3 col-md-3 col-lg-2 control-label">Fin matinée</label><div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'after'       => '</div>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Fiche.aprem_debut', 
        array(  'label'         => false,
                'type'     => 'time',
                'interval' => 15,
                'selected'  => array('meridian'=> $this->request->data['Fiche']['aprem_debut']['meridian']),
                'before'        => '<label for="FicheInterim" class="col-sm-3 col-md-3 col-lg-2 control-label">Debut Aprem</label><div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'after'       => '</div>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Fiche.aprem_fin', 
        array(  'label'         => false,
                'type'     => 'time',
                'interval' => 15,
                'selected'  => array('meridian'=> $this->request->data['Fiche']['aprem_fin']['meridian']),
                'before'        => '<label for="FicheInterim" class="col-sm-3 col-md-3 col-lg-2 control-label">Fin Aprem</label><div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'after'       => '</div>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Fiche.commentaire', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Commentaire*'),
                'placeholder'   => 'Quelques mots...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    $options = array(
        'label' => 'Enregister',
        'div' => array(
            'class' => 'col-sm-9 col-md-9 col-lg-10',
        ),
        'before'        => '',
        'after'         => '</div>',
        'class'         =>'btn btn-success btn-success-my btn-block',
    );
    echo '<div class="form-group"><label class="col-sm-3 col-md-3 col-lg-2 control-label"></label>';
    echo $this->Form->end($options);
    echo '</div></div>';
    ?>
