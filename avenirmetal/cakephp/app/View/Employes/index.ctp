<div class="row bloc-am">
    <div class="col-sm-12 bloc-min">
    <h4>EMPLOYES</h4>
    <?php 
        echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> Ajouter un Employé', array('action'=>'add'), array('class'=>'btn btn-success btn-blocked-su','escape'=>false)); 
        $this->Html->addCrumb('employes', '/employes');
        //$this->Html->addCrumb('Connexion', '/users/login');
    ?>
    <hr class="hrw">
        <div class="table-responsive">
          <table class="table table-bordered table-hover avenir-datatable ">
            <thead> 
              <tr>
                <th>Informations</th>
                <th>A propos</th>
                <th class="text-center">Fiche</th>
                <th class="text-center">EDITER</th>
                <th class="text-center">Effacer</th>
              </tr>
            </thead>
            <tbody>
            <?php
            // echo $employe['Employe']['id'];
            // echo '</td><td>';
            // echo '<br/>';

            foreach ($employes as $employe) { 
                echo '<tr><td>';
                    echo '<span style="font-weight:800;">';
                    echo $employe['Employe']['nom'];
                    echo ' ';
                    echo $employe['Employe']['prenom'];
                    echo '</span> (';
                    $tel = $employe['Employe']['tel'];
                    echo substr($tel, 0,2).'.'.substr($tel, 2,2).'.'.substr($tel, 4, 2).'.'.substr($tel, 6, 2).'.'.substr($tel, 8, 2);
                    echo ')';
                    echo '<br/>';
                    echo $employe['Employe']['adresse'];
                    if(isset($employe['Employe']['cp'])){
                        echo ' - ';
                        echo $employe['Employe']['cp'];
                    }
                    if(isset($employe['Employe']['ville'])){
                        echo ' - ';
                        echo $employe['Employe']['ville'];
                    }
                echo '</td><td>';
                if(isset($employe['Employe']['statut']))
                    echo 'Catégorie : <span style="font-weight:800;">'.$employe['Employe']['statut'].'</span><br/>';
                $interim = $employe['Employe']['interim'];
                switch ($interim) {
                    case 1:
                        echo '<span style="font-weight:800;">Interimaire</span>';
                        break;
                }
                if(isset($employe['Agence']['nom'])){
                    echo ' de '.$employe['Agence']['nom'].'.';
                } 
                if((isset($employe['Agence']['nom']))||($employe['Employe']['interim']==1)){
                    echo '<br/>';
                }                  
                echo $employe['Employe']['commentaire'];
                echo '</td><td class="text-center">';
                echo $this->Html->link('<i class="glyphicon glyphicon-eye-open"></i>', array('controller'=>'fiches', 'action'=>'employe', $employe['Employe']['id']), array('escape'=>false));
                echo '</td><td class="text-center">';
                echo $this->Html->link('<i class="glyphicon glyphicon-pencil"></i>', array('action'=>'edit', $employe['Employe']['id']), array('escape'=>false));
                echo '</td><td class="text-center">';
                echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>', array('action' => 'delete', $employe['Employe']['id']), array('confirm' => 'Etes vous sure?','escape'=>false));
                echo '</td></tr>';
              }
              ?>
            </tbody>
          </table>
        </div>

    </div>
</div>

<style type="text/css">
    h4.white {
        color:white;
    }
    hr.hrtr{
        border-top:rgb(87, 153, 250);
    }
    .tickLabel {
        color:white;
        font-size: 15px;
        font-weight: 100;
    } 
    .table-bordered>tbody>tr>td {
        vertical-align: middle;
    }
</style>