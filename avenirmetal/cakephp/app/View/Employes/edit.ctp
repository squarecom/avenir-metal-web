<div class="row bloc-am">
<div class="col-sm-12 bloc-min">
<h4>MODIFIER UN EMPLOYE</h4>
<?php 
    echo $this->Html->link('<i class="glyphicon glyphicon-arrow-left"></i> Retourner à l\'écran précédént',
     array('action'=>'index'), array('class'=>'btn btn-danger btn-blocked-dg', 'escape'=>false));
    $this->Html->addCrumb('Employes', '/Employes');
    $this->Html->addCrumb('Ajouter', '/Employes/add');
?>
<hr class="hrw">

    <?php
    echo $this->Form->create('Employe', array('class'=>'form-horizontal','type'=>'file')); 
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Employe.nom', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Nom*'),
                'placeholder'   => 'Le nom de l\'employe...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Employe.prenom', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Prenom*'),
                'placeholder'   => 'Le prenom de l\'employe...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Employe.tel', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Téléphone'),
                'placeholder'   => 'Le numéro de téléphone...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Employe.interim', 
        array(  'label'         => false,
                'type'          => 'checkbox',
                'before'        => '<label for="EmployeInterim" class="col-sm-3 col-md-3 col-lg-2 control-label">Interimaire</label><div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                //'after'         => 'a',
                'between'       => '</div>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    //echo '<span class="help-block">(A cocher si l\'employé est un intérimaire, vous pouvez également renseignez l\'agence ci-dessous).</span>';
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Employe.agence_id', 
        array(  'label'   => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Agence'),
                'empty'   => 'Non Obligatoire',
                'before'  => '',
                //'options' => array(1,2,3,4,5),
                'options' => $agences,
                'after'   => '</div>',
                'between' => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'     => array('class' => 'form-group'),
                'class'   => 'form-control input-sm',
                'error'   => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Employe.adresse', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Adresse*'),
                'placeholder'   => 'L\'adresse de l\'employé...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Employe.ville', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Ville*'),
                'placeholder'   => 'Et la ville...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
        echo $this->Form->input('Employe.cp', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Code Postal*'),
                'placeholder'   => 'Le Code Postal...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Employe.statut', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Statut*'),
                'placeholder'   => 'Secrétaire, Commercial, ...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Employe.commentaire', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Commentaire*'),
                'placeholder'   => 'Quelques mots...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    $options = array(
        'label' => 'Enregister',
        'div' => array(
            'class' => 'col-sm-9 col-md-9 col-lg-10',
        ),
        'before'        => '',
        'after'         => '</div>',
        'class'         =>'btn btn-success btn-success-my btn-block',
    );
    echo '<div class="form-group"><label class="col-sm-3 col-md-3 col-lg-2 control-label"></label>';
    echo $this->Form->end($options);
    echo '</div></div>';
    ?>
