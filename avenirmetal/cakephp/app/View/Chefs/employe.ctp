<div class="row bloc-am">
    <div class="col-sm-12 bloc-min">
    <h3>FICHES PAR EMPLOYE</h3>
    <?php
    echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> Ajouter une Fiche', array('action'=>'add'), array('class'=>'btn btn-success btn-blocked-su','escape'=>false)); 
    $this->Html->addCrumb('Fiches', '/Fiches');

    echo '</div></div>'; 
    echo '<div class="row" style="margin-bottom:20px;"><div class="col-sm-4">';
    echo $this->Html->link('<i class="glyphicon glyphicon-calendar"></i><br/>Trier par jour', 
        array('action'=>'index'), array('class'=>'btn btn-primary btn-primary-my btn-block','escape'=>false)); 
    echo '</div><div class="col-sm-4">';
    echo $this->Html->link('<i class="glyphicon glyphicon-briefcase"></i><br/>  Trier par chantier', 
        array('action'=>'chantier'), array('class'=>'btn btn-primary btn-primary-my btn-block','escape'=>false)); 
    echo '</div><div class="col-sm-4">';
    echo $this->Html->link('<i class="glyphicon glyphicon-bullhorn"></i><br/>Trier par chef', 
        array('action'=>'chef'), array('class'=>'btn btn-primary btn-primary-my btn-block','escape'=>false)); 
    echo '</div></div>';

    $jour = array(' ','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche');
    $mois = array(' ','Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Décembre');
    echo '<div class="row bloc-am"><div class="col-sm-12 bloc-min">';

    if(isset($fiches[0])){
    	$nom =$fiches[0]['Employe']['prenom'].$fiches[0]['Employe']['nom'];
        echo '<h4 class="text-center">'.$fiches[0]['Employe']['prenom'].' '.$fiches[0]['Employe']['nom'].'</h4>';
    }
    ?>
        <hr class="hrw">

        
        <div class="table-responsive">
          <table class="table table-bordered table-hover avenir-datatable ">
            <thead> 
              <tr>
                <th>Informations</th>
                <th>Details</th>
                <th class="text-center">distance</th>
                <th class="text-center">Repas</th>
                <th class="text-center">Trajet</th>
                <th class="text-center">horaires</th>
                <th class="text-center">options</th>
              </tr>
            </thead>
            <tbody> 
            <?php
            // echo $employe['Employe']['id'];
            // echo '</td><td>';
            // echo '<br/>';
            $told = 1;
            // $date = date('Y-m-j');
            // debug($date);

            

            foreach ($fiches as $employe) {
                //debug($nom); 
                //debug($employe);

                if(!isset($nom)){
                    $nom = $employe['Employe']['prenom'].$employe['Employe']['nom'];
                }
                if(($employe['Employe']['prenom'].$employe['Employe']['nom'])!=$nom){
                    $nom =$employe['Employe']['prenom'].$employe['Employe']['nom'];
                    $told =0;
                }
                if($told==0){
                    echo '</tbody></table></div><div style="clear:both;"></div>
                    <div class="text-center"><button class="btn btn-success btn-success-my "><i class="glyphicon glyphicon-save"></i> Exporter les fiches de l\'employé en CSV</button></div>
                    </div></div>
                    <div style="clear:both;"></div>
                    <div class="row bloc-am">

                    <div class="col-sm-12 bloc-min"><h4 class="text-center">'.$employe['Employe']['prenom'].' '.$employe['Employe']['nom'].'</h4><hr class="hrw"><div class="table-responsive"><table class="table table-bordered table-hover avenir-datatable ">';
                    echo '<thead><tr><th>Informations</th><th>Details</th><th class="text-center">distance</th><th class="text-center">Repas</th><th class="text-center">Trajet</th><th class="text-center">horaires</th><th class="text-center">options</th></tr></thead><tbody>';
                    $told=1;
                }

                echo '<tr><td>';
                    echo '<span style="font-weight:800;">';
	                    $jdt = date("N", strtotime($employe['Fiche']['jour']));
	                    $ndt = date("d", strtotime($employe['Fiche']['jour']));
	                    $adt = date("n", strtotime($employe['Fiche']['jour']));
	                    echo $jour[$jdt].' '.$ndt.' '.$mois[$adt];
                    echo '</span></br>';
                    echo $employe['Fiche']['commentaire'];
                echo '</td><td>';  
                    echo 'Chef : <span style="font-weight:800;">';
                    echo $employe['Chef']['Employe']['prenom'];
                    echo ' ';
                    echo $employe['Chef']['Employe']['nom'];
                    echo '</span><br>Chantier : <span style="font-weight:800;">';
                    echo $employe['Chantier']['Chantier']['num'];
                    echo ' ';
                    echo $employe['Chantier']['Chantier']['nom'];
                echo '</span></td><td class="text-center">';                 
                    switch ($employe['Fiche']['granddeplacement']) {
                        case 1: echo '<i class="glyphicon glyphicon-ok"></i>'; break;
                        default: echo '<i class="glyphicon glyphicon-remove"></i>'; break;
                    }
                echo '</td><td class="text-center">';
                    switch ($employe['Fiche']['repas']) {
                        case 1: echo '<i class="glyphicon glyphicon-ok"></i>'; break;
                        default: echo '<i class="glyphicon glyphicon-remove"></i>'; break;
                    }
                echo '</td><td class="text-center">';
                switch ($employe['Fiche']['trajet']) {
                        case 1: echo '<i class="glyphicon glyphicon-ok"></i>'; break;
                        default: echo '<i class="glyphicon glyphicon-remove"></i>'; break;
                    }
                echo '</td><td class="text-center">';
                    echo '<span style="font-weight:800;">';
                    echo date("H:i", strtotime($employe['Fiche']['matin_debut']));
                    echo '</span> a <span style="font-weight:800;">';
                    echo date("H:i", strtotime($employe['Fiche']['matin_fin']));
                    echo '</span><br><span style="font-weight:800;">';
                    echo date("H:i", strtotime($employe['Fiche']['aprem_debut']));
                    echo '</span> a <span style="font-weight:800;">';
                    echo date("H:i", strtotime($employe['Fiche']['aprem_fin']));
                    echo '</span>';
                echo '</td><td class="text-center">';
                    echo $this->Html->link('<i class="glyphicon glyphicon-pencil"></i>', array('action'=>'edit', $employe['Employe']['id']), array('escape'=>false));
                    echo ' ';
                    echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>', array('action' => 'delete', $employe['Employe']['id']), array('confirm' => 'Etes vous sure?','escape'=>false));
                echo '</td></tr>';
              }
              //die();
              ?>
            </tbody>
          </table>
        </div>
        <div style="clear:both;"></div>
                    <div class="text-center"><button class="btn btn-success btn-success-my "><i class="glyphicon glyphicon-save"></i> Exporter les fiches de l'employé en CSV</button></div>
        <div style="clear:both;"></div>

    </div>
</div>
<div style="clear:both;"></div>
<style type="text/css">
    h4.white {
        color:white;
    }
    hr.hrtr{
        border-top:rgb(87, 153, 250);
    }
    .tickLabel {
        color:white;
        font-size: 15px;
        font-weight: 100;
    }
    div#placeholder3 div.legend table {
        height: 150px;
    }
    div#placeholder3 div.legend table tbody tr td.legendLabel {
        font-size: 20px;
        width:250px;
        text-align: left;
        padding-left:20px;
    }
    div#placeholder3 div.legend table tbody tr td.legendColorBox div {
        border:none !important;
    }

    div#placeholder3 div.legend table tbody tr:first-child td.legendColorBox div:first-child div {
        background: rgb(200, 200, 255);
        border-radius:5px;
        height:35px !important;
        width: 35px !important;
    }
    div#placeholder3 div.legend table tbody tr:last-child td.legendColorBox div:first-child div {
        background: rgb(80, 80, 255);
        border-radius:5px;
        height:35px !important;
        width: 35px !important;
    }  
    .table-bordered>tbody>tr>td {
        vertical-align: middle;
    }
</style>