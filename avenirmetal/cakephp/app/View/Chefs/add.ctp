<div class="row bloc-am">
<div class="col-sm-12 bloc-min">
<h4>AJOUTER UN CHEF</h4>
<?php 
    echo $this->Html->link('<i class="glyphicon glyphicon-arrow-left"></i> Retourner à l\'écran précédént',
     array('action'=>'index'), array('class'=>'btn btn-danger btn-blocked-dg', 'escape'=>false));
    $this->Html->addCrumb('Chefs', '/Chefs');
    $this->Html->addCrumb('Ajouter', '/Chefs/add');
?>
<hr class="hrw">

    <?php
    echo $this->Form->create('Chef', array('class'=>'form-horizontal','type'=>'file')); 
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Chef.employe_id', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Employé*'),
                'empty'   => 'Choix de l\'employé',
                'before'  => '',
                'options' => $employes,
                'after'   => '</div>',
                'between' => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'     => array('class' => 'form-group'),
                'class'   => 'form-control input-sm',
                'error'   => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Chef.username', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Username*'),
                'placeholder'   => 'Identifiant pour se connecter',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
        /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Chef.password', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Mot de passe*'),
                'placeholder'   => 'Mot de passe...',
                'type'          => 'password',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    $options = array(
        'label' => 'Promouvoir l\'employé en chef de chantier',
        'div' => array(
            'class' => 'col-sm-9 col-md-9 col-lg-10',
        ),
        'before'        => '',
        'after'         => '</div>',
        'class'=>'btn btn-success btn-success-my btn-block'
    );
    echo '<div class="form-group"><label class="col-sm-3 col-md-3 col-lg-2 control-label"></label>';
    echo $this->Form->end($options);
    echo '</div></div>';
    ?>
