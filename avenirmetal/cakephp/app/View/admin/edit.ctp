<!-- app/View/Users/edit.ctp -->
<div class="col-xs-12 col-sm-12">
    <div class="widget-box">
        <div class="widget-title"><span class="icon"><i class="icon-cog"></i></span><h5>Editer son profil</h5></div>
            <div class="widget-content nopadding">
<?php
$this->Html->addCrumb('Utilisateur', '/users');
$this->Html->addCrumb('Editer son profil', '/users/edit', 'class=\'current\'');
?>

<?php echo $this->Form->create('User', array('class'=>'form-horizontal','type'=>'file', 'action' => 'edit')); ?>

        <?php 

         echo $this->Form->input('User.username', 
            array(  'label'         => array('class'=>'control-label', 'text'=>'Username de co*'),
                    'placeholder'   => 'Votre Pseudo de connection...',
                    'before'        => '',
                    'after'         => '</div>',
                    'between'       => '<div class=\'controls\'>',
                    'div'           => array('class' => 'form-group'),
                    'class'         => 'form-control input-sm',
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
            )
        );

        echo $this->Form->input('User.show_username', 
            array(  'label'         => array('class'=>'control-label', 'text'=>'Username visible*'),
                    'placeholder'   => 'Votre Pseudo visible...',
                    'before'        => '',
                    'after'         => '</div>',
                    'between'       => '<div class=\'controls\'>',
                    'div'           => array('class' => 'form-group'),
                    'class'         => 'form-control input-sm',
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
            )
        );

        echo $this->Form->input('User.email', 
            array(  'label'         => array('class'=>'control-label', 'text'=>'Adresse e-mail*'),
                    'placeholder'   => 'Votre Adresse e-mail...',
                    'before'        => '',
                    'after'         => '</div>',
                    'between'       => '<div class=\'controls\'>',
                    'div'           => array('class' => 'form-group'),
                    'class'         => 'form-control input-sm',
                    'type'          => 'email',
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
            )
        );

        echo '<h3>Le reste est pas obligatoire</h3>';

        echo $this->Form->input('Profile.name', 
            array(  'label'         => array('class'=>'control-label', 'text'=>'Nom'),
                    'placeholder'   => 'Votre nom...',
                    'required'      => false,
                    'before'        => '',
                    'after'         => '</div>',
                    'between'       => '<div class=\'controls\'>',
                    'div'           => array('class' => 'form-group'),
                    'class'         => 'form-control input-sm',
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
            )
        );

        echo $this->Form->input('Profile.fname', 
            array(  'label'         => array('class'=>'control-label', 'text'=>'Pr&eacutenom'),
                    'placeholder'   => 'Votre Prenom...',
                    'required'      => false,
                    'before'        => '',
                    'after'         => '</div>',
                    'between'       => '<div class=\'controls\'>',
                    'div'           => array('class' => 'form-group'),
                    'class'         => 'form-control input-sm',
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
            )
        );
        
        echo $this->Form->input('Profile.birthdate', 
            array(
                    'label'         => array('class'=>'control-label', 'text'=>'Date de naissance'),
                    'required'      => false,
                    'type'          =>'date',
                    'dateFormat'   =>'DMY',
                    'minYear'      =>date('Y')-100,
                    'maxYear'      =>date('Y'),
                    'allowEmpty'    => true,
                    'placeholder'   => 'Adresse e-mail...',
                    'before'        => '',
                    'after'         => '</div>',
                    'between'       => '<div class=\'controls\'>',
                    'div'           => array('class' => 'form-group'),
                    'class'         => 'input-sm',
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        ));

         echo $this->Form->input('Profile.city', 
            array(  'label'         => array('class'=>'control-label', 'text'=>'Ville*'),
                    'required'      => false,
                    'placeholder'   => 'Votre Ville...',
                    'before'        => '',
                    'after'         => '</div>',
                    'between'       => '<div class=\'controls\'>',
                    'div'           => array('class' => 'form-group'),
                    'class'         => 'form-control input-sm',
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
            )
        );

         echo $this->Form->input('Profile.skills', 
            array(  'label'         => array('class'=>'control-label', 'text'=>'Description*'),
                    'placeholder'   => 'Votre description...',
                    'required'      => false,
                    'before'        => '',
                    'after'         => '</div>',
                    'between'       => '<div class=\'controls\'>',
                    'div'           => array('class' => 'form-group'),
                    'class'         => 'form-control input-sm',
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
            )
        );

        echo $this->Form->input('Profile.context', 
            array(  'label'         => array('class'=>'control-label', 'text'=>'Poste favori*'),
                    'placeholder'   => 'Adresse e-mail...',
                    'required'      => false,
                    'before'        => '',
                    'after'         => '</div>',
                    'between'       => '<div class=\'controls\'>',
                    'div'           => array('class' => 'form-group'),
                    'class'         => 'form-control input-sm',
                    'options'       => array(
                                        'En recherche de CDI' => 'En recherche de CDI',
                                        'En recherche de Stage' => 'En recherche de Stage',
                                        'En recherche d alternance' => 'En recherche d alternance',
                                        'Auto-Entrepreneur' => 'Auto-Entrepreneur',
                                        'Eudiant' => 'Eudiant',
                                        'Employe' => 'Employe',
                                        'Autre' => 'Autre'
                                        ),
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
            )
        );

         echo $this->Form->input('Profile.hobbies', 
            array(  'label'         => array('class'=>'control-label', 'text'=>'Hobbies*'),
                    'placeholder'   => 'Adresse e-mail...',
                    'required'      => false,
                    'before'        => '',
                    'after'         => '</div>',
                    'between'       => '<div class=\'controls\'>',
                    'div'           => array('class' => 'form-group'),
                    'class'         => 'form-control input-sm',
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
            )
        );


        echo $this->Form->input('Profile.avatar_file', 
            array(  'label'         => array('class'=>'control-label', 'text'=>'Avatar'),
                    'type'          => 'file',
                    'required'      => false,
                    'before'        => '',
                    'after'         => '</div>',
                    'between'       => '<div class=\'controls\'>',
                    'div'           => array('class' => 'form-group'),
                    'class'         => 'form-control input-sm',
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
            )
        );
        echo $this->Form->hidden('ActivityUser.content', array('value'=>'hello'));

        $options = array(
            'label' => 'Enregistrer les changements',
            'div' => array(
                'class' => 'controls',
            ),
            'before'        => '',
            'after'         => '</div>',
            'between'       => '<div class=\'controls\'>',
            'class'=>'btn btn-primary'
        );
        echo '<div class="form-group"><label class="control-label"></label>';
        echo $this->Form->end($options);
        echo '</div></div></div></div>';
        ?>
                             </div>
            </div>
          </div>