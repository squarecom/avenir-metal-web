<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Avenir Metal</a>
        </div>
        <div class="navbar-collapse collapse">
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Rechercher...">
            <button class="btn btn-primary"><i class="glyphicon-search glyphicon"></i></button> 
            <a href="./logout" class="btn btn-danger">Déconnexion</a>
          </form>

        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li ><a href="./index">Accueil</a></li>
            <li><a href="./chantier">Chantiers</a></li>
            <li class="active"><a href="./all">Administrateurs</a></li>
            <li><a href="./employees">Employés</a></li>
            <li><a href="./fiches">Fiches</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="background-color:white;">
          <h1 class="page-header">Tableau de bord</h1>
          <h2 class="sub-header">Admins</h2>
          <button class="btn btn-success">Ajouter un Administrateur</button>
          <br><br>
          <p>Est-ce qu'on lie l'administrateur a un employe ? ( Comme ça l'admin, qui est un employé de AM, a un numéro de téléphone, nom et prenom associé 
            (On peut mettre une variable pour pas qu'il soit dans les fiches : Exemple : onFiche = false )</p>
          <hr>
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover avenir-datatable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Header</th>
                  <th>Editer</th>
                  <th>Supprimer</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Pierre</td>
                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Paul</td>
                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Jean</td>
                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
