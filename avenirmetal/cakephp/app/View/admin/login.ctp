<div class="container" style="max-width:330px; padding:15px; margin:0 auto;">

<?php
    echo $this->Form->create('User', array('class'=>'form-signin')); 

    echo '<h2 class="form-signin-heading">Avenir Metal</h2>';

    echo $this->Form->input('login', 
        array(  'label'         => false,
                'placeholder'   => 'Login...',
                'div'           => false,
                'required'      => true,
                'autofocus'     => true,
                'class'         => 'form-control',
                'style'         => 'border-bottom-right-radius:0; border-bottom-left-radius:0; margin-bottom:-1px;'
        )
    );
    echo $this->Form->input('pass', 
        array(  'label'         => false,
                'placeholder'   => 'Mot de passe..',
                'div'           => false,
                'type'          => 'password',
                'required'      => true,
                'class'         => 'form-control'
        )
    );

    echo '<label class="checkbox"><input type="checkbox" value="remember-me"> Se rappeler de moi</label>';

    $options = array(
        'label' => false,
        'div'   => false,
        'type'  => 'submit',
        'value' => 'Se connecter',
        'class' => 'btn btn-lg btn-primary btn-block'
    );
    echo $this->Form->end($options);
?>
</div>




