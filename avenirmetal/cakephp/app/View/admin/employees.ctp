 <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Avenir Metal</a>
        </div>
        <div class="navbar-collapse collapse">
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Rechercher...">
            <button class="btn btn-primary"><i class="glyphicon-search glyphicon"></i></button> 
            <a href="./logout" class="btn btn-danger">Déconnexion</a>
          </form>

        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li ><a href="./index">Accueil</a></li>
            <li><a href="./chantier">Chantiers</a></li>
            <li><a href="./all">Administrateurs</a></li>
            <li class="active"><a href="./employees">Employés</a></li>
            <li><a href="./fiches">Fiches</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="background-color:white;">
          <h1 class="page-header">Tableau de bord</h1>
          <h2 class="sub-header">Employees</h2>
          <button class="btn btn-primary">Ajouter un Employés</button>
          <hr>
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover avenir-datatable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nom</th>
                  <th>Prenom</th>
                  <th>Telephone</th>
                  <th>Interim</th>
                  <th>Adresse</th>
                  <th>Commentaire</th>
                  <th>Editer</th>
                  <th>Supprimer</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Dupont</td>
                  <td>Jean</td>
                  <td>06-99-99-99-99</td>
                  <td>Non</td>
                  <td>64 Rue de la raie, Grenoble 38000</td>
                  <td></td>
                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Manique</td>
                  <td>Paul</td>
                  <td>06-44-44-44-44</td>
                  <td>Oui</td>
                  <td>75 Allée du manoire, Voiron 38000</td>
                  <td>stagiaire quelques mois</td>
                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
