 <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Avenir Metal</a>
        </div>
        <div class="navbar-collapse collapse">
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Rechercher...">
            <button class="btn btn-primary"><i class="glyphicon-search glyphicon"></i></button> 
            <a href="./logout" class="btn btn-danger">Déconnexion</a>
          </form>

        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li ><a href="./index">Accueil</a></li>
            <li><a href="./chantier">Chantiers</a></li>
            <li><a href="./all">Administrateurs</a></li>
            <li><a href="./employees">Employés</a></li>
            <li class="active"><a href="./fiches">Fiches</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="background-color:white;">
          <h1 class="page-header">Tableau de bord</h1>
          <h2 class="sub-header">Fiches</h2>
          <a href="./fichesemp" class="btn btn-info">Trier par employé</a>
          <a href="./fichescha" class="btn btn-info">Trier par chantier</a>
          <a href="./fichesche" class="btn btn-info">Trier par chef</a>
          <a href="./fichesadd" class="btn btn-success">Ajouter une Fiche</a>
          <h3 class="sub-header">Mercredi 2 Avril</h3>
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover avenir-datatable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Chantier</th>
                  <th>Chef</th>

                  <th>Employé</th>
                  <th>Début</th>
                  <th>Fin</th>
                  <th>Grand Deplacement</th>
                  <th>Repas</th>
                  <th>Commentaire</th>

                  <th>Editer</th>
                  <th>Supprimer</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Paris 01</td>
                  <td>Paul Dumont</td>

                  <td>Paul Dumont</td>
                  <td>8h00</td>
                  <td>17h00</td>
                  <td>Oui</td>
                  <td>Oui</td>
                  <td>Le chef se met dans les fiches ?</td>

                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Paris 01</td>
                  <td>Paul Dumont</td>

                  <td>Marc Labrique</td>
                  <td>8h00</td>
                  <td>17h00</td>
                  <td>Oui</td>
                  <td>Oui</td>
                  <td></td>

                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Paris 01</td>
                  <td>Paul Dumont</td>

                  <td>Francois Montin</td>
                  <td>8h00</td>
                  <td>17h00</td>
                  <td>Oui</td>
                  <td>Oui</td>
                  <td></td>

                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Lyon 01</td>
                  <td>Gérard Dumortier</td>

                  <td>Arnolde Beaufils</td>
                  <td>8h00</td>
                  <td>17h00</td>
                  <td>Oui</td>
                  <td>Oui</td>
                  <td></td>

                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Lyon 01</td>
                  <td>Gérard Dumortier</td>

                  <td>Thierry Lebeton</td>
                  <td>8h00</td>
                  <td>17h00</td>
                  <td>Oui</td>
                  <td>Oui</td>
                  <td></td>

                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
              </tbody>
            </table>
          </div>
          <h3 class="sub-header">Mardi 1 Avril</h3>
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover avenir-datatable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Chantier</th>
                  <th>Chef</th>

                  <th>Employé</th>
                  <th>Début</th>
                  <th>Fin</th>
                  <th>Grand Deplacement</th>
                  <th>Repas</th>
                  <th>Commentaire</th>

                  <th>Editer</th>
                  <th>Supprimer</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Paris 01</td>
                  <td>Paul Dumont</td>

                  <td>Paul Dumont</td>
                  <td>9h00</td>
                  <td>18h00</td>
                  <td>Oui</td>
                  <td>Oui</td>
                  <td>Le chef se met dans les fiches ?</td>

                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Paris 01</td>
                  <td>Paul Dumont</td>

                  <td>Marc Labrique</td>
                  <td>9h00</td>
                  <td>18h00</td>
                  <td>Oui</td>
                  <td>Oui</td>
                  <td></td>

                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Paris 01</td>
                  <td>Paul Dumont</td>

                  <td>Francois Montin</td>
                  <td>9h00</td>
                  <td>18h00</td>
                  <td>Oui</td>
                  <td>Oui</td>
                  <td></td>

                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Lyon 01</td>
                  <td>Gérard Dumortier</td>

                  <td>Arnolde Beaufils</td>
                  <td>8h00</td>
                  <td>17h00</td>
                  <td>Oui</td>
                  <td>Oui</td>
                  <td></td>

                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Lyon 01</td>
                  <td>Gérard Dumortier</td>

                  <td>Thierry Lebeton</td>
                  <td>8h00</td>
                  <td>17h00</td>
                  <td>Oui</td>
                  <td>Oui</td>
                  <td></td>

                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
