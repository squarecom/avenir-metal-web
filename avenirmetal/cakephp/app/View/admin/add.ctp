<!-- app/View/Users/add.ctp -->
<div class="row">
    <div class="col-xs-12 col-sm-12 col-lg-12">
        <div class="widget-box">
            <div class="widget-title"><span class="icon"><i class="fa fa-plus"></i></span><h5>Inscription</h5></div>
            <div class="widget-content nopadding">
                <?php
                $this->Html->addCrumb('Utilisateur', '/users');
                $this->Html->addCrumb('Inscription', '/users/add', 'class=\'current\'');

                echo $this->Form->create('User', array('class'=>'form-horizontal','type'=>'file')); 

                echo $this->Form->input('User.username', 
                    array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Username de co*'),
                            'placeholder'   => 'Votre Pseudo de connection...',
                            'before'        => '',
                            'after'         => '</div>',
                            'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                            'div'           => array('class' => 'form-group'),
                            'class'         => 'form-control input-sm',
                            'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    )
                );

                echo $this->Form->input('User.show_username', 
                    array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Username visible*'),
                            'placeholder'   => 'Votre Pseudo visible...',
                            'before'        => '',
                            'after'         => '</div>',
                            'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10\'>',
                            'div'           => array('class' => 'form-group'),
                            'class'         => 'form-control input-sm',
                            'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    )
                );

                echo $this->Form->input('User.email', 
                    array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Adresse e-mail*'),
                            'placeholder'   => 'Votre Adresse e-mail...',
                            'before'        => '',
                            'after'         => '</div>',
                            'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10\'>',
                            'div'           => array('class' => 'form-group'),
                            'class'         => 'form-control input-sm',
                            'type'          => 'email',
                            'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    )
                );

                echo $this->Form->input('User.password', 
                    array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Mot de passe*'),
                            'placeholder'   => 'Votre mot de passe...',
                            'before'        => '',
                            'after'         => '</div>',
                            'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10\'>',
                            'div'           => array('class' => 'form-group'),
                            'class'         => 'form-control input-sm',
                            'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    )
                );

                echo '<h3>Le reste est pas obligatoire</h3>';

                echo $this->Form->input('Profile.name', 
                    array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Nom'),
                            'placeholder'   => 'Votre nom...',
                            'required'      => false,
                            'before'        => '',
                            'after'         => '</div>',
                            'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10\'>',
                            'div'           => array('class' => 'form-group'),
                            'class'         => 'form-control input-sm',
                            'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    )
                );

                echo $this->Form->input('Profile.fname', 
                    array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Pr&eacutenom'),
                            'placeholder'   => 'Votre Prenom...',
                            'required'      => false,
                            'before'        => '',
                            'after'         => '</div>',
                            'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10\'>',
                            'div'           => array('class' => 'form-group'),
                            'class'         => 'form-control input-sm',
                            'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    )
                );
                
                echo $this->Form->input('Profile.birthdate', 
                    array(
                            'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Date de naissance'),
                            'required'      => false,
                            'type'          =>'date',
                            'dateFormat'   =>'DMY',
                            'minYear'      =>date('Y')-100,
                            'maxYear'      =>date('Y'),
                            'allowEmpty'    => true,
                            'placeholder'   => 'Adresse e-mail...',
                            'before'        => '',
                            'after'         => '</div>',
                            'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10\'>',
                            'div'           => array('class' => 'form-group'),
                            'class'         => 'input-sm',
                            'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                ));

                 echo $this->Form->input('Profile.city', 
                    array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Ville*'),
                            'required'      => false,
                            'placeholder'   => 'Votre Ville...',
                            'before'        => '',
                            'after'         => '</div>',
                            'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10\'>',
                            'div'           => array('class' => 'form-group'),
                            'class'         => 'form-control input-sm',
                            'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    )
                );

                 echo $this->Form->input('Profile.skills', 
                    array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Description*'),
                            'placeholder'   => 'Votre description...',
                            'required'      => false,
                            'before'        => '',
                            'after'         => '</div>',
                            'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10\'>',
                            'div'           => array('class' => 'form-group'),
                            'class'         => 'form-control input-sm',
                            'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    )
                );

                echo $this->Form->input('Profile.context', 
                    array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Pays*'),
                            'placeholder'   => 'Situation...',
                            'required'      => false,
                            'before'        => '',
                            'after'         => '</div>',
                            'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10\'>',
                            'div'           => array('class' => 'form-group'),
                            'class'         => 'form-control input-sm',
                            'options'       => array(
                                                'FR' => 'Francais',
                                                'EN' => 'Angleterre',
                                                'ALL' => 'Allemand',
                                                'ESP' => 'Espagne',
                                                'ITA' => 'Italie',
                                                '5' => 'Suisse',
                                                '0' => 'Autre'
                                                ),
                            'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    )
                );

                 echo $this->Form->input('Profile.hobbies', 
                    array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Hobbies*'),
                            'placeholder'   => 'Adresse e-mail...',
                            'required'      => false,
                            'before'        => '',
                            'after'         => '</div>',
                            'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10\'>',
                            'div'           => array('class' => 'form-group'),
                            'class'         => 'form-control input-sm',
                            'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    )
                );


                echo $this->Form->input('Profile.avatar_file', 
                    array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Avatar'),
                            'type'          => 'file',
                            'required'      => false,
                            'before'        => '',
                            'after'         => '</div>',
                            'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10\'>',
                            'div'           => array('class' => 'form-group'),
                            'class'         => 'form-control input-sm',
                            'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
                    )
                );

                $options = array(
                    'label' => 'S\'inscrire',
                    'div' => array(
                        'class' => 'col-sm-9 col-md-9 col-lg-10',
                    ),
                    'before'        => '',
                    'after'         => '</div>',
                    'class'=>'btn btn-primary'
                );
                echo '<div class="form-group"><label class="col-sm-3 col-md-3 col-lg-2 control-label"></label>';
                echo $this->Form->end($options);
                ?>
            </div>
        </div>
    </div>
</div>