 <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Avenir Metal</a>
        </div>
        <div class="navbar-collapse collapse">
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Rechercher...">
            <button class="btn btn-primary"><i class="glyphicon-search glyphicon"></i></button> 
            <a href="./logout" class="btn btn-danger">Déconnexion</a>
          </form>

        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li ><a href="./index">Accueil</a></li>
            <li class="active"><a href="./chantier">Chantiers</a></li>
            <li><a href="./all">Administrateurs</a></li>
            <li><a href="./employees">Employés</a></li>
            <li><a href="./fiches">Fiches</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="background-color:white;">
          <h1 class="page-header">Tableau de bord</h1>
          <h2 class="sub-header">Chantier</h2>
          <button class="btn btn-success">Ajouter un Chantier</button>
          <hr>
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover avenir-datatable">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Ville</th>
                  <th>Numéro</th>
                  <th>Adresse</th>
                  <th>Commentaire</th>
                  <th>Editer</th>
                  <th>Supprimer</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Paris</td>
                  <td>01</td>
                  <td>98 Allée des Baies, Paris 75000</td>
                  <td></td>
                  <td><i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Paris</td>
                  <td>02</td>
                  <td>78 Rue des Baieyass, Paris 75000</td>
                  <td>Chantier terminé le 5 avril</td>
                  <td> <i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Lyon</td>
                  <td>01</td>
                  <td>64 Allée des Monti, Lyon 69500</td>
                  <td></td>
                  <td> <i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>Lyon</td>
                  <td>02</td>
                  <td>12 Allée des Baies, Lyon 69500</td>
                  <td></td>
                  <td> <i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>5</td>
                  <td>Lyon</td>
                  <td>03</td>
                  <td>111 Chemin des malapier, Lyon 69500</td>
                  <td></td>
                  <td> <i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>6</td>
                  <td>Grenoble</td>
                  <td>01</td>
                  <td>4 Rue des Saint-Jarmin, Grenoble 38000</td>
                  <td>Commencé le 3 Mars - 6 mois minimum</td>
                  <td> <i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>7</td>
                  <td>Grenoble</td>
                  <td>01</td>
                  <td>45 Chemin des Baies, Grenoble 38000</td>
                  <td></td>
                  <td> <i class="glyphicon glyphicon-pencil"></i></td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
              </tbody>
            </table>
          </div>
          <br><br>
          <h2 class="sub-header">Chef de Chantier</h2>
          <button class="btn btn-success">Ajouter un chef de chantier</button>
          <hr>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Chef</th>
                  <th>Chantier</th>
                  <th>Numéro</th>
                  <th>Supprimer</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Paul Dupont</td>
                  <td>Paris</td>
                  <td>01</td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Antoine Chat</td>
                  <td>Paris</td>
                  <td>02</td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Francois Dupier</td>
                  <td>Lyon</td>
                  <td>01</td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>Jean Poulet</td>
                  <td>Lyon</td>
                  <td>02</td>
                  <td><i class="glyphicon glyphicon-trash"></i></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
