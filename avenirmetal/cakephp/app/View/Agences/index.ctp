<div class="row bloc-am">
    <div class="col-sm-12 bloc-min">
    <h4>AGENCES</h4>
    <?php 
        echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> Ajouter une Agence', array('action'=>'add'), array('class'=>'btn btn-success btn-blocked-su','escape'=>false)); 
        $this->Html->addCrumb('Agences', '/Agences');
        //$this->Html->addCrumb('Connexion', '/users/login');
    ?>
    <hr class="hrw">
        <div class="table-responsive">
          <table class="table table-bordered table-hover avenir-datatable ">
            <thead> 
              <tr>
                <th>Informations</th>
                <th>A propos</th>
                <th class="text-center">EDITER</th>
                <th class="text-center">Effacer</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($agences as $agence) { 
                echo '<tr><td>';
                    echo '<span style="font-weight:800;">';
                    echo $agence['Agence']['nom'];
                    echo '</span> (';
                    $tel = $agence['Agence']['tel'];
                    echo substr($tel, 0,2).'.'.substr($tel, 2,2).'.'.substr($tel, 4, 2).'.'.substr($tel, 6, 2).'.'.substr($tel, 8, 2);
                    echo ' - ';
                    echo $agence['Agence']['email'];
                    echo ')';
                    echo '<br/>';
                    echo $agence['Agence']['adresse'];
                    echo ' - ';
                    echo $agence['Agence']['ville'];
                echo '</td><td>';
                echo $agence['Agence']['commentaire'];
                echo '</td><td class="text-center">';
                echo $this->Html->link('<i class="glyphicon glyphicon-pencil"></i>', array('action'=>'edit', $agence['Agence']['id']), array('escape'=>false));
                echo '</td><td class="text-center">';
                echo $this->Form->postLink('<i class="glyphicon glyphicon-trash"></i>', array('action' => 'delete', $agence['Agence']['id']), array('confirm' => 'Etes vous sure?','escape'=>false));
                echo '</td></tr>';
              }
              ?>
            </tbody>
          </table>
        </div>

    </div>
</div>
<!-- <div class="row bloc-am">
    <div class="col-sm-12 bloc-min" style="color:white;">
        <h4>RAPPORT INTERIMAIRE(S) / NON INTERIM(S)</h4> 
        <hr class="hrtr">
        <div class="row text-center">
            <div id="placeholder3" style="height:200px; width:100%;"></div>
        </div>
    </div>
</div> -->
<style type="text/css">
    h4.white {
        color:white;
    }
    hr.hrtr{
        border-top:rgb(87, 153, 250);
    }
    .tickLabel {
        color:white;
        font-size: 15px;
        font-weight: 100;
    }
    div#placeholder3 div.legend table {
        height: 150px;
    }
    div#placeholder3 div.legend table tbody tr td.legendLabel {
        font-size: 20px;
        width:250px;
        text-align: left;
        padding-left:20px;
    }
    div#placeholder3 div.legend table tbody tr td.legendColorBox div {
        border:none !important;
    }

    div#placeholder3 div.legend table tbody tr:first-child td.legendColorBox div:first-child div {
        background: rgb(200, 200, 255);
        border-radius:5px;
        height:35px !important;
        width: 35px !important;
    }
    div#placeholder3 div.legend table tbody tr:last-child td.legendColorBox div:first-child div {
        background: rgb(80, 80, 255);
        border-radius:5px;
        height:35px !important;
        width: 35px !important;
    }  
    .table-bordered>tbody>tr>td {
        vertical-align: middle;
    }
</style>