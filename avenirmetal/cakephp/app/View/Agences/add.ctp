<div class="row bloc-am">
<div class="col-sm-12 bloc-min">
<h4>AJOUTER UNE AGENCE</h4>
<?php 
    echo $this->Html->link('<i class="glyphicon glyphicon-arrow-left"></i> Retourner à l\'écran précédént',
     array('action'=>'index'), array('class'=>'btn btn-danger btn-blocked-dg', 'escape'=>false));
    $this->Html->addCrumb('Agences', '/Agences');
    $this->Html->addCrumb('Ajouter', '/Agences/add');
?>
<hr class="hrw">

    <?php
    echo $this->Form->create('Agence', array('class'=>'form-horizontal','type'=>'file')); 
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Agence.nom', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Nom*'),
                'placeholder'   => 'Le nom de l\'Agence...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    // 3 Email
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Agence.email', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Email 1'),
                'placeholder'   => 'Email 1...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
        /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Agence.email2', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Email 2'),
                'placeholder'   => 'Email 2...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
        /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Agence.email3', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Email 3'),
                'placeholder'   => 'Email 3...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    // 3 Numéros
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Agence.tel', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Téléphone 1'),
                'placeholder'   => 'Téléphone 1...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Agence.tel2', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Téléphone 2'),
                'placeholder'   => 'Téléphone 2...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Agence.tel3', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Téléphone 3'),
                'placeholder'   => 'Téléphone 3...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Agence.fax', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Fax'),
                'placeholder'   => 'Un Fax...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Agence.adresse', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Adresse'),
                'placeholder'   => 'L\'adresse de l\'agence...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Agence.ville', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Ville'),
                'placeholder'   => 'Et la ville...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
        echo $this->Form->input('Agence.cp', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Code Postal'),
                'placeholder'   => 'Le Code Postal...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    echo $this->Form->input('Agence.commentaire', 
        array(  'label'         => array('class'=>'col-sm-3 col-md-3 col-lg-2 control-label', 'text'=>'Commentaire'),
                'placeholder'   => 'Quelques mots...',
                'before'        => '',
                'after'         => '</div>',
                'between'       => '<div class=\'col-sm-9 col-md-9 col-lg-10 \'>',
                'div'           => array('class' => 'form-group'),
                'class'         => 'form-control input-sm',
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'alert alert-danger'))
        )
    );
    echo '<div style="clear:both;"></div>';
    /* --------------------------------------------------------------------------------------------------------------------------------------------- */
    $options = array(
        'label' => 'Ajouter l\'agence',
        'div' => array(
            'class' => 'col-sm-9 col-md-9 col-lg-10',
        ),
        'before'        => '',
        'after'         => '</div>',
        'class'=>'btn btn-success btn-success-my btn-block'
    );
    echo '<div class="form-group"><label class="col-sm-3 col-md-3 col-lg-2 control-label"></label>';
    echo $this->Form->end($options);
    echo '</div></div>';
    ?>
