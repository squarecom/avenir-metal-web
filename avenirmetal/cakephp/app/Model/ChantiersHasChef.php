<?php

class ChantiersHasChef extends AppModel {

    public $useTable = 'chantiers_has_chefs';

    public $belongsTo = array(
        'Chef' => array(
            'className' => 'Chef',
            'foreignKey' => 'chef_id',
        ),
        'Chantier' => array(
            'className' => 'Chantier',
            'foreignKey' => 'chantier_id',
        )
    );

    public $validate = array(
        'chantier_id'=> array(
            'unique'=>array( 
                    "rule"=>array("checkUnique", array("chantier_id", "chef_id")), 
                    "message"=>"Un chantier avec ce numéro et ce nom existe déjà " 
            ),
            'numeric' => array(
                'rule'    => 'numeric',
                'message' => 'Vous devez saisir des chiffres'
            ),
         ),
        'chef_id'=> array(
            'unique'=>array( 
                    "rule"=>array("checkUnique", array("chantier_id", "chef_id")), 
                    "message"=>"Un chef de chantier avec ce chantier et ce nom existe déjà " 
            ),
            'numeric' => array(
                'rule'    => 'numeric',
                'message' => 'Vous devez saisir des chiffres'
            ),
         )

    );
}