<?php
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class Chef extends AppModel {

    public $useTable = 'chefs';

    public $hasMany = array(
        'ChantiersHasChef' => array(
            'className' => 'ChantiersHasChef',
            'foreignKey' => 'chef_id',
        )
    );

    public $belongsTo = array(
        'Employe' => array(
            'className' => 'Employe',
            'foreignKey' => 'employe_id',
        )
    );

        public $validate = array(
            'username' => array(
                'unique'=>array( 
                        "rule"=> 'isUnique',
                        "message"=>"Un chef avec ce pseudonyme existe déja." 
                ),
                'empty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Le login du chef de chantier doit être renseigné.'
                )
             ),
            'password' => array(
                'between' => array(
                    'rule'    => array('between', 1, 50),
                    'message' => 'De 1 a 50 caractères'
                ),
                'empty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'L\'employé doit être renseigné'
                )
             ),
             'employe_id' => array(
                'unique'=>array( 
                        "rule"=> 'isUnique',
                        "message"=>"Cette employé est déja déclaré comme chef de chantier." 
                ),
                'empty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'L\'employé doit être renseigné'
                )
             ),
        );  
    
    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new SimplePasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
        }
        return true;
    }
        
}