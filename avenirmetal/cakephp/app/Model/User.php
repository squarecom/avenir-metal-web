<?php
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {

    public $validate = array(
        'username'=> array(
            'unique' => array(   
                'rule' => 'isUnique',
                'message' => 'Ce pseudo est deja pris, désolé.'   
            ),
            'between' => array(
                'rule'    => array('between', 3, 20),
                'message' => 'Entre 3 et 20 caractères'
            )
         ),   
        'password' => array(
            'between' => array(
                'rule'    => array('between', 2, 20),
                'message' => 'Entre 5 et 20 caractères',
            )
         )
    );
    
    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new SimplePasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
        }
        return true;
    }
        
}