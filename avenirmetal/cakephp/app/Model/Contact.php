<?php

class Contact extends AppModel {

    public $useTable = 'contacts';

    public $validate = array(
        'nom' => array(
            'between' => array(
                'rule'    => array('between', 1, 25),
                'message' => 'Entre 1 et 25 caractères',
            ),
            'empty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Le nom de l\'employe doit être renseigné'
                )
         )
    );       
}