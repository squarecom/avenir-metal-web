<?php

class Chantier extends AppModel {

    public $useTable = 'chantiers';

    public $hasMany = array(
        'ChantiersHasChef' => array(
            'className' => 'ChantiersHasChef',
            'foreignKey' => 'chantier_id',
        )
    );

    public $validate = array(
        'num'=> array(
            'unique'=>array( 
                    "rule"=>array("checkUnique", array("num", "nom")), 
                    "message"=>"Un chantier avec ce numéro et ce nom existe déjà " 
            ),
            'numeric' => array(
                'rule'    => 'numeric',
                'message' => 'Vous devez saisir des chiffres'
            ),
            'empty' => array(
                'rule' => 'notEmpty',
                'message' => 'Le numéro du chantier doit être renseigné'
            )
         ),   
        'nom' => array(     
            'unique'=>array( 
                    "rule"=>array("checkUnique", array("num", "nom")), 
                    "message"=>"Un chantier avec ce numéro et ce nom existe déjà " 
            ),    
            'empty' => array(
                'rule' => 'notEmpty',
                'message' => 'Le numéro du chantier doit être renseigné'
            )
         )
    );  
}