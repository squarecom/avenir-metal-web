<?php
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class Admin extends AppModel {

    public $useTable = 'admin';

    public $validate = array(
        'login'=> array(
            'unique' => array(   
                'rule' => 'isUnique',
                'message' => 'Votre pseudo de connexion est deja pris, désolé.'   
            ),
            'between' => array(
                'rule'    => array('between', 3, 20),
                'message' => 'Entre 3 et 20 caractères'
            ),
         ),   
        'pass' => array(
            'between' => array(
                'rule'    => array('between', 5, 20),
                'message' => 'Entre 5 et 20 caractères',
            ),
         )
    );
    
    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new SimplePasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
        }
        return true;
    }
        
}