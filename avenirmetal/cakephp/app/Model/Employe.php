<?php

class Employe extends AppModel {

    public $useTable = 'employes';

    public $hasMany = array(
        'Fiche' => array(
            'className' => 'Fiche',
            'foreignKey' => 'employe_id',
        )
    );

    public $belongsTo = array(
        'Agence' => array(
            'className' => 'Agence',
            'foreignKey' => 'agence_id'
        )
    );

    public $validate = array(
        'nom' => array(
            'between' => array(
                'rule'    => array('between', 1, 25),
                'message' => 'Entre 1 et 25 caractères',
            ),
            'empty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Le nom de l\'employe doit être renseigné'
                )
         ),
        'prenom' => array(
            'between' => array(
                'rule'    => array('between', 1, 25),
                'message' => 'Entre 1 et 25 caractères',
            ),
            'empty' => array(
                'rule' => 'notEmpty',
                'message' => 'Le prenom de l\'employe doit être renseigné'
            )
         ),
        'interim' => array(
            'empty' => array(
                'rule' => 'notEmpty',
                'message' => 'Le nom de l\'agence interim de l\'employe doit être renseigné'
            )
         ),
    );       
}