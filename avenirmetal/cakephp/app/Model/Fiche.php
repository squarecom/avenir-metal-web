<?php

class Fiche extends AppModel {

    public $useTable = 'chantiers_has_chefs_has_employes';

    public $belongsTo = array(
        'Employe' => array(
            'className' => 'Employe',
            'foreignKey' => 'employe_id',
        ),
        // 'ChantiersHasChefCha' => array(
        //     'className' => 'ChantiersHasChef',
        //     'foreignKey' => 'chantiers_has_chefs_chantiers_id',
        // ),
        // 'ChantiersHasChefChe' => array(
        //     'className' => 'ChantiersHasChef',
        //     'foreignKey' => 'chantiers_has_chefs_chefs_id',
        // )
    );

    public $validate = array(
        'chantiers_has_chefs_chantiers_id' => array(
            'unique'=>array( 
                    "rule"=>array("checkUnique", array("chantiers_has_chefs_chantiers_id", "employe_id","jour")), 
                    "message"=>"Une fiche à ce jour, pour cette employe sur ce chantier existe deja " 
            ),
            'empty' => array(
                'rule' => 'notEmpty',
                'message' => 'Le numéro du chantier doit être renseigné'
            )
         ),
        'chantiers_has_chefs_chefs_id' => array(
           'empty' => array(
                'rule' => 'notEmpty',
                'message' => 'Le nom du chantier doit être renseigné'
            )
         ),
         'employe_id' => array(
            'unique'=>array( 
                    "rule"=>array("checkUnique", array("chantiers_has_chefs_chantiers_id", "employe_id","jour")), 
                    "message"=>"Une fiche à ce jour, pour cette employe sur ce chantier existe deja " 
            ),
            'empty' => array(
                'rule' => 'notEmpty',
                'message' => 'Le login du chef de chantier doit être renseigné'
            )
         ),
         'jour' => array(
            'unique'=>array( 
                    "rule"=>array("checkUnique", array("chantiers_has_chefs_chantiers_id", "employe_id","jour")), 
                    "message"=>"Une fiche à ce jour, pour cette employe sur ce chantier existe deja " 
            ),
            'empty' => array(
                'rule' => 'notEmpty',
                'message' => 'Le numéro du chantier doit être renseigné'
            )
         )
    );       
}