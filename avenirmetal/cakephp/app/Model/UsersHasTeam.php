<?php

class UsersHasTeam extends AppModel {

    public $belongsTo = array(
        'Team' => array(
            'className' => 'Team',
            'foreignKey' => 'team_id'
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );

    public $validate = array(
        'user_id' => array(
            'numbrule' => array(
                'rule'    => 'naturalNumber',
                'message' => 'Chiffres seulement'
            )
        ),
        'team_id' => array(
            'numbrule' => array(
                'rule'    => 'naturalNumber',
                'message' => 'Chiffres seulement'
            )
        ),
        'team_game_id' => array(
            'numbrule' => array(
                'rule'    => 'naturalNumber',
                'message' => 'Chiffres seulement'
            )
        )
    );        
}