<?php

class Team extends AppModel {

    public $hasOne = array(
        'StatTeam' => array(
            'className' => 'StatTeam',
            'dependent' => true
    ));

    /*
    Team hasAndBelongsToMany User

    Team hasMany UsersHasTeam
    UsersHasTeam belongsTo Team, User
    User hasMany UsersHasTeam
    */

    public $hasMany = array(
        'employe' => array(
            'className' => 'employe',
            'foreignKey' => 'employe_id',
        )

    );
        'ActivityUser' => array(
            'className' => 'ActivityUser',
            'foreignKey' => 'user_id',
            'order' => 'ActivityUser.created DESC',
        ),
        'CoteTeam' => array(
            'className' => 'CoteTeam',
            'foreignKey' => 'team_id',
            'order' => 'CoteTeam.created DESC',
        ),
        'CommentTeam' => array(
            'className' => 'CommentTeam',
            'foreignKey' => 'team_id',
            'order' => 'CommentTeam.created DESC',
        ),
        'UsersHasTeam' => array(
            'className' => 'UsersHasTeam',
            'foreignKey' => 'team_id',
            'order' => 'UsersHasTeam.team_game_id ASC',
        ),
        'TournamentsHasTeam' => array(
            'className' => 'TournamentsHasTeam',
            'foreignKey' => 'team_id',
            'order' => 'TournamentsHasTeam.created DESC',
        ),
        'Match1' => array(
            'className' => 'Match',
            'foreignKey' => 'team1_id',
            'order' => 'Match1.created DESC',
        ),
        'Match2' => array(
            'className' => 'Match',
            'foreignKey' => 'team2_id',
            'order' => 'Match2.created DESC',
        ),
    );

    public $actsAs = array(
        'Upload.Upload' => array(
            'fields' => array(
            'image' => 'img/team/:id'
            )
        )
    );

    public $validate = array(
        'title'=> array(
            'unique' => array(   
                'rule' => 'isUnique',
                'message' => 'Le nom de votre équipe est deja pris, désolé.'   
            ),
            'between' => array(
                'rule'    => array('between', 3, 12),
                'message' => 'Entre 3 et 12 caractères'
            ),
         ),  
        'acronym'=> array(
            'unique' => array(   
                'rule' => 'isUnique',
                'message' => 'L\'acronyme de votre équipe est deja pris, désolé.'   
            ),
            'between' => array(
                'rule'    => array('between', 1, 6),
                'message' => 'Entre 1 et 6 caractères'
            ),
         ),  
        'content'=> array(
            'between' => array(
                'rule'    => array('between', 1, 200),
                'message' => 'Entre 1 et 200 caractères'
            ),
         ),  
        'image_file' => array(
            'rule' => array('fileExtension', array('jpg','png')),
            'message' => 'JPEG ou PNG uniquement',
            'allowEmpty' => true,
        )
    );

}