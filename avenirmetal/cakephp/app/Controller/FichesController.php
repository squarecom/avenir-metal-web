<?php
class FichesController extends AppController {
    public $helpers = array('Html', 'Form'); 
    public $name = 'chantier_has_chef_has_employe';
    public $components = array('Paginator', 'RequestHandler');
    public $scaffold; 
    public $uses = array('Fiche','ChantiersHasChef','Chantier','Chef','Employe');
    public $limitResult = 200;
    public function index($id=null) {
        /*$options['joins'] = array(
            array('table' => 'chantiers_has_chefs',
                'alias' => 'CHChef',
                'type' => 'LEFT',
                'conditions' => array(
                    'Fiche.chantiers_has_chefs_chantiers_id = CHChef.chantier_id',
                    'Fiche.chantiers_has_chefs_chefs_id = CHChef.chef_id',
                )
            )
        );*/
        //$options['order'] = array('Fiche.jour'=> 'DESC','Employe.nom'=> 'ASC');
        /* ----------------------------- */
        if(isset($_GET['jour1'])){
            if($_GET['jour1']['month']<10)
                $_GET['jour1']['month'] = '0'.$_GET['jour1']['month'];
            $jmin = $_GET['jour1']['year'].'-'.$_GET['jour1']['month'].'-'.$_GET['jour1']['day'];
        }

        if(isset($_GET['jour2'])){
            if($_GET['jour2']['month']<10)
                $_GET['jour2']['month'] = '0'.$_GET['jour2']['month'];
            $jmax = $_GET['jour2']['year'].'-'.$_GET['jour2']['month'].'-'.$_GET['jour2']['day'];
        }

        if(isset($_GET['id']))
        $id = $_GET['id'];
        $options = array();
        if((isset($jmin))&&(isset($jmax))){
            $options[] = array('jour >='=>$jmin, 'jour <='=>$jmax);
        } else if(isset($jmin)){
            $options[] = array('jour >='=>$jmin);
        } else if(isset($jmax)){
            $options[] = array('jour <='=>$jmax);
        }
        /* ------------------------------------ */
        $paginatation = array(
            'limit' =>  $this->limitResult,
            'order' => array(
                'Fiche.jour' => 'desc', 'Employe.nom'=> 'ASC'
            )
        );
        $this->Paginator->settings = $paginatation;
        $fiches = $this->Paginator->paginate(
            "Fiche",
            $options
        );
        $cpt=0;
        foreach ($fiches as $fiche) {
            $fichesF[$cpt] = $fiche;
            $fichesF[$cpt]['Chantier'] = $this->Chantier->find('first',array(
                'fields'    => array('num','nom'),
                'recursive' => -1,
                'conditions'=> array('Chantier.id'=>$fiche['Fiche']['chantiers_has_chefs_chantiers_id'])
            ));
            $val = $this->Chef->find('first',array(
                'fields'    => array('id', 'employe_id'),
                'conditions'=> array('Chef.id'=>$fiche['Fiche']['chantiers_has_chefs_chefs_id'])
            ));
            $fichesF[$cpt]['Chef'] = $this->Employe->find('first',array(
                'fields'    => array('nom', 'prenom'),
                'recursive' => -1,
                'conditions'=> array('Employe.id'=>$val['Chef']['employe_id'])
            ));
            $cpt++;
        }

        if(isset($fichesF))
            $this->set(array('fiches'=> $fichesF));

        $this->set(array(
            'title_for_layout' => 'Fiches'
        ));
    }

    /**
     * Rajouter le fait que si il y a une id, on recherche les fiches de l'employe avec cette id
     * @param  [type] $id [ID employe]
     * @return [view]     [view]
     */
    public function employe($id=null) {


        if(isset($_GET['jour1'])){
            if($_GET['jour1']['month']<10)
                $_GET['jour1']['month'] = '0'.$_GET['jour1']['month'];
            $jmin = $_GET['jour1']['year'].'-'.$_GET['jour1']['month'].'-'.$_GET['jour1']['day'];
        }

        if(isset($_GET['jour2'])){
            if($_GET['jour2']['month']<10)
                $_GET['jour2']['month'] = '0'.$_GET['jour2']['month'];
            $jmax = $_GET['jour2']['year'].'-'.$_GET['jour2']['month'].'-'.$_GET['jour2']['day'];
        }

        if(isset($_GET['id']))
        $id = $_GET['id'];
        $options = array();
        if(($id!=null)&&(isset($jmin))){
            $options[] = array('employe_id'=>$id, 'jour >='=>$jmin, 'jour <='=>$jmax);
        } else if($id!=null){
            $options[] = array('employe_id'=>$id);
        } else if(isset($jmin)){
            $options[] = array('jour >='=>$jmin, 'jour <='=>$jmax);
        }

        $paginatation = array(
            'limit' =>  $this->limitResult,
            'order' => array(
                 'Employe.nom'=> 'ASC','Fiche.jour' => 'desc'
            )
        );
        $this->Paginator->settings = $paginatation;
        $fiches = $this->Paginator->paginate(
            "Fiche",
            $options
        );

        $cpt=0;
        foreach ($fiches as $fiche) {
            $fichesF[$cpt] = $fiche;
            $fichesF[$cpt]['Chantier'] = $this->Chantier->find('first',array(
                'fields'    => array('num','nom'),
                'recursive' => -1,
                'conditions'=> array('Chantier.id'=>$fiche['Fiche']['chantiers_has_chefs_chantiers_id'])
            ));
            $val = $this->Chef->find('first',array(
                'fields'    => array('id', 'employe_id'),
                'conditions'=> array('Chef.id'=>$fiche['Fiche']['chantiers_has_chefs_chefs_id'])
            ));
            $fichesF[$cpt]['Chef'] = $this->Employe->find('first',array(
                'fields'    => array('nom', 'prenom'),
                'recursive' => -1,
                'conditions'=> array('Employe.id'=>$val['Chef']['employe_id'])
            ));
            $cpt++;
        }

        $employes = $this->Employe->find('all', array(
            'order'=> 'Employe.prenom ASC'
        ));
        foreach ($employes as $employe)
            $employesF[$employe['Employe']['id']] = $employe['Employe']['prenom'].' '.$employe['Employe']['nom'];

        if(isset($fichesF))
            $this->set(array('fiches'=> $fichesF));

        if(isset($employesF))
            $this->set(array('employes'=> $employesF));

        $this->set(array(
            'title_for_layout' => 'Fiches'
        ));
    }


    /**
     * Rajouter le fait que si il y a une id, on recherche les fiches du chantier avec cette id
     * @param  [type] $id [ID chantier]
     * @return [view]     [view]
     */
    public function chantier($id=null) {

        if(isset($_GET['jour1'])){
            if($_GET['jour1']['month']<10)
                $_GET['jour1']['month'] = '0'.$_GET['jour1']['month'];
            $jmin = $_GET['jour1']['year'].'-'.$_GET['jour1']['month'].'-'.$_GET['jour1']['day'];
        }

        if(isset($_GET['jour2'])){
            if($_GET['jour2']['month']<10)
                $_GET['jour2']['month'] = '0'.$_GET['jour2']['month'];
            $jmax = $_GET['jour2']['year'].'-'.$_GET['jour2']['month'].'-'.$_GET['jour2']['day'];
        }

        if(isset($_GET['id']))
            $id = $_GET['id'];
        $options = array();
        if(($id!=null)&&(isset($jmin))){
            $options[] = array('chantiers_has_chefs_chantiers_id'=>$id, 'jour >='=>$jmin, 'jour <='=>$jmax);
        } else if($id!=null){
            $options[] = array('chantiers_has_chefs_chantiers_id'=>$id);
        } else if(isset($jmin)){
            $options[] = array('jour >='=>$jmin, 'jour <='=>$jmax);
        }

        $paginatation = array(
            'limit' =>  $this->limitResult,
            'order' => array(
                 'Fiche.chantiers_has_chefs_chantiers_id'=> 'DESC','Fiche.jour'=> 'DESC','Employe.nom'=> 'ASC'
            )
        );
        $this->Paginator->settings = $paginatation;
        $fiches = $this->Paginator->paginate(
            "Fiche",
            $options
        );

        $cpt=0;
        foreach ($fiches as $fiche) {
            $fichesF[$cpt] = $fiche;
            $fichesF[$cpt]['Chantier'] = $this->Chantier->find('first',array(
                'fields'    => array('num','nom'),
                'recursive' => -1,
                'conditions'=> array('Chantier.id'=>$fiche['Fiche']['chantiers_has_chefs_chantiers_id'])
            ));
            $val = $this->Chef->find('first',array(
                'fields'    => array('id', 'employe_id'),
                'conditions'=> array('Chef.id'=>$fiche['Fiche']['chantiers_has_chefs_chefs_id'])
            ));
            $fichesF[$cpt]['Chef'] = $this->Employe->find('first',array(
                'fields'    => array('nom', 'prenom'),
                'recursive' => -1,
                'conditions'=> array('Employe.id'=>$val['Chef']['employe_id'])
            ));
            $cpt++;
        }

        $chantiers = $this->Chantier->find('all', array(
            'order'=> 'Chantier.num ASC'
        ));
        foreach ($chantiers as $chantier) {
            $chantiersF[$chantier['Chantier']['id']] = $chantier['Chantier']['num'].' '.$chantier['Chantier']['nom'];
        }

        if(isset($chantiersF))
            $this->set(array('chantiers'=> $chantiersF));
        
        if(isset($fichesF))
            $this->set(array('fiches'=> $fichesF));

        $this->set(array(
            'title_for_layout' => 'Fiches'
        ));
    }

    /**
     * Rajouter le fait que si il y a une id, on recherche les fiches du chantier avec cette id
     * @param  [type] $id [ID employe]
     * @return [view]     [view]
     */
    public function chef($id=null) {

        if(isset($_GET['jour1'])){
            if($_GET['jour1']['month']<10)
                $_GET['jour1']['month'] = '0'.$_GET['jour1']['month'];
            $jmin = $_GET['jour1']['year'].'-'.$_GET['jour1']['month'].'-'.$_GET['jour1']['day'];
        }

        if(isset($_GET['jour2'])){
            if($_GET['jour2']['month']<10)
                $_GET['jour2']['month'] = '0'.$_GET['jour2']['month'];
            $jmax = $_GET['jour2']['year'].'-'.$_GET['jour2']['month'].'-'.$_GET['jour2']['day'];
        }

        if(isset($_GET['id']))
            $id = $_GET['id'];
        $options = array();
        if(($id!=null)&&(isset($jmin))){
            $options[] = array('chantiers_has_chefs_chefs_id'=>$id, 'jour >='=>$jmin, 'jour <='=>$jmax);
        } else if($id!=null){
            $options[] = array('chantiers_has_chefs_chefs_id'=>$id);
        } else if(isset($jmin)){
            $options[] = array('jour >='=>$jmin, 'jour <='=>$jmax);
        }

        $paginatation = array(
            'limit' =>  $this->limitResult,
            'order' => array(
                  'Fiche.chantiers_has_chefs_chefs_id'=> 'DESC','Fiche.jour'=> 'DESC','Employe.nom'=> 'ASC'
            )
        );
        $this->Paginator->settings = $paginatation;
        $fiches = $this->Paginator->paginate(
            "Fiche",
            $options
        );

        $cpt=0;
        foreach ($fiches as $fiche) {
            $fichesF[$cpt] = $fiche;
            $fichesF[$cpt]['Chantier'] = $this->Chantier->find('first',array(
                'fields'    => array('num','nom'),
                'recursive' => -1,
                'conditions'=> array('Chantier.id'=>$fiche['Fiche']['chantiers_has_chefs_chantiers_id'])
            ));
            $val = $this->Chef->find('first',array(
                'fields'    => array('id', 'employe_id'),
                'conditions'=> array('Chef.id'=>$fiche['Fiche']['chantiers_has_chefs_chefs_id'])
            ));
            $fichesF[$cpt]['Chef'] = $this->Employe->find('first',array(
                'fields'    => array('nom', 'prenom'),
                'recursive' => -1,
                'conditions'=> array('Employe.id'=>$val['Chef']['employe_id'])
            ));
            $cpt++;
        }

        $chefs = $this->Chef->find('all', array(
            'order'=> 'Employe.prenom ASC'
        ));
        foreach ($chefs as $chef) {
            $emp = $this->Employe->find('first', array(
                'recursive'=>-1,
                'conditions'=> array('Employe.id'=>$chef['Chef']['employe_id'])
            ));
            $chefsF[$chef['Chef']['id']] = $emp['Employe']['prenom'].' '.$emp['Employe']['nom'];
        }
        if(isset($chefsF))
            $this->set(array('chefs'=> $chefsF));

        if(isset($fichesF))
            $this->set(array('fiches'=> $fichesF));

        $this->set(array(
            'title_for_layout' => 'Fiches'
        ));
    }


    /**
     * @return [view]     [view]
     */
    public function add() {
        if ($this->request->is('post')) {
            if (!empty($this->request->data)) {  

                /* ------------ */
                $a = $this->request->data['Fiche']['jour']['year'].'-'.$this->request->data['Fiche']['jour']['month'].'-'.$this->request->data['Fiche']['jour']['day'];
                if($this->request->data['Fiche']['matin_debut']['meridian']=='pm'){
                    $this->request->data['Fiche']['matin_debut']['hour'] = $this->request->data['Fiche']['matin_debut']['hour']+12;
                }
                if($this->request->data['Fiche']['matin_debut']['hour']==24){
                    $this->request->data['Fiche']['matin_debut']['hour'] =23;
                    $this->request->data['Fiche']['matin_debut']['min'] =45;
                }
                $b = $this->request->data['Fiche']['matin_debut']['hour'].':'.$this->request->data['Fiche']['matin_debut']['min'].':00';
                $this->request->data['Fiche']['matin_debut'] = date('Y-m-d H:i:s', strtotime("$a $b"));
                /* ------------ */
                $a = $this->request->data['Fiche']['jour']['year'].'-'.$this->request->data['Fiche']['jour']['month'].'-'.$this->request->data['Fiche']['jour']['day'];
                if($this->request->data['Fiche']['matin_fin']['meridian']=='pm'){
                    $this->request->data['Fiche']['matin_fin']['hour'] = $this->request->data['Fiche']['matin_fin']['hour']+12;
                }
                if($this->request->data['Fiche']['matin_fin']['hour']==24){
                    $this->request->data['Fiche']['matin_fin']['hour'] =23;
                    $this->request->data['Fiche']['matin_fin']['min'] =45;
                }
                $b = $this->request->data['Fiche']['matin_fin']['hour'].':'.$this->request->data['Fiche']['matin_fin']['min'].':00';
                $this->request->data['Fiche']['matin_fin'] = date('Y-m-d H:i:s', strtotime("$a $b"));
                /* ------------ */
                $a = $this->request->data['Fiche']['jour']['year'].'-'.$this->request->data['Fiche']['jour']['month'].'-'.$this->request->data['Fiche']['jour']['day'];
                if($this->request->data['Fiche']['aprem_debut']['meridian']=='pm'){
                    $this->request->data['Fiche']['aprem_debut']['hour'] = $this->request->data['Fiche']['aprem_debut']['hour']+12;
                }
                if($this->request->data['Fiche']['aprem_debut']['hour']==24){
                    $this->request->data['Fiche']['aprem_debut']['hour'] =23;
                    $this->request->data['Fiche']['aprem_debut']['min'] =45;
                }
                $b = $this->request->data['Fiche']['aprem_debut']['hour'].':'.$this->request->data['Fiche']['aprem_debut']['min'].':00';
                $this->request->data['Fiche']['aprem_debut'] = date('Y-m-d H:i:s', strtotime("$a $b"));
                /* ------------ */
                $a = $this->request->data['Fiche']['jour']['year'].'-'.$this->request->data['Fiche']['jour']['month'].'-'.$this->request->data['Fiche']['jour']['day'];
                if($this->request->data['Fiche']['aprem_fin']['meridian']=='pm'){
                    $this->request->data['Fiche']['aprem_fin']['hour'] = $this->request->data['Fiche']['aprem_fin']['hour']+12;
                }
                if($this->request->data['Fiche']['aprem_fin']['hour']==24){
                    $this->request->data['Fiche']['aprem_fin']['hour'] =23;
                    $this->request->data['Fiche']['aprem_fin']['min'] =45;
                }
                $b = $this->request->data['Fiche']['aprem_fin']['hour'].':'.$this->request->data['Fiche']['aprem_fin']['min'].':00';
                $this->request->data['Fiche']['aprem_fin'] = date('Y-m-d H:i:s', strtotime("$a $b"));
                /* ------------ */            
                $this->Fiche->create();

                //$this->request->data('matin_debut') = date('Y-m-d H:i:s', strtotime("$date $time"));


                if ($this->Fiche->save($this->request->data)){
                    $this->Session->setFlash(__('La fiche à bien été ajouté.'),'flash_custom');
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('Erreur majeur. Veuillez recommencer'),'flash_err');
                }     
            } else {
                $this->Session->setFlash(__('Toutes les données demandées non pas étaient complétées'),'flash_err');
            }            
        }

        $chantiers = $this->Chantier->find('all',array(
             'conditions'=> array('clos'=> 0),
             'order' => 'num ASC'
        ));
        foreach ($chantiers as $chantier) {
            $chantiersF[$chantier['Chantier']['id']] = $chantier['Chantier']['num'].' '.$chantier['Chantier']['nom'];
        }

        $chefs = $this->Chef->find('all', array(
            'order' => 'prenom ASC'
        ));
        foreach ($chefs as $chef) {
            $chefsF[$chef['Chef']['id']] = $chef['Employe']['prenom'].' '.$chef['Employe']['nom'];
        }

        $employes = $this->Employe->find('all', array(
            'order' => 'prenom ASC'
        ));
        foreach ($employes as $employe) {
            $employesF[$employe['Employe']['id']] = $employe['Employe']['prenom'].' '.$employe['Employe']['nom'];
        }

        $mois = array(1=>'Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Décembre');

        $this->set(array(
            'cmois'     => $mois,
            'chantiers' => $chantiersF,
            'chefs' => $chefsF,
            'employes'  => $employesF,
            'title_for_layout' => 'Ajout'
        ));
    }


    /**
     * @param  [type] $id [ID fiche]
     * @return [view]     [view]
     */
    public function edit($id) {

        if (!isset($id)){ return $this->redirect(array('action' => 'index')); }

        $this->Fiche->id = $id;
        if (!$this->Fiche->exists()) {
            $this->Session->setFlash(__('L\'utilisateur demandé n\'existe pas ou plus, désolé.'),'flash_err');
            return $this->redirect(array('action' => 'index'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            /* ------------ */
            $a = $this->request->data['Fiche']['jour']['year'].'-'.$this->request->data['Fiche']['jour']['month'].'-'.$this->request->data['Fiche']['jour']['day'];
            if($this->request->data['Fiche']['matin_debut']['meridian']=='pm'){
                $this->request->data['Fiche']['matin_debut']['hour'] = $this->request->data['Fiche']['matin_debut']['hour']+12;
            }
            if($this->request->data['Fiche']['matin_debut']['hour']==24){
                $this->request->data['Fiche']['matin_debut']['hour'] =23;
                $this->request->data['Fiche']['matin_debut']['min'] =45;
            }
            $b = $this->request->data['Fiche']['matin_debut']['hour'].':'.$this->request->data['Fiche']['matin_debut']['min'].':00';
            $this->request->data['Fiche']['matin_debut'] = date('Y-m-d H:i:s', strtotime("$a $b"));
            /* ------------ */
            $a = $this->request->data['Fiche']['jour']['year'].'-'.$this->request->data['Fiche']['jour']['month'].'-'.$this->request->data['Fiche']['jour']['day'];
            if($this->request->data['Fiche']['matin_fin']['meridian']=='pm'){
                $this->request->data['Fiche']['matin_fin']['hour'] = $this->request->data['Fiche']['matin_fin']['hour']+12;
            }
            if($this->request->data['Fiche']['matin_fin']['hour']==24){
                $this->request->data['Fiche']['matin_fin']['hour'] =23;
                $this->request->data['Fiche']['matin_fin']['min'] =45;
            }
            $b = $this->request->data['Fiche']['matin_fin']['hour'].':'.$this->request->data['Fiche']['matin_fin']['min'].':00';
            $this->request->data['Fiche']['matin_fin'] = date('Y-m-d H:i:s', strtotime("$a $b"));
            /* ------------ */
            $a = $this->request->data['Fiche']['jour']['year'].'-'.$this->request->data['Fiche']['jour']['month'].'-'.$this->request->data['Fiche']['jour']['day'];
            if($this->request->data['Fiche']['aprem_debut']['meridian']=='pm'){
                $this->request->data['Fiche']['aprem_debut']['hour'] = $this->request->data['Fiche']['aprem_debut']['hour']+12;
            }
            if($this->request->data['Fiche']['aprem_debut']['hour']==24){
                $this->request->data['Fiche']['aprem_debut']['hour'] =23;
                $this->request->data['Fiche']['aprem_debut']['min'] =45;
            }
            $b = $this->request->data['Fiche']['aprem_debut']['hour'].':'.$this->request->data['Fiche']['aprem_debut']['min'].':00';
            $this->request->data['Fiche']['aprem_debut'] = date('Y-m-d H:i:s', strtotime("$a $b"));
            /* ------------ */
            $a = $this->request->data['Fiche']['jour']['year'].'-'.$this->request->data['Fiche']['jour']['month'].'-'.$this->request->data['Fiche']['jour']['day'];
            if($this->request->data['Fiche']['aprem_fin']['meridian']=='pm'){
                $this->request->data['Fiche']['aprem_fin']['hour'] = $this->request->data['Fiche']['aprem_fin']['hour']+12;
            }
            if($this->request->data['Fiche']['aprem_fin']['hour']==24){
                $this->request->data['Fiche']['aprem_fin']['hour'] =23;
                $this->request->data['Fiche']['aprem_fin']['min'] =45;
            }
            $b = $this->request->data['Fiche']['aprem_fin']['hour'].':'.$this->request->data['Fiche']['aprem_fin']['min'].':00';
            $this->request->data['Fiche']['aprem_fin'] = date('Y-m-d H:i:s', strtotime("$a $b"));
            /* ------------ */            

            if ($this->Fiche->save($this->request->data)) {
                $this->Session->setFlash(__('Les changements ont été effectués'),'flash_custom');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('L\'utilisateur n\'a pas pu être sauvegarder. Veuillez réessayer SVP.'),'flash_err');
            } 
        } else {
            $this->request->data = $this->Fiche->read(null, $id);
        }

        $chantiers = $this->Chantier->find('all');
        foreach ($chantiers as $chantier) {
            $chantiersF[$chantier['Chantier']['id']] = $chantier['Chantier']['num'].' '.$chantier['Chantier']['nom'];
        }

        $chefs = $this->Chef->find('all');
        foreach ($chefs as $chef) {
            $chefsF[$chef['Chef']['id']] = $chef['Employe']['prenom'].' '.$chef['Employe']['nom'];
        }

        $employes = $this->Employe->find('all');
        foreach ($employes as $employe) {
            $employesF[$employe['Employe']['id']] = $employe['Employe']['prenom'].' '.$employe['Employe']['nom'];
        }
        
        $this->request->data = $this->Fiche->find('first', 
            array('conditions'=> array('Fiche.id'=>$id))
        );

        /* ------------------------- */
        $md = $this->request->data['Fiche']['matin_debut'];
        unset($this->request->data['Fiche']['matin_debut']);
        $this->request->data['Fiche']['matin_debut']['hour'] = substr($md, 11,2);
        $this->request->data['Fiche']['matin_debut']['min'] = substr($md, 14,2);
        if($this->request->data['Fiche']['matin_debut']['hour']<12)
            $this->request->data['Fiche']['matin_debut']['meridian'] = 'am';
        else
            $this->request->data['Fiche']['matin_debut']['meridian'] = 'pm';
        /* ------------------------- */
        $mf = $this->request->data['Fiche']['matin_fin'];
        unset($this->request->data['Fiche']['matin_fin']);
        $this->request->data['Fiche']['matin_fin']['hour'] = substr($mf, 11,2);
        $this->request->data['Fiche']['matin_fin']['min'] = substr($mf, 14,2);
        if($this->request->data['Fiche']['matin_fin']['hour']<=12)
            $this->request->data['Fiche']['matin_fin']['meridian'] = 'am';
        else
            $this->request->data['Fiche']['matin_fin']['meridian'] = 'pm';
        /* ------------------------- */
        $ad = $this->request->data['Fiche']['aprem_debut'];
        unset($this->request->data['Fiche']['aprem_debut']);
        $this->request->data['Fiche']['aprem_debut']['hour'] = substr($ad, 11,2);
        $this->request->data['Fiche']['aprem_debut']['min'] = substr($ad, 14,2);
        if($this->request->data['Fiche']['aprem_debut']['hour']<=12)
            $this->request->data['Fiche']['aprem_debut']['meridian'] = 'am';
        else
            $this->request->data['Fiche']['aprem_debut']['meridian'] = 'pm';
        /* ------------------------- */
        $af = $this->request->data['Fiche']['aprem_fin'];
        unset($this->request->data['Fiche']['aprem_fin']);
        $this->request->data['Fiche']['aprem_fin']['hour'] = substr($af, 11,2);
        $this->request->data['Fiche']['aprem_fin']['min'] = substr($af, 14,2);
        if($this->request->data['Fiche']['aprem_fin']['hour']<=12)
            $this->request->data['Fiche']['aprem_fin']['meridian'] = 'am';
        else
            $this->request->data['Fiche']['aprem_fin']['meridian'] = 'pm';
        /* ------------------------- */
        $mois = array(1=>'Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Décembre');
        $this->set(array(
            'cmois'     => $mois,
            'chantiers' => $chantiersF,
            'chefs' => $chefsF,
            'employes'  => $employesF,
            'title_for_layout' => 'Edition'
        ));
    } 
/*---------------------------------------------------------------------------------------------------------------------------------------------*/
     public  function export(){
        //$data = $this->Member->find('all');
        $data = array (
            'id' => 1, 
            'email' => 'arnaud.roland@gmail.com',
            'created' => '26/06/2014'
        );

        //var_dump($data);
        $this->set(compact('data'));
     
        $filename  = 'export_' . strftime('%Y-%m-%d') . '.xls';
     
        $this->autoLayout = false;
     
        //App::import('Core', 'File');
        App::uses('Folder', 'Utility');
        App::uses('File', 'Utility');

        //On créer le fichier en mode écriture avec les résultat de la vue
        $file = new File('..'.DS.'exports'.DS.$filename, true);
        $file->write($this->render());
        $file->close();
        
        //Redirecttion avec succès
        $this->Session->setFlash("Nouveau fichier disponible.", 'message_ok');
        //$this->redirect($this->referer());
    } 

/*---------------------------------------------------------------------------------------------------------------------------------------------*/
    public function exportplugin() {

        $fiches = $this->Fiche->find('all');
        debug($fiches);

        $data = $fiches[0]['Employe'];
        die();
        $_serialize = 'data';

        $this->viewClass = 'CsvView.Csv';
        $this->set(compact('data', '_serialize'));
        
    }






































/*---------------------------------------------------------------------------------------------------------------------------------------------*/
    public function exportemp($jmin=null,$jmax=null,$id=null) {


        /*if(isset($_GET['jour1'])){
            if($_GET['jour1']['month']<10)
                $_GET['jour1']['month'] = '0'.$_GET['jour1']['month'];
            $jmin = $_GET['jour1']['year'].'-'.$_GET['jour1']['month'].'-'.$_GET['jour1']['day'];
        }
        if(isset($_GET['jour2'])){
            if($_GET['jour2']['month']<10)
                $_GET['jour2']['month'] = '0'.$_GET['jour2']['month'];
            $jmax = $_GET['jour2']['year'].'-'.$_GET['jour2']['month'].'-'.$_GET['jour2']['day'];
        }*/
        //debug($jmin); debug($jmax); debug($id); die();

        $options['joins'] = array(
            array('table' => 'chantiers_has_chefs',
                'alias' => 'CHChef',
                'type' => 'LEFT',
                'conditions' => array(
                    'Fiche.chantiers_has_chefs_chantiers_id = CHChef.chantier_id',
                    'Fiche.chantiers_has_chefs_chefs_id = CHChef.chef_id',
                )
            )
        );
        $options['order'] = array('Fiche.employe_id'=> 'DESC', 'Fiche.jour'=> 'DESC');

        if(($id!=null)&&(isset($jmin))){
            $options['conditions'] = array('employe_id'=>$id, 'jour >='=>$jmin, 'jour <='=>$jmax);
        } else if($id!=null){
            $options['conditions'] = array('employe_id'=>$id);
        } else if(isset($jmin)){
            $options['conditions'] = array('jour >='=>$jmin, 'jour <='=>$jmax);
        }

        $fiches = $this->Fiche->find('all', $options);

        $cpt=0;
        foreach ($fiches as $fiche) {
            $fichesF[$cpt] = $fiche;
            $fichesF[$cpt]['Chantier'] = $this->Chantier->find('first',array(
                'fields'    => array('num','nom'),
                'recursive' => -1,
                'conditions'=> array('Chantier.id'=>$fiche['Fiche']['chantiers_has_chefs_chantiers_id'])
            ));
            $val = $this->Chef->find('first',array(
                'fields'    => array('id', 'employe_id'),
                'conditions'=> array('Chef.id'=>$fiche['Fiche']['chantiers_has_chefs_chefs_id'])
            ));
            $fichesF[$cpt]['Chef'] = $this->Employe->find('first',array(
                'fields'    => array('nom', 'prenom'),
                'recursive' => -1,
                'conditions'=> array('Employe.id'=>$val['Chef']['employe_id'])
            ));
            $cpt++;
        }

        $employes = $this->Employe->find('all');
        foreach ($employes as $employe)
            $employesF[$employe['Employe']['id']] = $employe['Employe']['prenom'].' '.$employe['Employe']['nom'];
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $told = 1;
        $totalh =0;
        $totalgd = 0;
        $totalr = 0;
        $totalt = 0;
        $totaln = 0;
        $fiches = $fichesF;
        $employes = $employesF;
        $jour = array(' ','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche');
        $mois = array(1=>'Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Décembre');

        if(isset($fiches))
        foreach ($fiches as $employe) {
            if($told!=0){   
                if(!isset($nom)){
                    $nom = $employe['Employe']['prenom'].' '.$employe['Employe']['nom'];
                    $nomf = $employe['Employe']['prenom'].'-'.$employe['Employe']['nom'];
                    $data[] = array($nom,'','','','','','','','','','');
                    $data[] = array('Periode','Date','Chef','Chantier', 'GD','Repas','Dep','Horaires 1','Horaires 2','Total');
                }
                if(($employe['Employe']['prenom'].' '.$employe['Employe']['nom'])!=$nom){
                    $nom =$employe['Employe']['prenom'].' '.$employe['Employe']['nom'];
                    $told =0;
                }
               /* if($told==0){
                    $data[] = array($totaln,$totalgd,$totalr,$totalt,$totalh)
                    //echo $employe['Employe']['prenom'].' '.$employe['Employe']['nom'].;
                }*/


                $heur1 = intval(date("G", strtotime($employe['Fiche']['matin_debut'])));
                $heur2 = intval(date("G", strtotime($employe['Fiche']['matin_fin']))); 
                $heur3 = intval(date("G", strtotime($employe['Fiche']['aprem_debut'])));
                $heur4 = intval(date("G", strtotime($employe['Fiche']['aprem_fin']))); 
                $nuit  = false;

                if((22<=$heur1)||($heur1<=7)){$nuit=true;} else {
                    if((22<=$heur2)||($heur2<=7)){$nuit=true;} else {
                        if((22<=$heur3)||($heur3<=7)){$nuit=true;} else {
                            if((22<=$heur4)||($heur4<=7)){$nuit=true;}
                        }
                    }
                }
                if($nuit){
                    $d1 = 'Nuit'; 
                } else {$d1 = 'Jour'; }

                $jdt = date("N", strtotime($employe['Fiche']['jour']));
                $ndt = date("d", strtotime($employe['Fiche']['jour']));
                $adt = date("n", strtotime($employe['Fiche']['jour']));
                $d2 = $jour[$jdt].' '.$ndt.' '.$mois[$adt];

                $d3 = $employe['Chef']['Employe']['prenom'].' '.$employe['Chef']['Employe']['nom'];
                $d4 = $employe['Chantier']['Chantier']['num'].' '.$employe['Chantier']['Chantier']['nom'];

                switch ($employe['Fiche']['granddeplacement']) {
                    case 1: $d5 = 'Oui'; $totalgd++;break;
                    default: $d5 = 'Non'; break;
                }
                switch ($employe['Fiche']['repas']) {
                    case 1: $d6 = 'Oui'; $totalr++; break;
                    default: $d6 = 'Non'; break;
                }
                switch ($employe['Fiche']['trajet']) {
                    case 1: $d7 = 'Oui'; $totalt++; break;
                    default: $d7 = 'Non';; break;
                }
                /* -------------------------------------------------------------------------------------------------------------- */
                $d8 = date("H:i", strtotime($employe['Fiche']['matin_debut'])).' a '.date("H:i", strtotime($employe['Fiche']['matin_fin']));
                $d9 = date("H:i", strtotime($employe['Fiche']['aprem_debut'])).' a '.date("H:i", strtotime($employe['Fiche']['aprem_fin']));
                /* -------------------------------------------------------------------------------------------------------------- */
                $min1 = intval(date("i", strtotime($employe['Fiche']['matin_debut']))); // 0
                $min1 = round(($min1*100)/60);
                $total1 = $heur1.'.'.$min1;
                $min2 = intval(date("i", strtotime($employe['Fiche']['matin_fin']))); // 0
                $min2 = round(($min2*100)/60);
                $total2 = $heur2.'.'.$min2;
                $bloc1 = $total2-$total1;
            
                $min3 = intval(date("i", strtotime($employe['Fiche']['aprem_debut']))); // 0
                $min3 = round(($min3*100)/60);
                $total3 = $heur3.'.'.$min3;
                $min4 = intval(date("i", strtotime($employe['Fiche']['aprem_fin']))); // 0
                $min4 = round(($min4*100)/60);
                $total4 = $heur4.'.'.$min4;
                $bloc2 = $total4-$total3;

                $total = $bloc1+$bloc2;
                $totalh = $totalh+$total;
                $totaln++;
                $d10 = $total;
                /* -------------------------------------------------------------------------------------------------------------- */
            }
            $data[] = array($d1,$d2,$d3,$d4,$d5,$d6,$d7,$d8,$d9,$d10);
        }

        //debug($fiches);
        //debug($told);
        //die();

        
        $data[] = array($totaln.' Fiche(s)','','','',$totalgd,$totalr,$totalt,'','',$totalh);
         // -------------------------------------------------------------------
        $file_name = $nomf.'-du-'.$jmin.'-au-'.$jmax.'';
        $this->response->download($file_name);
        $_serialize = 'data';
        $this->viewClass = 'CsvView.Csv';
        $this->set(compact('data', '_serialize'));
    }
/*---------------------------------------------------------------------------------------------------------------------------------------------*/   





































/*---------------------------------------------------------------------------------------------------------------------------------------------*/
    public function exportchantier($jmin=null,$jmax=null,$id=null) {







        if(($id!=null)&&(isset($jmin))){
            $options['conditions'] = array('chantiers_has_chefs_chantiers_id'=>$id, 'jour >='=>$jmin, 'jour <='=>$jmax);
        } else if($id!=null){
            $options['conditions'] = array('chantiers_has_chefs_chantiers_id'=>$id);
        } else if(isset($jmin)){
            $options['conditions'] = array('jour >='=>$jmin, 'jour <='=>$jmax);
        }



        $options['joins'] = array(
            array('table' => 'chantiers_has_chefs',
                'alias' => 'CHChef',
                'type' => 'LEFT',
                'conditions' => array(
                    'Fiche.chantiers_has_chefs_chantiers_id = CHChef.chantier_id',
                    'Fiche.chantiers_has_chefs_chefs_id = CHChef.chef_id',
                )
            )
        );
        $options['order'] = array('Fiche.chantiers_has_chefs_chantiers_id'=> 'DESC','Fiche.jour'=> 'DESC','Employe.nom'=> 'ASC');
        $fiches = $this->Fiche->find('all', $options);
        $cpt=0;
        foreach ($fiches as $fiche) {
            $fichesF[$cpt] = $fiche;
            $fichesF[$cpt]['Chantier'] = $this->Chantier->find('first',array(
                'fields'    => array('num','nom'),
                'recursive' => -1,
                'conditions'=> array('Chantier.id'=>$fiche['Fiche']['chantiers_has_chefs_chantiers_id'])
            ));
            $val = $this->Chef->find('first',array(
                'fields'    => array('id', 'employe_id'),
                'conditions'=> array('Chef.id'=>$fiche['Fiche']['chantiers_has_chefs_chefs_id'])
            ));
            $fichesF[$cpt]['Chef'] = $this->Employe->find('first',array(
                'fields'    => array('nom', 'prenom'),
                'recursive' => -1,
                'conditions'=> array('Employe.id'=>$val['Chef']['employe_id'])
            ));
            $cpt++;
        }

        $chantiers = $this->Chantier->find('all');
        foreach ($chantiers as $chantier)
            $chantiersF[$chantier['Chantier']['id']] = $chantier['Chantier']['num'].' '.$chantier['Chantier']['nom'];
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $told = 1;
        $totalh =0;
        $totalgd = 0;
        $totalr = 0;
        $totalt = 0;
        $totaln = 0;
        $fiches = $fichesF;
        $chantiers = $chantiersF;
        $jour = array(' ','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche');
        $mois = array(1=>'Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Décembre');

        if(isset($fiches))
        foreach ($fiches as $employe) {
            if($told!=0){   
                if(!isset($nom)){
                    $nom  = $employe['Chantier']['Chantier']['num'].' '.$employe['Chantier']['Chantier']['nom'];
                    $nomf = $employe['Chantier']['Chantier']['num'].'-'.$employe['Chantier']['Chantier']['nom'];

                    $data[] = array($nom,'','','','','','','','','','');
                    $data[] = array('Periode','Date','Employe','Chef', 'GD','Repas','Dep','Horaires 1','Horaires 2','Total');
                }
                if(($employe['Chantier']['Chantier']['num'].' '.$employe['Chantier']['Chantier']['nom'])!=$nom){
                    $nom =$employe['Chantier']['Chantier']['num'].$employe['Chantier']['Chantier']['nom'];
                    $told =0;
                }
               /* if($told==0){
                    $data[] = array($totaln,$totalgd,$totalr,$totalt,$totalh)
                    //echo $employe['Employe']['prenom'].' '.$employe['Employe']['nom'].;
                }*/


                $heur1 = intval(date("G", strtotime($employe['Fiche']['matin_debut'])));
                $heur2 = intval(date("G", strtotime($employe['Fiche']['matin_fin']))); 
                $heur3 = intval(date("G", strtotime($employe['Fiche']['aprem_debut'])));
                $heur4 = intval(date("G", strtotime($employe['Fiche']['aprem_fin']))); 
                $nuit  = false;

                if((22<=$heur1)||($heur1<=7)){$nuit=true;} else {
                    if((22<=$heur2)||($heur2<=7)){$nuit=true;} else {
                        if((22<=$heur3)||($heur3<=7)){$nuit=true;} else {
                            if((22<=$heur4)||($heur4<=7)){$nuit=true;}
                        }
                    }
                }
                if($nuit){
                    $d1 = 'Nuit'; 
                } else {$d1 = 'Jour'; }

                $jdt = date("N", strtotime($employe['Fiche']['jour']));
                $ndt = date("d", strtotime($employe['Fiche']['jour']));
                $adt = date("n", strtotime($employe['Fiche']['jour']));
                $d2 = $jour[$jdt].' '.$ndt.' '.$mois[$adt];

                
                $d3 = $employe['Employe']['prenom'].' '.$employe['Employe']['nom'];
                $d4 = $employe['Chef']['Employe']['prenom'].' '.$employe['Chef']['Employe']['nom'];

                switch ($employe['Fiche']['granddeplacement']) {
                    case 1: $d5 = 'Oui'; $totalgd++;break;
                    default: $d5 = 'Non'; break;
                }
                switch ($employe['Fiche']['repas']) {
                    case 1: $d6 = 'Oui'; $totalr++; break;
                    default: $d6 = 'Non'; break;
                }
                switch ($employe['Fiche']['trajet']) {
                    case 1: $d7 = 'Oui'; $totalt++; break;
                    default: $d7 = 'Non';; break;
                }
                /* -------------------------------------------------------------------------------------------------------------- */
                $d8 = date("H:i", strtotime($employe['Fiche']['matin_debut'])).' a '.date("H:i", strtotime($employe['Fiche']['matin_fin']));
                $d9 = date("H:i", strtotime($employe['Fiche']['aprem_debut'])).' a '.date("H:i", strtotime($employe['Fiche']['aprem_fin']));
                /* -------------------------------------------------------------------------------------------------------------- */
                $min1 = intval(date("i", strtotime($employe['Fiche']['matin_debut']))); // 0
                $min1 = round(($min1*100)/60);
                $total1 = $heur1.'.'.$min1;
                $min2 = intval(date("i", strtotime($employe['Fiche']['matin_fin']))); // 0
                $min2 = round(($min2*100)/60);
                $total2 = $heur2.'.'.$min2;
                $bloc1 = $total2-$total1;
            
                $min3 = intval(date("i", strtotime($employe['Fiche']['aprem_debut']))); // 0
                $min3 = round(($min3*100)/60);
                $total3 = $heur3.'.'.$min3;
                $min4 = intval(date("i", strtotime($employe['Fiche']['aprem_fin']))); // 0
                $min4 = round(($min4*100)/60);
                $total4 = $heur4.'.'.$min4;
                $bloc2 = $total4-$total3;

                $total = $bloc1+$bloc2;
                $totalh = $totalh+$total;
                $totaln++;
                $d10 = $total;
                /* -------------------------------------------------------------------------------------------------------------- */
            }
            $data[] = array($d1,$d2,$d3,$d4,$d5,$d6,$d7,$d8,$d9,$d10);
        }

        //debug($fiches);
        //debug($told);

        

        
        $data[] = array($totaln.' Jour(s)','','','',$totalgd,$totalr,$totalt,'','',$totalh);

  //       debug($data);
        // die();
         // -------------------------------------------------------------------
        $file_name = $nomf.'-du-'.$jmin.'-au-'.$jmax;
        $this->response->download($file_name);
        $_serialize = 'data';
        $this->viewClass = 'CsvView.Csv';
        $this->set(compact('data', '_serialize'));
    }
/*---------------------------------------------------------------------------------------------------------------------------------------------*/   






































/*---------------------------------------------------------------------------------------------------------------------------------------------*/
    public function exportjour($jmin=null,$jmax=null,$id=null) {


        $options['joins'] = array(
            array('table' => 'chantiers_has_chefs',
                'alias' => 'CHChef',
                'type' => 'LEFT',
                'conditions' => array(
                    'Fiche.chantiers_has_chefs_chantiers_id = CHChef.chantier_id',
                    'Fiche.chantiers_has_chefs_chefs_id = CHChef.chef_id',
                )
            )
        );

        $options['order'] = array('Fiche.jour'=> 'DESC','Employe.nom'=> 'ASC');
        // $options['conditions'] = array(
        //     'CHChef.chantier_id' => 1
        // );

        /* ----------------------------- */
        if(isset($_GET['jour1'])){
            if($_GET['jour1']['month']<10)
                $_GET['jour1']['month'] = '0'.$_GET['jour1']['month'];
            $jmin = $_GET['jour1']['year'].'-'.$_GET['jour1']['month'].'-'.$_GET['jour1']['day'];
        }

        if(isset($_GET['jour2'])){
            if($_GET['jour2']['month']<10)
                $_GET['jour2']['month'] = '0'.$_GET['jour2']['month'];
            $jmax = $_GET['jour2']['year'].'-'.$_GET['jour2']['month'].'-'.$_GET['jour2']['day'];
        }

        if(isset($_GET['id']))
        $id = $_GET['id'];

        if((isset($jmin))&&(isset($jmax))){
            $options['conditions'] = array('jour >='=>$jmin, 'jour <='=>$jmax);
        } else if(isset($jmin)){
            $options['conditions'] = array('jour >='=>$jmin);
        } else if(isset($jmax)){
            $options['conditions'] = array('jour <='=>$jmax);
        }
        /* ------------------------------------ */

        $fiches = $this->Fiche->find('all', $options);
        $cpt=0;
        foreach ($fiches as $fiche) {
            $fichesF[$cpt] = $fiche;
            $fichesF[$cpt]['Chantier'] = $this->Chantier->find('first',array(
                'fields'    => array('num','nom'),
                'recursive' => -1,
                'conditions'=> array('Chantier.id'=>$fiche['Fiche']['chantiers_has_chefs_chantiers_id'])
            ));
            $val = $this->Chef->find('first',array(
                'fields'    => array('id', 'employe_id'),
                'conditions'=> array('Chef.id'=>$fiche['Fiche']['chantiers_has_chefs_chefs_id'])
            ));
            $fichesF[$cpt]['Chef'] = $this->Employe->find('first',array(
                'fields'    => array('nom', 'prenom'),
                'recursive' => -1,
                'conditions'=> array('Employe.id'=>$val['Chef']['employe_id'])
            ));
            $cpt++;
        }

        $jour = array(' ','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche');
        $mois = array(1=>'Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Décembre');
        // A mettre ?
        if(isset($fiches[0])){
            $lf = $fiches[0]['Fiche']['jour'];
            $jdt = date("N", strtotime($lf));
            $ndt = date("d", strtotime($lf));
            $adt = date("n", strtotime($lf));
            $dt = $jour[$jdt].' '.$ndt.' '.$mois[$adt];
        }

        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $told = 1;
        $totalh =0;
        $totalgd = 0;
        $totalr = 0;
        $totalt = 0;
        $totaln = 0;
        $fiches = $fichesF;
        //$employes = $employesF;
        

        if(isset($fiches))
        foreach ($fiches as $employe) {
            if($told!=0){   
                if(!isset($nom)){
                    $nom = $employe['Fiche']['jour'];
                    $nomf = $employe['Fiche']['jour'];

                    $jdt = date("N", strtotime($employe['Fiche']['jour']));
                    $ndt = date("d", strtotime($employe['Fiche']['jour']));
                    $adt = date("n", strtotime($employe['Fiche']['jour']));
                    $dt = $jour[$jdt].' '.$ndt.' '.$mois[$adt];

                    $data[] = array($dt,'','','','','','','','','','');
                    $data[] = array('Periode','Employe','Chef','Chantier', 'GD','Repas','Dep','Horaires 1','Horaires 2','Total');
                }
                if($employe['Fiche']['jour']!=$nom){
                    $nom = $employe['Fiche']['jour'];
                    $told =0;
                }
               /* if($told==0){
                    $data[] = array($totaln,$totalgd,$totalr,$totalt,$totalh)
                    //echo $employe['Employe']['prenom'].' '.$employe['Employe']['nom'].;
                }*/


                $heur1 = intval(date("G", strtotime($employe['Fiche']['matin_debut'])));
                $heur2 = intval(date("G", strtotime($employe['Fiche']['matin_fin']))); 
                $heur3 = intval(date("G", strtotime($employe['Fiche']['aprem_debut'])));
                $heur4 = intval(date("G", strtotime($employe['Fiche']['aprem_fin']))); 
                $nuit  = false;

                if((22<=$heur1)||($heur1<=7)){$nuit=true;} else {
                    if((22<=$heur2)||($heur2<=7)){$nuit=true;} else {
                        if((22<=$heur3)||($heur3<=7)){$nuit=true;} else {
                            if((22<=$heur4)||($heur4<=7)){$nuit=true;}
                        }
                    }
                }
                if($nuit){
                    $d1 = 'Nuit'; 
                } else {$d1 = 'Jour'; }

                /*$jdt = date("N", strtotime($employe['Fiche']['jour']));
                $ndt = date("d", strtotime($employe['Fiche']['jour']));
                $adt = date("n", strtotime($employe['Fiche']['jour']));
                $d2 = $jour[$jdt].' '.$ndt.' '.$mois[$adt];*/

                $d2 = $employe['Employe']['prenom'].' '.$employe['Employe']['nom'];
                $d3 = $employe['Chef']['Employe']['prenom'].' '.$employe['Chef']['Employe']['nom'];
                $d4 = $employe['Chantier']['Chantier']['num'].' '.$employe['Chantier']['Chantier']['nom'];

                switch ($employe['Fiche']['granddeplacement']) {
                    case 1: $d5 = 'Oui'; $totalgd++;break;
                    default: $d5 = 'Non'; break;
                }
                switch ($employe['Fiche']['repas']) {
                    case 1: $d6 = 'Oui'; $totalr++; break;
                    default: $d6 = 'Non'; break;
                }
                switch ($employe['Fiche']['trajet']) {
                    case 1: $d7 = 'Oui'; $totalt++; break;
                    default: $d7 = 'Non';; break;
                }
                /* -------------------------------------------------------------------------------------------------------------- */
                $d8 = date("H:i", strtotime($employe['Fiche']['matin_debut'])).' a '.date("H:i", strtotime($employe['Fiche']['matin_fin']));
                $d9 = date("H:i", strtotime($employe['Fiche']['aprem_debut'])).' a '.date("H:i", strtotime($employe['Fiche']['aprem_fin']));
                /* -------------------------------------------------------------------------------------------------------------- */
                $min1 = intval(date("i", strtotime($employe['Fiche']['matin_debut']))); // 0
                $min1 = round(($min1*100)/60);
                $total1 = $heur1.'.'.$min1;
                $min2 = intval(date("i", strtotime($employe['Fiche']['matin_fin']))); // 0
                $min2 = round(($min2*100)/60);
                $total2 = $heur2.'.'.$min2;
                $bloc1 = $total2-$total1;
            
                $min3 = intval(date("i", strtotime($employe['Fiche']['aprem_debut']))); // 0
                $min3 = round(($min3*100)/60);
                $total3 = $heur3.'.'.$min3;
                $min4 = intval(date("i", strtotime($employe['Fiche']['aprem_fin']))); // 0
                $min4 = round(($min4*100)/60);
                $total4 = $heur4.'.'.$min4;
                $bloc2 = $total4-$total3;

                $total = $bloc1+$bloc2;
                $totalh = $totalh+$total;
                $totaln++;
                $d10 = $total;
                /* -------------------------------------------------------------------------------------------------------------- */
            }
            $data[] = array($d1,$d2,$d3,$d4,$d5,$d6,$d7,$d8,$d9,$d10);
        }

        //debug($fiches);
        //debug($told);

        $data[] = array($totaln.' Fiche(s)','','','',$totalgd,$totalr,$totalt,'','',$totalh);

         // -------------------------------------------------------------------
        $file_name = $nomf;
        $this->response->download($file_name);
        $_serialize = 'data';
        $this->viewClass = 'CsvView.Csv';
        $this->set(compact('data', '_serialize'));
    }
/*---------------------------------------------------------------------------------------------------------------------------------------------*/   




















































/*---------------------------------------------------------------------------------------------------------------------------------------------*/
    public function exportchef($jmin=null,$jmax=null,$id=null) {





        if(isset($_GET['jour1'])){
            if($_GET['jour1']['month']<10)
                $_GET['jour1']['month'] = '0'.$_GET['jour1']['month'];
            $jmin = $_GET['jour1']['year'].'-'.$_GET['jour1']['month'].'-'.$_GET['jour1']['day'];
        }

        if(isset($_GET['jour2'])){
            if($_GET['jour2']['month']<10)
                $_GET['jour2']['month'] = '0'.$_GET['jour2']['month'];
            $jmax = $_GET['jour2']['year'].'-'.$_GET['jour2']['month'].'-'.$_GET['jour2']['day'];
        }

        if(isset($_GET['id']))
            $id = $_GET['id'];

        if(($id!=null)&&(isset($jmin))){
            $options['conditions'] = array('chantiers_has_chefs_chefs_id'=>$id, 'jour >='=>$jmin, 'jour <='=>$jmax);
        } else if($id!=null){
            $options['conditions'] = array('chantiers_has_chefs_chefs_id'=>$id);
        } else if(isset($jmin)){
            $options['conditions'] = array('jour >='=>$jmin, 'jour <='=>$jmax);
        }

        $options['joins'] = array(
            array('table' => 'chantiers_has_chefs',
                'alias' => 'CHChef',
                'type' => 'LEFT',
                'conditions' => array(
                    'Fiche.chantiers_has_chefs_chantiers_id = CHChef.chantier_id',
                    'Fiche.chantiers_has_chefs_chefs_id = CHChef.chef_id',
                )
            )
        );
        $options['order'] = array('Fiche.chantiers_has_chefs_chefs_id'=> 'DESC','Fiche.jour'=> 'DESC','Employe.nom'=> 'ASC');
        $fiches = $this->Fiche->find('all', $options);
        $cpt=0;
        foreach ($fiches as $fiche) {
            $fichesF[$cpt] = $fiche;
            $fichesF[$cpt]['Chantier'] = $this->Chantier->find('first',array(
                'fields'    => array('num','nom'),
                'recursive' => -1,
                'conditions'=> array('Chantier.id'=>$fiche['Fiche']['chantiers_has_chefs_chantiers_id'])
            ));
            $val = $this->Chef->find('first',array(
                'fields'    => array('id', 'employe_id'),
                'conditions'=> array('Chef.id'=>$fiche['Fiche']['chantiers_has_chefs_chefs_id'])
            ));
            $fichesF[$cpt]['Chef'] = $this->Employe->find('first',array(
                'fields'    => array('nom', 'prenom'),
                'recursive' => -1,
                'conditions'=> array('Employe.id'=>$val['Chef']['employe_id'])
            ));
            $cpt++;
        }

        $chefs = $this->Chef->find('all');
        foreach ($chefs as $chef) {
            $emp = $this->Employe->find('first', array(
                'recursive'=>-1,
                'conditions'=> array('Employe.id'=>$chef['Chef']['employe_id'])
            ));
            $chefsF[$chef['Chef']['id']] = $emp['Employe']['prenom'].' '.$emp['Employe']['nom'];
        }

        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $told = 1;
        $totalh =0;
        $totalgd = 0;
        $totalr = 0;
        $totalt = 0;
        $totaln = 0;
        $fiches = $fichesF;
        $chefs = $chefsF;
        $jour = array(' ','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche');
        $mois = array(1=>'Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Décembre');

        if(isset($fiches))
        foreach ($fiches as $employe) {
            if($told!=0){   
                if(!isset($nom)){
                    $nom = $employe['Chef']['Employe']['prenom'].' '.$employe['Chef']['Employe']['nom'];
                    $nomf = $employe['Chef']['Employe']['prenom'].'-'.$employe['Chef']['Employe']['nom'];

                    $data[] = array($nom,'','','','','','','','','','');
                    $data[] = array('Periode','Date','Employe','Chantier', 'GD','Repas','Dep','Horaires 1','Horaires 2','Total');
                }
                if(($employe['Chef']['Employe']['prenom'].' '.$employe['Chef']['Employe']['nom'])!=$nom){
                    $nom =$employe['Chef']['Employe']['prenom'].$employe['Chef']['Employe']['nom'];
                    $told =0;
                }

                $heur1 = intval(date("G", strtotime($employe['Fiche']['matin_debut'])));
                $heur2 = intval(date("G", strtotime($employe['Fiche']['matin_fin']))); 
                $heur3 = intval(date("G", strtotime($employe['Fiche']['aprem_debut'])));
                $heur4 = intval(date("G", strtotime($employe['Fiche']['aprem_fin']))); 
                $nuit  = false;

                if((22<=$heur1)||($heur1<=7)){$nuit=true;} else {
                    if((22<=$heur2)||($heur2<=7)){$nuit=true;} else {
                        if((22<=$heur3)||($heur3<=7)){$nuit=true;} else {
                            if((22<=$heur4)||($heur4<=7)){$nuit=true;}
                        }
                    }
                }
                if($nuit){
                    $d1 = 'Nuit'; 
                } else {$d1 = 'Jour'; }

                $jdt = date("N", strtotime($employe['Fiche']['jour']));
                $ndt = date("d", strtotime($employe['Fiche']['jour']));
                $adt = date("n", strtotime($employe['Fiche']['jour']));
                $d2 = $jour[$jdt].' '.$ndt.' '.$mois[$adt];

                $d3 = $employe['Employe']['prenom'].' '.$employe['Employe']['nom'];
                $d4 = $employe['Chantier']['Chantier']['num'].' '.$employe['Chantier']['Chantier']['nom'];

                switch ($employe['Fiche']['granddeplacement']) {
                    case 1: $d5 = 'Oui'; $totalgd++;break;
                    default: $d5 = 'Non'; break;
                }
                switch ($employe['Fiche']['repas']) {
                    case 1: $d6 = 'Oui'; $totalr++; break;
                    default: $d6 = 'Non'; break;
                }
                switch ($employe['Fiche']['trajet']) {
                    case 1: $d7 = 'Oui'; $totalt++; break;
                    default: $d7 = 'Non';; break;
                }
                /* -------------------------------------------------------------------------------------------------------------- */
                $d8 = date("H:i", strtotime($employe['Fiche']['matin_debut'])).' a '.date("H:i", strtotime($employe['Fiche']['matin_fin']));
                $d9 = date("H:i", strtotime($employe['Fiche']['aprem_debut'])).' a '.date("H:i", strtotime($employe['Fiche']['aprem_fin']));
                /* -------------------------------------------------------------------------------------------------------------- */
                $min1 = intval(date("i", strtotime($employe['Fiche']['matin_debut']))); // 0
                $min1 = round(($min1*100)/60);
                $total1 = $heur1.'.'.$min1;
                $min2 = intval(date("i", strtotime($employe['Fiche']['matin_fin']))); // 0
                $min2 = round(($min2*100)/60);
                $total2 = $heur2.'.'.$min2;
                $bloc1 = $total2-$total1;
            
                $min3 = intval(date("i", strtotime($employe['Fiche']['aprem_debut']))); // 0
                $min3 = round(($min3*100)/60);
                $total3 = $heur3.'.'.$min3;
                $min4 = intval(date("i", strtotime($employe['Fiche']['aprem_fin']))); // 0
                $min4 = round(($min4*100)/60);
                $total4 = $heur4.'.'.$min4;
                $bloc2 = $total4-$total3;

                $total = $bloc1+$bloc2;
                $totalh = $totalh+$total;
                $totaln++;
                $d10 = $total;
                /* -------------------------------------------------------------------------------------------------------------- */
            }
            $data[] = array($d1,$d2,$d3,$d4,$d5,$d6,$d7,$d8,$d9,$d10);
        }

        //debug($fiches);
        //debug($told);



        
        $data[] = array($totaln.' Fiche(s)','','','',$totalgd,$totalr,$totalt,'','',$totalh);

        //debug($data);
        //die();
         // -------------------------------------------------------------------
        $file_name = $nomf.'-du-'.$jmin.'-au-'.$jmax.'';
        $this->response->download($file_name);
        $_serialize = 'data';
        $this->viewClass = 'CsvView.Csv';
        $this->set(compact('data', '_serialize'));
    }
/*---------------------------------------------------------------------------------------------------------------------------------------------*/   










































}