<?php
/*


Controller de Team Esprit qui sert d'exemple pour le projet

- Utilisation de pagination
- Ajout de champs dans bdd avec certains attributs
- Utilisation d'autre model grâce à containable (AppController)
- ...


*/













class TeamsController extends AppController {
	public $helpers = array('Html', 'Form','Js');
    //public $name = 'Teams';
    public $scaffold;
    public $uses = array('Team','User','CoteTeam','ActivityUser','ActivityTeam','TeamTournament',
        'Match','CommentTeam','UsersHasTeam','Game'
        ,'UsersHasTeam', 'StatTeam');
    public $components = array('Paginator','RequestHandler');

	public function index() {
		$this->set('teams', $this->Team->find('all'));
	}

    private function comment($id) {
        /*
        $this->Paginator->settings = $this->paginate;

        $comment = $this->Paginator->paginate(
            'CommentTeam',
            array('CommentTeam.team_id' => $id)
        );
        
        foreach ($comment as $coms) {
            //$coms['Profile'] = $this->Team->User->findById($coms['User']['id']);
            $coms['CommentTeam']['content'] = nl2br($coms['CommentTeam']['content']);
            $nboc = substr_count($coms['CommentTeam']['content'],'<br />');            
            if($nboc>6){
                for ($i=0; $i <= 6 ; $i++) { 
                    $posst = strripos($coms['CommentTeam']['content'],'<br />');
                    $coms['CommentTeam']['content'] = substr_replace($coms['CommentTeam']['content'],'', $posst, 6);
                }
            }
            $coms['Infos'] = $this->Team->User->find('first', array(
                'fields' => array('User.show_username', 'Profile.avatar'),
                'recursive' => 0,
                'conditions' => array('User.id'=>$coms['User']['id'])
            ));
            $tabcom[] = $coms;     
        }
        debug($tabcom);
        return $tabcom;
        */
    }

    public $paginate = array(
        'limit' => 20,
        'order' => array(
            'CommentTeam.created' => 'desc'
        ),  
    );
    






























    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid team'));
        }
        //$tabcom = $this->comment($id);
        $team = $this->Team->find('first', array(
            'recursive' => -1,
            'conditions'=> array('Team.id'=>$id)
        ));
        if (!$team) {
            throw new NotFoundException(__('Invalid team'));
        }
        $score = $this->Team->CoteTeam->find('all',array(
            'limit' => 20,
            'order' => array('CoteTeam.created DESC'),
            'fields' => array('CoteTeam.score'),
            'conditions' => array('team_id' => $id)
        ));
        
        $comment = $this->Team->CommentTeam->find('all',array(
            'limit' => 3,
            'fields' => array('CommentTeam.id', 'CommentTeam.content','CommentTeam.created', 'User.id'),
            'order' => array('CommentTeam.created DESC'),
            'conditions' => array('team_id' => $id)
        )); 
        $members = $this->UsersHasTeam->find('all', array(
            'conditions' => array('UsersHasTeam.team_id' => $id, 'UsersHasTeam.approved' => 1)
        ));
        $jeu = $this->Game->find('first', array(
            'fields'=> array('Game.id','Game.title','Game.acr'),
            'recursive'=>-1,
            'conditions' => array('Game.id' => $team['Team']['game_id'])
        ));
        if(isset($jeu)){
            $this->set('jeu', $jeu);
        }
        //debug($members);
        foreach ($members as $tem) {
            $tabtest[] = $tem['User'];
        }
        if(isset($tabtest)){
            $this->set('members', $members);
        }
        
        //debug($members['1']); 
        
        $this->Paginator->settings = $this->paginate;

        $comment = $this->Paginator->paginate(
            'CommentTeam',
            array('CommentTeam.team_id' => $id)
        );
        
        foreach ($comment as $coms) {
            //$coms['Profile'] = $this->Team->User->findById($coms['User']['id']);
            $coms['CommentTeam']['content'] = nl2br($coms['CommentTeam']['content']);
            $nboc = substr_count($coms['CommentTeam']['content'],'<br />');            
            if($nboc>6){
                for ($i=0; $i <= 6 ; $i++) { 
                    $posst = strripos($coms['CommentTeam']['content'],'<br />');
                    $coms['CommentTeam']['content'] = substr_replace($coms['CommentTeam']['content'],'', $posst, 6);
                }
            }
            $coms['Infos'] = $this->User->find('first', array(
                'fields' => array('User.show_username','User.role', 'Profile.avatar'),
                'recursive' => 0,
                'conditions' => array('User.id'=>$coms['User']['id'])
            ));
            $tabcom[] = $coms;     
        }
        if(isset($team)){$this->set('team', $team);}
        if(isset($score)){$this->set('score', $score);}
        if(isset($tabcom)){$this->set('comment', $tabcom);}
        $this->set('team_layout', true);
    }


//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    public function getAll($id){
        return $this->Team->find('all',
            array(
                'conditions' => array('tournament_id'=>$id)
            ));
    }

//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

	public function add() {

        $curr_user = $this->Session->read('Auth.User.id');
        $testse = $this->UsersHasTeam->find('all', array(
            //'recursive' => -1,
            'conditions' => array('UsersHasTeam.user_id' => $curr_user, 'UsersHasTeam.approved'=>1)
        ));
        $this->set('games', $this->Game->find('all', array(
            'conditions' => array('Game.draft <>' => 1, 'Game.online' => 1),
            
        ))); 
        $games = $Game->find('all', array(
            'conditions' => array('Game.draft <>' => 1, 'Game.online' => 1)
        )); 

        $tab = array();
        foreach ($testse as $test) {
            $tab[] = $test['UsersHasTeam']['team_game_id'];
        }
        foreach ($games as $game) {
            if (!in_array($game['Game']['id'], $tab)) {
                $styleavailable[] = $game;
            }
        }
        if(!isset($styleavailable)){
            $this->Session->setFlash(__('Je suis désolé, vous avez déja une équipe dans chaque style de tournois.'),'flash_err');
            return $this->redirect(array('controller'=>'Users', 'action' => 'profile', $curr_user));
        }

        $this->set('styleavailable', $styleavailable);
        /* -------------- */

        if ($this->request->is('post')) {

            if (!empty($this->request->data)) {

                $testval = 0+intval($this->request->data['Team']['game_id']);
                $valid = false;
                if(!isset($styleavailable)){
                    $this->Session->setFlash(__('Je suis désolé, vous avez déja une équipe dans chaque style de tournois.'),'flash_custom');
                    return $this->redirect(array('action' => 'profile', $curr_user));
                }
                foreach ($styleavailable as $style) {
                    $tryy = $testval==$style['Game']['id']? true: false; 
                    if($tryy){
                        $valid=true;
                    }
                }
                if(isset($valid) && $valid){

                    $this->request->data['Team']['user_id'] = $this->Session->read('Auth.User.id');
                    $this->request->data['StatTeam']['win'] = 0;
                    if(isset($this->request->data['Team']['image'])){
                        $this->request->data['Team']['image'] = '/img/team/default.png';
                    }
                    
                    $this->Team->create();
                    if ($this->Team->saveAssociated($this->request->data)){

                        ///////////////////////////////// RECUPERATION DES INFOS TEAM /////////////////////////////////////////

                        $u = $this->Team->find('first',array(
                            'fields'    => array('Team.id','Team.game_id'),
                            'recursive' => -1,
                            'order' => array('Team.created' => 'desc'),
                            'conditions' => array('title' => $this->request->data['Team']['title'])
                        ));

                        ///////////////////////////////// CREATION D'UNE COTE TEAM ///////////////////////////////////////////////

                        $this->request->data['CoteTeam']['score'] = 1200;
                        $this->request->data['CoteTeam']['team_id'] = $u['Team']['id'];
                        $this->CoteTeam->create();
                        $this->CoteTeam->save($this->request->data);

                        ///////////////////////////////// CREATION DES STATS DE LA TEAM ///////////////////////////////////////////////

                        /*$this->request->data['StatTeam']['team_id'] = $u['Team']['id'];
                        $this->request->data['StatTeam']['win'] = 0;
                        $this->request->data['StatTeam']['def'] = 0;
                        $this->request->data['StatTeam']['first'] = 0;
                        $this->request->data['StatTeam']['second'] = 0;
                        $this->request->data['StatTeam']['third'] = 0;
                        $this->request->data['StatTeam']['tournaments'] = 0;
                        $this->StatTeam->create();
                        $this->StatTeam->save($this->request->data);*/

                        ///////////////////////////////// CREATION DE L'ACTIVITE ///////////////////////////////////////////////
                        
                        $this->request->data['ActivityTeam']['team_id']= $u['Team']['id'];
                        $this->request->data['ActivityTeam']['content']= 'L\'équipe vient d\'être crée et est en attente d\'acceptation';
                        $this->request->data['ActivityTeam']['role']= 1;
                        $this->request->data['ActivityTeam']['style']=1;        
                        $this->ActivityTeam->create();
                        $this->ActivityTeam->save($this->request->data);

                        ///////////////////////////////// CREATION DE L'USER HAS TEAM ///////////////////////////////////////////
                        
                        $this->request->data['UsersHasTeam']['user_id']= $this->Session->read('Auth.User.id');
                        $this->request->data['UsersHasTeam']['team_id']= $u['Team']['id'];
                        $this->request->data['UsersHasTeam']['team_game_id']= $u['Team']['game_id'];
                        $this->request->data['UsersHasTeam']['approved']=1;        
                        $this->UsersHasTeam->create();
                        $this->UsersHasTeam->save($this->request->data);

                        /////////////////////////////////// INFORMATION A L'UTILISATEUR ///////////////////////////////////////////
                        
                        $this->Session->setFlash(__('Votre équipe à bien été crée. Votre équipe doit être accepté pour pouvoir profiter de toutes les possibilitées du site'),'flash_custom');
                        return $this->redirect(array('action' => 'index'));

                        ////////////////////////////////////////////////////////////////////////////////////////////////////////
                    } else {
                    $this->Session->setFlash(__('Erreur majeur. Veuillez contacter un administrateur'),'flash_err');
                    }     
                } else {
                    $this->Session->setFlash(__('Vous ne pouvez pas choisir ce style d\'équipe, si le problème persiste veuillez contacter un administrateur.'),'flash_err');
                }            
            } else {
                $this->Session->setFlash(__('Toutes les données demandées non pas étaient complétées'),'flash_err');
            }
        }  

        /////////////////////////////////// INFORMATION A LA VUE ///////////////////////////////////////////

        $this->set(array(
            'title_for_layout' => 'Création d\'équipe'
         ));

    }
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\






}