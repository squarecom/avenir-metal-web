<?php
class AdminsController extends AppController {
    public $helpers = array('Html', 'Form'); // Nom des helpers a utiliser, sous forme de tableau

    public $name = 'admin'; // Nom de la table MySQL a utiliser

    public $scaffold; // C'est "l'échafaudage" de CakePHP, il permet de plus facilement gérer les associations entre les modèles CakePHP
    // Voir http://book.cakephp.org/2.0/fr/controllers/scaffolding.html

    public $uses = array('Admin'); // Les modèles qu'on utlise, pas vraiment utilise si on en utilise qu'un (comme ici), mais je préfère le mettre..


    /*********************************************************** Fonction index des utilisateurs (tableau) ********************************************************/

    public function index() {
            //$this->set('Admins', $this->Admin->find('all'));
        // Bonjour
    }
    public function chantier() {
            //$this->set('Admins', $this->Admin->find('all'));
    }
    public function all() {
            //$this->set('Admins', $this->Admin->find('all'));
    }
    public function employees() {
            //$this->set('Admins', $this->Admin->find('all'));
    }
    public function fiches() {
            //$this->set('Admins', $this->Admin->find('all'));
    }
    public function fichesemp() {
            //$this->set('Admins', $this->Admin->find('all'));
    }
    public function fichescha() {
            //$this->set('Admins', $this->Admin->find('all'));
    }
    public function fichesche() {
            //$this->set('Admins', $this->Admin->find('all'));
    }
   

   /*********************************************************** Fonction de ajout d'un utilisateur ********************************************************/

    public function add() {

        if ($this->request->is('post')) {

            if (!empty($this->request->data)) {
                if ($this->Admin->save($this->request->data)) {
                    $this->Session->setFlash(__('Le compte Admin a bien été crée'),'flash_custom');
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('L\'utilisateur n\'a pas pu être sauvegarder. Veuillez réessayer SVP.'),'flash_err');
                }               
            } else {
                $this->Session->setFlash(__('Toutes les données demandées non pas étaient complétées'),'flash_err');
            }
        }

        ////////////////////////////////////// DONNES POUR LA VUE //////////////////////////////////////
        $this->set(array(
            'title_for_layout' => 'Ajouter un Administrateur',
         ));
    }


    /*********************************************************** Fonction de edition d'un utilisateur ********************************************************/

    public function edit($id) {

        //////////////////////////   Test si on a l'ID de l'utilisateur a editer ////////////////////////

        if (!isset($id)){
            return $this->redirect(array('action' => 'index'));
        }

        //////////////////////////   Test l'existance de l'utilisateur ////////////////////////

        $this->Admin->id = $id;
        if (!$this->Admin->exists()) {
            $this->Session->setFlash(__('L\'utilisateur demandé n\'existe pas ou plus, désolé.'),'flash_err');
            return $this->redirect(array('action' => 'index'));
        }

        //////////////////////////   ACTION DE SAUVEGARDE //////////////////////// 

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Admin->save($this->request->data)) {
                $this->Session->setFlash(__('Les changements ont été effectués'),'flash_custom');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('L\'utilisateur n\'a pas pu être sauvegarder. Veuillez réessayer SVP.'),'flash_err');
            } 
        } else {
            $this->request->data = $this->Admin->read(null, $id);
        }

        ////////////////////////////////////// DONNES POUR LA VUE //////////////////////////////////////
        $this->set(array(
            'title_for_layout' => 'Edition'
        ));
    }


    /*********************************************************** Fonction de suppression d'un utilisateur ********************************************************/

    public function delete($id = null) {
            $this->request->onlyAllow('post');

            $this->Admin->id = $id;
            if (!$this->Admin->exists()) {
                throw new NotFoundException(__('Invalid user'));
            }
            if ($this->Admin->delete()) {
                $this->Session->setFlash(__('L\'utilisateur a été supprimé'),'flash_custom');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('L\'utilisateur n\'a pas été supprimé'),'flash_err');
            return $this->redirect(array('action' => 'index'));
    }
        

    /*********************************************************** Pre-Fonction de fonction d'un utilisateur ********************************************************/

    // Explications : BeforeFilter est une fonction qui est executé avant toutes les fonctions dans le controller
    // Il y en a deux différents sur CakePHP, beforeFilter (Avant la fonction) et beforeRender (Avant d'afficher la vue, après avoir exécuté la fonction)

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login', 'logout','add','view','chantier','all','employees','fiches', 'fichesemp','fichescha','fichesche', 'fichesadd'); 
    }


    /*********************************************************** Fonction de connexion d'un utilisateur ********************************************************/ 

    public function login() {
        if ($this->request->is('post')) {
            /*
            if ($this->Auth->login()) {
                $this->Session->setFlash(__('Authentification réussit'),'flash_custom');
                return $this->redirect($this->Auth->redirect('./index'));
            }
            */
            $this->Auth->login();
            $this->Session->setFlash(__('Authentification réussit'),'flash_custom');
            return $this->redirect($this->Auth->redirect('./index'));
            //$this->Session->setFlash(__('Pseudonyme ou mot de passe invalide, veuillez réessayer.'), 'flash_err');
        }
         $this->set(array(
            'title_for_layout' => 'Connexion'
        ));
    }
    

    /*********************************************************** Fonction de deconnexion d'un utilisateur ********************************************************/ 

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }


}