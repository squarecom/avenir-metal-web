<?php
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
class UsersController extends AppController {
    public $helpers = array('Html', 'Form'); 
    public $name = 'Users'; 
    public $components = array('Paginator', 'RequestHandler');
    public $scaffold; 
    public $uses = array('User','Employe','Chef','Chantier','ChantierHasChef','Agence','Fiche','Contact');

    /*********************************************************** Fonction index des utilisateurs (tableau) ********************************************************/

    public function index() {
        $this->set('users', $this->User->find('all'));
    }
    public function chantier() {
            //$this->set('Users', $this->User->find('all'));
    }
    public function all() {
            //$this->set('Users', $this->User->find('all'));
    }
    public function employees() {
            //$this->set('Users', $this->User->find('all'));
    }
    public function fiches() {
            //$this->set('Users', $this->User->find('all'));
    }
    public function fichesemp() {
            //$this->set('Users', $this->User->find('all'));
    }
    public function fichescha() {
            //$this->set('Users', $this->User->find('all'));
    }
    public function fichesche() {
            //$this->set('Users', $this->User->find('all'));
    }
   
    public function results() {
        $value = $this->request->query['q'];
        echo $this->Agence->find('first', array(
            'conditions' => array('nom LIKE' => '%'.$value.'%')
        ));
    }
   /*********************************************************** Fonction de ajout d'un utilisateur ********************************************************/

    public function add() {

        if ($this->request->is('post')) {

            if (!empty($this->request->data)) {
                if ($this->User->save($this->request->data)) {
                    $this->Session->setFlash(__('Le compte User a bien été crée'),'flash_custom');
                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('L\'utilisateur n\'a pas pu être sauvegarder. Veuillez réessayer SVP.'),'flash_err');
                }               
            } else {
                $this->Session->setFlash(__('Toutes les données demandées non pas étaient complétées'),'flash_err');
            }
        }

        ////////////////////////////////////// DONNES POUR LA VUE //////////////////////////////////////
        $this->set(array(
            'title_for_layout' => 'Ajouter un Useristrateur',
         ));
    }


    /*********************************************************** Fonction de edition d'un utilisateur ********************************************************/

    public function edit($id) {

        //////////////////////////   Test si on a l'ID de l'utilisateur a editer ////////////////////////

        if (!isset($id)){
            return $this->redirect(array('action' => 'index'));
        }

        //////////////////////////   Test l'existance de l'utilisateur ////////////////////////

        $this->User->id = $id;
        if (!$this->User->exists()) {
            $this->Session->setFlash(__('L\'utilisateur demandé n\'existe pas ou plus, désolé.'),'flash_err');
            return $this->redirect(array('action' => 'index'));
        }

        //////////////////////////   ACTION DE SAUVEGARDE //////////////////////// 

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('Les changements ont été effectués'),'flash_custom');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('L\'utilisateur n\'a pas pu être sauvegarder. Veuillez réessayer SVP.'),'flash_err');
            } 
        } else {
            $this->request->data = $this->User->read(null, $id);
        }

        ////////////////////////////////////// DONNES POUR LA VUE //////////////////////////////////////
        $this->set(array(
            'title_for_layout' => 'Edition'
        ));
    }


    /*********************************************************** Fonction de suppression d'un utilisateur ********************************************************/

    public function delete($id = null) {
            $this->request->onlyAllow('post');

            $this->User->id = $id;
            if (!$this->User->exists()) {
                throw new NotFoundException(__('Invalid user'));
            }
            if ($this->User->delete()) {
                $this->Session->setFlash(__('L\'utilisateur a été supprimé'),'flash_custom');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('L\'utilisateur n\'a pas été supprimé'),'flash_err');
            return $this->redirect(array('action' => 'index'));
    }
        

    /*********************************************************** Pre-Fonction de fonction d'un utilisateur ********************************************************/

    // Explications : BeforeFilter est une fonction qui est executé avant toutes les fonctions dans le controller
    // Il y en a deux différents sur CakePHP, beforeFilter (Avant la fonction) et beforeRender (Avant d'afficher la vue, après avoir exécuté la fonction)

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login', 'logout','add','view','chantier','all','employees','fiches', 'fichesemp','fichescha','fichesche', 'fichesadd'); 
    }


    /*********************************************************** Fonction de connexion d'un utilisateur ********************************************************/ 
    public function login() {
        $app = $this->Session->check('Auth.User.username');
        if($app){
            return $this->redirect($this->Auth->redirect('./home'));
        }
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->Session->setFlash(__('Authentification réussit'),'flash_custom');
                return $this->redirect($this->Auth->redirect('./index'));
            }
            $this->Session->setFlash(__('Pseudonyme ou mot de passe invalide, veuillez réessayer.'), 'flash_err');
        }
        $this->set(array(
            'title_for_layout' => 'Connexion'
        ));
    } 

    public function home() {
        $app = $this->Session->check('Auth.User.username');
        if(!$app){
            return $this->redirect($this->Auth->redirect('./index'));
        }
        ////////////////////// FIND /////////////////////////
        $employes = $this->Employe->find('count');
        $chefs = $this->Chef->find('count');
        $chantiers = $this->Chantier->find('count');
        $contacts = $this->Contact->find('count');
        $users = $this->User->find('count');
        $agences = $this->Agence->find('count');
        $fiches = $this->Fiche->find('count', array(
            'conditions' => array('jour'=> date('Y-m-j'))
        ));

        ////////////////////// SET /////////////////////////
        $this->set(array('employes' => $employes));
        $this->set(array('chefs' => $chefs));
        $this->set(array('chantiers' => $chantiers));
        $this->set(array('contacts' => $contacts));
        $this->set(array('users' => $users));
        $this->set(array('agences' => $agences));
        $this->set(array('fiches' => $fiches));

    }   

    public function search() {
        $app = $this->Session->check('Auth.User.username');
        if(!$app){
            return $this->redirect($this->Auth->redirect('./index'));
        }
        $value = $this->request->query['valeur'];
        ////////////////////// FIND /////////////////////////
        $employes = $this->Employe->find('all', array(
            'conditions'    => array(
                "OR" => array(
                    "Employe.nom LIKE" => "%".$value."%",
                    "Employe.prenom LIKE" => "%".$value."%"
                )
            )
        ));
        $agences = $this->Agence->find('all', array(
            'conditions'    => array(
                "Agence.nom LIKE" => "%".$value."%",
            )
        ));
        // Employ + fiche
        $chantiers = $this->Chantier->find('all', array(
            'conditions'    => array(
                "OR" => array(
                    "Chantier.num LIKE" => "%".$value."%",
                    "Chantier.nom LIKE" => "%".$value."%"
                )
            )
        ));
        // 
        $users = $this->User->find('all', array(
            'conditions'    => array(
                "User.username LIKE" => "%".$value."%",
            )
        ));
        $contacts = $this->Contact->find('all', array(
            'conditions'    => array(
                "OR" => array(
                    "Contact.nom LIKE" => "%".$value."%",
                    "Contact.prenom LIKE" => "%".$value."%",
                )
            )
        ));
        // debug($employes);
        // debug($agences);
        // debug($chantiers);
        // debug($users);
        // debug($contacts);
        // die();

        ////////////////////// SET /////////////////////////
        $this->set(array('employes' => $employes));
        $this->set(array('agences' => $agences));
        // $this->set(array('chefs' => $chefs));
        $this->set(array('chantiers' => $chantiers));
        // $this->set(array('chc' => $chc));
        $this->set(array('users' => $users));
        $this->set(array('contacts' => $contacts));
    }

    /*********************************************************** Fonction de deconnexion d'un utilisateur ********************************************************/ 

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }


}