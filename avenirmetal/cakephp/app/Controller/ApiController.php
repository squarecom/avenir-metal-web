<?php
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
class ApiController extends AppController {
    public $helpers = array('Html', 'Form'); 
    //public $name = 'admin'; 
    public $scaffold; 
    public $uses = array('User','Chef','Chantier','Fiche','Employe','ChantiersHasChef','Contact');


    /////////////////////////////////////// Court-circuit les vues et layouts et gère les autorisations //////////////////////////////////////

    public function beforeFilter() {
        $this->autoRender = false;
        $this->Auth->allow();
       // $this->Auth->allow('getLogin');

    }
    public function index(){
      $this->layout = 'default';
      $this->autoRender = true;

    }
    //////////////////////////////////////////////////////// Test si le login et le password sont bon //////////////////////////////////////////////////

    public function getLogin($login = null, $pwd = null) {
        if (!$login) { throw new NotFoundException(__('No login')); }
        if (!$pwd) { throw new NotFoundException(__('No password')); }

        $passwordHasher = new SimplePasswordHasher();
        $pwd = $passwordHasher->hash($pwd);

        $data = $this->User->find('first', array(
            'recursive' => -1,
            'conditions' => array('User.username'=>$login, 'User.password'=>$pwd)
        ));

        if($data){
            echo json_encode($data);
        } else {
            echo '';
        }
    }

    //////////////////////////////////////////////////////// Test si le login et le password sont bon //////////////////////////////////////////////////

    public function getHash($pwd = null) {
        if (!$pwd) { throw new NotFoundException(__('No password')); }

        $passwordHasher = new SimplePasswordHasher();
        $pwd = $passwordHasher->hash($pwd);
        echo json_encode($pwd);
    }

    //////////////////////////////////////////////////////// Test si le login et le password sont bon //////////////////////////////////////////////////

    public function getChef($login = null, $pwd = null) {
        if (!$login) { throw new NotFoundException(__('No login')); }
        if (!$pwd) { throw new NotFoundException(__('No password')); }

        $passwordHasher = new SimplePasswordHasher();
        $pwd = $passwordHasher->hash($pwd);

        $data = $this->Chef->find('first', array(
            'conditions' => array('Chef.username'=>$login, 'Chef.password'=>$pwd)
        ));

        if($data){
            echo json_encode($data);
        } else {
            echo '';
        }
    }

  //////////////////////////////////////////////////////// Recupère la liste de tout les chantier //////////////////////////////////////////////////

  public function getChantier($num){

    $data = $this->Chantier->find('first', array(
      'recursive' => -1,
      'conditions' => array('Chantier.id'=>$num)
    ));

    if($data){
      echo json_encode($data);
    } else {
      echo '';
    } 
  }

  //////////////////////////////////////////////////////// Recupère la liste de tout les chantiers assossier à un chef //////////////////////////////////////////////////

   public function getChantiersByChef($chef){

    $data = $this->ChantiersHasChef->find('all', array(
      'conditions'=> array('ChantiersHasChef.chef_id' => $chef, 'NOT' => array('Chantier.clos' => 1)),
      'fields'  => array('Chantier.*'),
      'order' => array('Chantier.nom ASC')
    ));     

    if($data){
      echo json_encode($data);
    } else {
      echo '';
    } 
  }

  //////////////////////////////////////////////////////// Recupère la liste de tout les chantiers assossier à un chef //////////////////////////////////////////////////

    public function getLastFichesByChantier($num){
      $fiches = $this->Fiche->find('all', array(
            'conditions'  => array(
                'Fiche.chantiers_has_chefs_chantiers_id'  => $num,
            ),
            'fields' => array('Fiche.jour', 'Fiche.chantiers_has_chefs_chantiers_id','Fiche.chantiers_has_chefs_chefs_id'),
            'order' => array('Fiche.jour DESC'),
            'group' => array('Fiche.jour'),
            'limit' => 5
        ));
        
        echo json_encode($fiches);

       
    }

    //////////////////////////////////////////////////////// Recupère la liste de tout les chantiers assossier à un chef //////////////////////////////////////////////////

    public function getLastFichesByChantierAndChef($numChantier,$numChef){


        $conditions = array("Fiche.jour >" => date('Y-m-d', strtotime("-50 days"))); 


        $fiches = $this->Fiche->find('all', array(
            'conditions'  => array(
                'Fiche.chantiers_has_chefs_chantiers_id'  => $numChantier,
                'Fiche.chantiers_has_chefs_chefs_id'  => $numChef,
            ),
            'fields' => array('Fiche.jour', 'Fiche.chantiers_has_chefs_chantiers_id','Fiche.chantiers_has_chefs_chefs_id'),
            'order' => array('Fiche.jour DESC'),
            'group' => array('Fiche.jour'),
            'limit' => 5
        ));
        
        $output = array();

        foreach ($fiches as $fiche) {
            $fiche = $fiche['Fiche']; 

            $ficheDetails = $this->getFicheDetails($fiche['chantiers_has_chefs_chantiers_id'],$fiche['chantiers_has_chefs_chefs_id'],$fiche['jour']);

            $fiche['Details']=$ficheDetails;

            $output[]['Fiche']=$fiche;

        }

        echo json_encode($output);

       
    }

     //////////////////////////////////////////////////////// Renvois le details des fiches à un chantier, un chef et un jour donnés //////////////////////////////////////////////////

    public function getFicheDetails($chantier_id,$chef_id,$jour){
      $fiche = $this->Fiche->find('all', array(
            'conditions'  => array(
                'Fiche.chantiers_has_chefs_chantiers_id'  => $chantier_id,
                'Fiche.chantiers_has_chefs_chefs_id'=>$chef_id,
                'Fiche.jour'=>$jour,
            ),
          
            'fields'=>array('Fiche.*','Employe.*'),
            'order' => array('Fiche.employe_id ASC'),
        ));
       
        return $fiche;

       
    }

    //////////////////////////////////////////////////////// Recupère la liste de tout les employés //////////////////////////////////////////////////

    public function getEmployeById($id){

        $data = $this->Employe->find('first', array(
            'recursive' => -1,
            'conditions' => array('Employe.id' => $id)
        ));
        if($data){
            echo json_encode($data);
        } else {
            echo '';
        } 
    }

    //////////////////////////////////////////////////////// Recupère la liste de tout les employés non interimaire //////////////////////////////////////////////////

    public function getEmployees(){

        $data = $this->Employe->find('all', array(
            'recursive' => -1,
            'conditions' => array('Employe.interim'=>0),
            'order' => array(
                    'Employe.nom ASC',
                    'Employe.prenom ASC',
                )
        ));
        if($data){
            echo json_encode($data);
        } else {
            echo '';
        } 
    }
      //////////////////////////////////////////////////////// Recupère la liste de tout les intérimaires //////////////////////////////////////////////////

    public function getInterims(){

        $data = $this->Employe->find('all', array(
            'recursive' => -1,
            'conditions' => array('Employe.interim'=>1),
            'joins' => array(
                array('table' => 'agences',
                    'alias' => 'Agence',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Agence.id= Employe.agence_id',
                    )
                ),
            ),
            'fields'=>array('Agence.*','Employe.*'),
            'order' => array(
                    'Employe.nom ASC',
                    'Employe.prenom ASC',
                )
        ));
        if($data){
            echo json_encode($data);
        } else {
            echo '';
        } 
    }

    //////////////////////////////////////////////////////// Recupère la liste de tous les contacts de l'administration //////////////////////////////////////////////////

    public function getContactAdmin(){

        $data = $this->Contact->find('all', array(
            'order' => array(
                    'nom ASC',
                    'prenom ASC',
                )
        ));
        if($data){
            echo json_encode($data);
        } else {
            echo '';
        } 
    }

    //////////////////////////////////////////////////////// Recupère la liste de toute les fiches //////////////////////////////////////////////////

    public function getFiches(){

        $data = $this->Fiche->find('all', array(
            'recursive' => -1,
            'order' => array('Fiche.periode_debut DESC')
        ));
        if($data){
            echo json_encode('$data');
        } else {
            echo '';
        } 
    }
///////// AJOUTER UNE FICHE ///
    public function setFiche(){

        header("Content-type: application/json");
        if($_POST!=null){

            foreach ($_POST as $fiche) {
                
               $success=$this->Fiche->save($fiche);

            }
            if($success){
                $output['success']="true";
            }else{
                $output['success']="false";
            }
            
        }else{
            $output="";
        }
        
        echo json_encode($output);
    }
}