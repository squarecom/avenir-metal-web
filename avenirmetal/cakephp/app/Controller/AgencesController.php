<?php
class AgencesController extends AppController {
    public $helpers = array('Html', 'Form'); 
    public $components = array('Paginator', 'RequestHandler');
    public $scaffold; 
    public $uses = array('Agence'); //'Agence'



	public function index() {
		$this->set('agences', $this->Agence->find('all'));
	}

	public function add() {
		if ($this->request->is('post')) {
			if (!empty($this->request->data)) {                    
                $this->Agence->create();
				if ($this->Agence->save($this->request->data)){
					$this->Session->setFlash(__('L\'Agence à bien été ajouté.'),'flash_custom');
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Erreur majeur. Veuillez recommencer'),'flash_err');
				}     
			} else {
				$this->Session->setFlash(__('Toutes les données demandées non pas étaient complétées'),'flash_err');
			}            
		}
        //$agences = $this->Agences->find('all');
	}
	public function edit($id) {

        if (!isset($id)){ return $this->redirect(array('action' => 'index')); }

        $this->Agence->id = $id;
        if (!$this->Agence->exists()) {
            $this->Session->setFlash(__('L\'utilisateur demandé n\'existe pas ou plus, désolé.'),'flash_err');
            return $this->redirect(array('action' => 'index'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Agence->save($this->request->data)) {
                $this->Session->setFlash(__('Les changements ont été effectués'),'flash_custom');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('L\'utilisateur n\'a pas pu être sauvegarder. Veuillez réessayer SVP.'),'flash_err');
            } 
        } else {
            $this->request->data = $this->Agence->read(null, $id);
        }
        $this->set(array(
            'title_for_layout' => 'Edition'
        ));
    }  

}