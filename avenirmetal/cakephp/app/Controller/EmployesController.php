<?php
class EmployesController extends AppController {
    public $helpers = array('Html', 'Form'); 
    public $components = array('Paginator', 'RequestHandler');
    public $scaffold; 
    public $uses = array('Employe','Agence');

	public function index() {
		$employes =  $this->Employe->find('all');
        $this->set('employes', $employes);
	}

	public function add() {
		if ($this->request->is('post')) {
			if (!empty($this->request->data)) {                    
				$this->Employe->create();
				if ($this->Employe->save($this->request->data)){
					$this->Session->setFlash(__('L\'employe à bien été ajouté.'),'flash_custom');
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Erreur majeur. Veuillez recommencer'),'flash_err');
				}     
			} else {
				$this->Session->setFlash(__('Toutes les données demandées non pas étaient complétées'),'flash_err');
			}            
		}
        $agences = $this->Agence->find('all');
        foreach ($agences as $agence) {
            $tabs[$agence['Agence']['id']] = $agence['Agence']['nom'];
        }
        if(isset($tabs)){
            $this->set(array(
            'agences'          => $tabs,
            ));
        }

        $this->set(array(
            'title_for_layout' => 'Ajout'
        ));
	}
	public function edit($id) {

        if (!isset($id)){ return $this->redirect(array('action' => 'index')); }

        $this->Employe->id = $id;
        if (!$this->Employe->exists()) {
            $this->Session->setFlash(__('L\'utilisateur demandé n\'existe pas ou plus, désolé.'),'flash_err');
            return $this->redirect(array('action' => 'index'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Employe->save($this->request->data)) {
                $this->Session->setFlash(__('Les changements ont été effectués'),'flash_custom');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('L\'utilisateur n\'a pas pu être sauvegarder. Veuillez réessayer SVP.'),'flash_err');
            } 
        } else {
            $this->request->data = $this->Employe->read(null, $id);
        }
        $agences = $this->Agence->find('all');
        foreach ($agences as $agence) {
            $tabs[$agence['Agence']['id']] = $agence['Agence']['nom'];
        }
        $this->set(array(
            'agences'           => $tabs,
            'title_for_layout' => 'Edition'
        ));
    }  

}