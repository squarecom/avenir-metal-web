<?php
class ChantiersController extends AppController {
    public $helpers = array('Html', 'Form'); 
    public $components = array('Paginator', 'RequestHandler');
    public $scaffold; 
    public $uses = array('Chantier','ChantiersHasChef','Employe','Chef','Fiche');

	public function index() {
		$chantiers =  $this->Chantier->find('all',array(

            'order' => array('Chantier.created DESC'),

            ));
        $this->set('chantiers', $chantiers);

        $ChantiersHasChefF   = $this->ChantiersHasChef->find('all', array(
           'recursive' => 2,
          'order' => array('Chantier.created DESC')
        ));     

        $Chefs =  $this->Chef->find('all',array(

            'order' => array('Employe.nom ASC','Employe.prenom ASC'),

            ));
        $this->set('ChantiersHasChefF', $ChantiersHasChefF);
        $this->set('Chefs', $Chefs);
	}

	public function add() {
		if ($this->request->is('post')) {
			if (!empty($this->request->data)) {                    
				$this->Chantier->create();
				if ($this->Chantier->save($this->request->data)){
					$this->Session->setFlash(__('Le Chantier à bien été ajouté.'),'flash_custom');
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Erreur majeur. Veuillez recommencer'),'flash_err');
				}     
			} else {
				$this->Session->setFlash(__('Toutes les données demandées non pas étaient complétées'),'flash_err');
			}            
		}
        $this->set(array(
            'title_for_layout' => 'Ajout'
        ));
	}


	public function edit($id) {

        if (!isset($id)){ return $this->redirect(array('action' => 'index')); }

        $this->Chantier->id = $id;
        if (!$this->Chantier->exists()) {
            $this->Session->setFlash(__('L\'utilisateur demandé n\'existe pas ou plus, désolé.'),'flash_err');
            return $this->redirect(array('action' => 'index'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Chantier->save($this->request->data)) {
                $this->Session->setFlash(__('Les changements ont été effectués'),'flash_custom');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('L\'utilisateur n\'a pas pu être sauvegarder. Veuillez réessayer SVP.'),'flash_err');
            } 
        } else {
            $this->request->data = $this->Chantier->read(null, $id);
        }
        $this->set(array(
            'title_for_layout' => 'Edition'
        ));
    }

    public function delete($id) {

        $this->Fiche->deleteAll(array('Fiche.chantiers_has_chefs_chantiers_id' => $id), false);
        $this->ChantiersHasChef->deleteAll(array('ChantiersHasChef.chantier_id' => $id), false);

        if ($this->Chantier->delete($id)) {
            $this->Session->setFlash(__('Le chantier a bien été supprimé.'),'flash_custom');
            return $this->redirect(array('action' => 'index'));
        }
    }  

}