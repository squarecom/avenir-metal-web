<?php
class ChefsController extends AppController {
    public $helpers = array('Html', 'Form'); 
    public $components = array('Paginator', 'RequestHandler');
    public $scaffold; 
    public $uses = array('Chef','Employe','Fiche','ChantiersHasChef');


	public function add() {
		if ($this->request->is('post')) {
			if (!empty($this->request->data)) {                    
				$this->Chef->create();
				if ($this->Chef->save($this->request->data)){
					$this->Session->setFlash(__('L\'employe à bien été ajouté.'),'flash_custom');
					return $this->redirect(array('controller'=>'chantiers', 'action' => 'index'));
				} else {
					$this->Session->setFlash(__('Erreur majeur. Veuillez recommencer'),'flash_err');
				}     
			} else {
				$this->Session->setFlash(__('Toutes les données demandées non pas étaient complétées'),'flash_err');
			}            
		}

		$employes = $this->Employe->find('all', array(
			'order'=> 'Employe.prenom ASC'
		));
		foreach ($employes as $employe) {
			$val = $employe['Employe']['prenom'].' '.$employe['Employe']['nom'];
			$test = $this->Chef->find('first', array(
				'conditions'	=> array('employe_id'=>$employe['Employe']['id'])
			));
			if($test==array()){
				$employesF[$employe['Employe']['id']] = $val;
			}
		}

        $this->set(array(
        	'employes'	=> $employesF,
            'title_for_layout' => 'Ajout'
        ));
	}

	public function delete($id) {

        $this->Fiche->deleteAll(array('Fiche.chantiers_has_chefs_chefs_id' => $id), false);
        $this->ChantiersHasChef->deleteAll(array('ChantiersHasChef.chef_id' => $id), false);

        if ($this->Chef->delete($id)) {
            $this->Session->setFlash(__('Le chantier a bien été supprimé.'),'flash_custom');
            return $this->redirect(array('controller'=>'Chantiers', 'action' => 'index'));
        }
    }


}