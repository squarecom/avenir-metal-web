# Avenir Metal WEB

## CakePHP
Documentation Controllers : http://book.cakephp.org/2.0/en/controllers.html
Documentation Views : http://book.cakephp.org/2.0/en/views.html
Documentation Models : http://book.cakephp.org/2.0/en/models.html
Get started : http://book.cakephp.org/2.0/en/getting-started.html

## Twitter Bootstrap 3.1.1
Dcumentation CSS : http://getbootstrap.com/css/
Document Components : http://getbootstrap.com/components/
Documentation JS : http://getbootstrap.com/javascript/
Get started : http://getbootstrap.com/getting-started/

## Apache Alias WAMP :
Nom : "avenirmetal"
Adresse : "[REPERTOIRE]\avenir-metal-web\avenirmetal\cakephp"
Puis lancez "localhost/avenirmetal/" dans notre navigateur


## TODO
  - Export CSV


## A TESTER

  - Delete en Cascade (Ex: SUpprimer chantier avec des fiches et des assignations de chef, etc.)
  - Vérifier l'ortographe (des bouttons, des formulaires, etc.)


## Script

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `am_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `am_db` ;

-- -----------------------------------------------------
-- Table `am_db`.`agences`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `am_db`.`agences` ;

CREATE TABLE IF NOT EXISTS `am_db`.`agences` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  `tel` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `adresse` VARCHAR(45) NULL,
  `ville` VARCHAR(45) NULL,
  `commentaire` LONGTEXT NULL,
  `created` DATETIME NULL,
  `modified` DATETIME NULL,
  `tel2` VARCHAR(45) NULL,
  `tel3` VARCHAR(45) NULL,
  `email2` VARCHAR(45) NULL,
  `email3` VARCHAR(45) NULL,
  `fax` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `am_db`.`employes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `am_db`.`employes` ;

CREATE TABLE IF NOT EXISTS `am_db`.`employes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  `prenom` VARCHAR(45) NOT NULL,
  `tel` VARCHAR(45) NULL,
  `interim` TINYINT(1) NOT NULL,
  `agence_id` INT NULL,
  `adresse` LONGTEXT NULL,
  `ville` VARCHAR(45) NULL,
  `cp` VARCHAR(45) NULL,
  `commentaire` LONGTEXT NULL,
  `statut` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_employes_agences1_idx` (`agence_id` ASC),
  CONSTRAINT `fk_employes_agences1`
    FOREIGN KEY (`agence_id`)
    REFERENCES `am_db`.`agences` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `am_db`.`chefs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `am_db`.`chefs` ;

CREATE TABLE IF NOT EXISTS `am_db`.`chefs` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `employe_id` INT NOT NULL,
  PRIMARY KEY (`id`, `employe_id`),
  INDEX `fk_chefs_employes1_idx` (`employe_id` ASC),
  CONSTRAINT `fk_chefs_employes1`
    FOREIGN KEY (`employe_id`)
    REFERENCES `am_db`.`employes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `am_db`.`chantiers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `am_db`.`chantiers` ;

CREATE TABLE IF NOT EXISTS `am_db`.`chantiers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `num` VARCHAR(45) NOT NULL,
  `nom` VARCHAR(45) NOT NULL,
  `commentaire` LONGTEXT NULL,
  `tel` VARCHAR(45) NULL,
  `adresse` LONGTEXT NULL,
  `ville` VARCHAR(45) NULL,
  `cp` VARCHAR(45) NULL,
  `created` DATETIME NULL,
  `clos` TINYINT(1) NULL DEFAULT false,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `am_db`.`chantiers_has_chefs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `am_db`.`chantiers_has_chefs` ;

CREATE TABLE IF NOT EXISTS `am_db`.`chantiers_has_chefs` (
  `chantier_id` INT NOT NULL,
  `chef_id` INT NOT NULL,
  PRIMARY KEY (`chantier_id`, `chef_id`),
  INDEX `fk_chantiers_has_chefs_chefs1_idx` (`chef_id` ASC),
  INDEX `fk_chantiers_has_chefs_chantiers1_idx` (`chantier_id` ASC),
  CONSTRAINT `fk_chantiers_has_chefs_chantiers1`
    FOREIGN KEY (`chantier_id`)
    REFERENCES `am_db`.`chantiers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chantiers_has_chefs_chefs1`
    FOREIGN KEY (`chef_id`)
    REFERENCES `am_db`.`chefs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `am_db`.`chantiers_has_chefs_has_employes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `am_db`.`chantiers_has_chefs_has_employes` ;

CREATE TABLE IF NOT EXISTS `am_db`.`chantiers_has_chefs_has_employes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `chantiers_has_chefs_chantiers_id` INT NOT NULL,
  `chantiers_has_chefs_chefs_id` INT NOT NULL,
  `employe_id` INT NOT NULL,
  `jour` DATE NOT NULL,
  `granddeplacement` TINYINT(1) NOT NULL,
  `repas` TINYINT(1) NOT NULL,
  `commentaire` LONGTEXT NULL,
  `matin_debut` DATETIME NULL,
  `matin_fin` DATETIME NULL,
  `aprem_debut` DATETIME NULL,
  `aprem_fin` DATETIME NULL,
  `trajet` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`, `chantiers_has_chefs_chantiers_id`, `chantiers_has_chefs_chefs_id`, `employe_id`, `jour`, `granddeplacement`),
  INDEX `fk_chantier_has_chef_has_employe_employe1_idx` (`employe_id` ASC),
  INDEX `fk_chantiers_has_chefs_has_employes_chantiers_has_chefs1_idx` (`chantiers_has_chefs_chantiers_id` ASC, `chantiers_has_chefs_chefs_id` ASC),
  CONSTRAINT `fk_chantier_has_chef_has_employe_employe1`
    FOREIGN KEY (`employe_id`)
    REFERENCES `am_db`.`employes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_chantiers_has_chefs_has_employes_chantiers_has_chefs1`
    FOREIGN KEY (`chantiers_has_chefs_chantiers_id` , `chantiers_has_chefs_chefs_id`)
    REFERENCES `am_db`.`chantiers_has_chefs` (`chantier_id` , `chef_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `am_db`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `am_db`.`users` ;

CREATE TABLE IF NOT EXISTS `am_db`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `commentaire` LONGTEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `am_db`.`contacts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `am_db`.`contacts` ;

CREATE TABLE IF NOT EXISTS `am_db`.`contacts` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  `prenom` VARCHAR(45) NULL,
  `tel` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `statut` VARCHAR(45) NULL,
  `adresse` LONGTEXT NULL,
  `ville` VARCHAR(45) NULL,
  `commentaire` LONGTEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
